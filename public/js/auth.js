function LoginUser()
        {
            var token    = $("input[name=_token]").val();
            var email    = $("input[name=email_login]").val();
            var password = $("input[name=password_login]").val();
            var data = {
                _token:token,
                email:email,
                password:password
            };
            // Ajax Post
            $.ajax({
                type: "post",
                url: "/api/login/user",
                data: data,
                cache: false,
                success: function (data)
                {
                    console.log('login request sent !');
                    console.log('status: ' +data.status);
                    console.log('message: ' +data.message);
                    $('#seconnecterModal').modal('hide');
                    $('div.setup-panel div a[href="#step-2"]').parent().next().children("a")
                },
                error: function (data){
                    alert('Fail to run Login..');
                }
            });
            return false;
        }
        // login user
        function SignupUser()
        {
            // next code goes here...
            var token       = $("input[name=_token]").val();
            var email       = $("input[name=email]").val();
            var password    = $("input[name=password]").val();
            // garnish the data
            var data = {
                _token:token,
                email:email,
                password:password
            };
            // ajax post
            $.ajax({
                type: "post",
                url: "/api/register/user",
                data: data,
                cache: false,
                success: function (data){
                    $("#register-form")[0].reset(); // this reset the form to back to normal
                    $("#register-status").html(data); // this return the data response 4rm backend
                },
                error: function (data){
                    $("#ajax-error").text('Fail to send request');
                }
            });
            // this make sure the form doesn't load
            // a form pause
            return false;
        }
        $(function() {
            $('#login-form-link').click(function(e) {
                $("#login-form").delay(100).fadeIn(100);
                $("#register-form").fadeOut(100);
                $('#register-form-link').removeClass('active');
                $(this).addClass('active');

                e.preventDefault();
                $("#login-form")[0].reset();
            });
            $('#register-form-link').click(function(e) {
                $("#register-form").delay(100).fadeIn(100);
                $("#login-form").fadeOut(100);
                $('#login-form-link').removeClass('active');
                $(this).addClass('active');

                e.preventDefault();
                $("#register-form")[0].reset();
            });
        });
