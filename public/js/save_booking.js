$(function() {



  /*
  * Block / Unblock UI
  */
  function blockUI() {
    $.blockUI({
        message: "<img src='../../../../img/loading.gif' alt='Loading ...' />",
        overlayCSS: {
            backgroundColor: '#1B2024',
            opacity: 0.55,
            cursor: 'wait'
        },
        css: {
            border: '4px #999 solid',
            padding: 15,
            backgroundColor: '#fff',
            color: 'inherit'
        }
   });
  }

  function unblockUI() {
    $.unblockUI();
  }


  function redirectPage(url, duration) {
	if (duration === undefined) {
			duration = 2000;
	}
	setTimeout(function () {
			window.location.href = url;
	}, duration);
	}


    $('#submit_form_save').click(function(){
        save_function();
    });



        function save_function(){

          var form = new FormData();

          form.append('start_date', $('#start_date').val());
          form.append('end_date', $('#end_date').val());
          form.append('typechambre', $('#typechambre').val());
          form.append('night', $('#night_content').val());
          form.append('quantite', $('#quantite').val());
          form.append('customer_titre', $('#customer_titre').val());
          form.append('customer_nom', $('#customer_nom').val());
          form.append('customer_prenom', $('#customer_prenom').val());
          form.append('customer_email', $('#customer_email').val());
          form.append('customer_phone', $('#customer_phone').val());
          form.append('customer_index_phone', $("#customer_phone").intlTelInput("getSelectedCountryData").dialCode);
          form.append('address', $('#address').val());
          form.append('lat_address', $('input[name=lat_address]').val());
          form.append('lng_address', $('input[name=lng_address]').val());
          form.append('formatted_address', $('input[name=formatted_address]').val());
          form.append('country_short_address', $('input[name=country_short_address]').val());
          form.append('place_id_address', $('input[name=place_id_address]').val());
          form.append('country_customer', $('#country_customer').val());
          form.append('customer_type_paiement', $('#customer_type_paiement').val());

          console.log(form);

            blockUI();

            //return false;

            // process the form
            $.ajax({
                type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                url         : '/save/user/order', // the url where we want to POST
                data        : form, // our data object
                dataType    : 'json', // what type of data do we expect back from the server
                processData: false,
                contentType: false,
                success:function(response) {
                  unblockUI();
                  if(response.success == true){

                    console.log(response.data);

                    swal({
                      title: "Good job!",
                      text: "Commandes enregistrée avec succès",
                      icon: "success",
                    });
                    //return false;
                    setTimeout(function () {
                      redirectPage('/reservation/'+btoa(response.data.ref_code)+'/terminate');
                    }, 500);

                  }else{

                    console.log("erreur");

                    swal("Oops", "Une erreur Veuillez réessayer svp", "error");

                  }
                    // log data to the console so we can see
                  console.log(response);
                    // here we will handle errors and validation messages
                  }
            });
            event.preventDefault();
        }



});
