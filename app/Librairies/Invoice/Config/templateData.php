<?php
/**
 * This file holds the default invoice template language.
 * @var array
 */

return array(
	"header" => 					"FACTURE",
	"to_title" =>					"Client",
	"invoice_number_title" =>		"Référence de la facture",
	"date_title" =>					"Date",
	"payment_terms_title" =>		"Mode de Paiement",
	"due_date_title" =>				"Date d'échéance",
	"purchase_order_title" =>		"Bon de commande",
	"quantity_header" =>			"Quantité",
	"item_header" =>				"Description",
	"unit_cost_header" =>			"Prix Unitaire",
	"amount_header"	=>				"Montant",
	"subtotal_title" =>				"Sous-total",
	"discounts_title" =>			"Réductions",
	"tax_title"	=>					"TVA",
	"shipping_title" =>				"Livraison",
	"total_title" =>				"Total",
	"amount_paid_title"	=>			"Montant à payé",
	"balance_title"	=>				"Solde dû",
	"terms_title" =>				"Conditions",
	"notes_title" =>				"Notes",
	"currency" => 					"XOF",
);
