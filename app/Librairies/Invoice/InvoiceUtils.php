<?php

namespace App\Librairies\Invoice;
use App\Librairies\Invoice\ConceptCore\Invoiced\Core;
use Carbon\Carbon;

class InvoiceUtils
{


  /**
   * holds array with data that needs to be passed trough.
   * @param array
   */
  public function generate($data)
  {
          /**
       * setup the core object
       * @var Core
       */
      $t = new Core();
            /**
       * perform a filter and validation check
       */
      $cleanData = $t->check($data);
      /**
       * Generate invoice pdf
       */
      return $t->generate($cleanData, false);

  }

}
