<?php
namespace App\Librairies;
/**
 *
 */
class SmsSender
{

  const API_URL = 'http://adjeminpush.com/api/smsmessaging/v1';

  private $receiver;
  private $message;
  private $senderName;

  public function __construct(string $receiver = null,string $message  = '', string $senderName = 'Ahoko Rent')
  {
    $this->receiver = $receiver;
    $this->message = $message;
    $this->senderName = $senderName;
  }

    /**
     * @return mixed
     */
  public function submitMessage(){
    $s = curl_init();
    $url = self::API_URL;
    curl_setopt($s, CURLOPT_URL, $url);
    curl_setopt($s, CURLOPT_HEADER, FALSE);
    curl_setopt($s, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($s, CURLOPT_POST,true);
    curl_setopt($s, CURLOPT_POSTFIELDS, $this->getFields());
    curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($s);
    curl_close($s);
    return $response;
  }

    /**
     * @return string
     */
  public function getFields(){
    $fields  = [];
    $fields["receiver"] = $this->receiver;
    $fields["message"]  = $this->message;
    $fields["sender_name"] = $this->senderName;
    $data = json_encode($fields);
    return $data;
  }
}
