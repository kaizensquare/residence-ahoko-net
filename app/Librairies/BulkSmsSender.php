<?php
namespace App\Librairies;
use App\Contact;
use App\Group;
use App\GroupContact;

/**
 *
 */
class BulkSmsSender
{

  const API_URL = 'https://api.bulksms.com/v1';
  const TOKEN_ID = 'D486C71B9B1B4CC1B339BCE1B62701CB-02-F';
  const TOKEN_SECRET = 'fLEo3qkX#hS*tdqUieSeD2sBCs0#E';




  public function __construct()
  {
  }



    /**
     * @return mixed
     */
    public function sendMessage (array $receivers = null,string $message  = '') {

        $fields  = [];
        $fields["from"] = "AHOKO";
        $fields["to"] = $receivers;
        $fields["body"]  = $message;
        $fields["encoding"]  = "UNICODE";
        $post_body = json_encode($fields);

        $ch = curl_init( );
        $url = self::API_URL.'/messages';
        $credentials = self::TOKEN_ID . ':' . self::TOKEN_SECRET;
        $headers = array(
            'Content-Type:application/json',
            'Authorization:Basic '. base64_encode($credentials)
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, 1 );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_body );
        // Allow cUrl functions 20 seconds to execute
        curl_setopt ( $ch, CURLOPT_TIMEOUT, 20 );
        // Wait 10 seconds while trying to connect
        curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
        $output = array();
        $output['server_response'] = curl_exec( $ch );
        $curl_info = curl_getinfo( $ch );
        $output['http_status'] = $curl_info[ 'http_code' ];
        curl_close( $ch );
        return $output;
    }

    public function showMessages($submissionId){
        $ch = curl_init( );
        $url = self::API_URL.'/messages?filter=submission.id%3D'.$submissionId;
        $credentials = self::TOKEN_ID . ':' . self::TOKEN_SECRET;
        $headers = array(
            'Content-Type:application/json',
            'Authorization:Basic '. base64_encode($credentials)
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt ( $ch, CURLOPT_URL, $url );
        //curl_setopt ( $ch, CURLOPT_POST, 1 );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
       // curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_body );
        // Allow cUrl functions 20 seconds to execute
        curl_setopt ( $ch, CURLOPT_TIMEOUT, 20 );
        // Wait 10 seconds while trying to connect
        curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
        $output = array();
        $output['server_response'] = curl_exec( $ch );
        $curl_info = curl_getinfo( $ch );
        $output['http_status'] = $curl_info[ 'http_code' ];
        curl_close( $ch );
        return $output;
    }

    public  function isInsufficientQuota($is_group_receiver, $receiver, $body){

        try{
            if( $is_group_receiver == 1){

                $group = Group::where(["id" =>  $receiver])->first();

                if($group != null){

                    $groupContacts = GroupContact::where(["group_id" => $group->id])->get();

                    if(!empty($groupContacts)){

                        $contacts = array();

                        foreach ($groupContacts as $groupContact){
                            $contact  = Contact::where(["id" => $groupContact->contact_id])->first();

                            if($contact != null){
                                array_push($contacts, "+".$contact->phone);
                            }

                        }
                    }else{
                        $contacts = array();
                    }

                }else{
                    $contacts = array();
                }


            }else{
                $contacts = array();
                $contact  = Contact::where(["id" =>  $receiver])->first();

                if($contact != null){
                    array_push($contacts, "+".$contact->phone);
                }
            }

            $countSMS = round(count($body) / 160) + 1;

            $totalNecessarySMS = ((count($contacts) * $countSMS));

            $ch = curl_init( );
            $url = self::API_URL.'/profile';
            $credentials = self::TOKEN_ID . ':' . self::TOKEN_SECRET;
            $headers = array(
                'Content-Type:application/json',
                'Authorization:Basic '. base64_encode($credentials)
            );
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt ( $ch, CURLOPT_URL, $url );
            //curl_setopt ( $ch, CURLOPT_POST, 1 );
            curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
            // curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_body );
            // Allow cUrl functions 20 seconds to execute
            curl_setopt ( $ch, CURLOPT_TIMEOUT, 20 );
            // Wait 10 seconds while trying to connect
            curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
            $output = array();
            $output['server_response'] = curl_exec( $ch );
            $curl_info = curl_getinfo( $ch );
            $output['http_status'] = $curl_info[ 'http_code' ];
            curl_close( $ch );

            $profile =  $output['server_response'] ;

            $profileDecode = \GuzzleHttp\json_decode($profile, true) ;

        
            $quota = $profileDecode["quota"];
         
            
    
            $remaining = $quota["remaining"];
                    
            $result = $remaining > $totalNecessarySMS;
            
            return !$result;
        }
        catch (\Exception $exception){

            return true;
        }


    }
}
