<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTypeChambreRequest;
use App\Http\Requests\UpdateTypeChambreRequest;
use App\Repositories\TypeChambreRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class TypeChambreController extends AppBaseController
{
    /** @var  TypeChambreRepository */
    private $typeChambreRepository;

    public function __construct(TypeChambreRepository $typeChambreRepo)
    {
        $this->typeChambreRepository = $typeChambreRepo;
    }

    /**
     * Display a listing of the TypeChambre.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $typeChambres = $this->typeChambreRepository->all();

        return view('type_chambres.index')
            ->with('typeChambres', $typeChambres);
    }

    /**
     * Show the form for creating a new TypeChambre.
     *
     * @return Response
     */
    public function create()
    {
        return view('type_chambres.create');
    }

    /**
     * Store a newly created TypeChambre in storage.
     *
     * @param CreateTypeChambreRequest $request
     *
     * @return Response
     */
    public function store(CreateTypeChambreRequest $request)
    {
        $input = $request->all();

        $typeChambre = $this->typeChambreRepository->create($input);

        Flash::success('Type Chambre saved successfully.');

        return redirect(route('typeChambres.index'));
    }

    /**
     * Display the specified TypeChambre.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $typeChambre = $this->typeChambreRepository->find($id);

        if (empty($typeChambre)) {
            Flash::error('Type Chambre not found');

            return redirect(route('typeChambres.index'));
        }

        return view('type_chambres.show')->with('typeChambre', $typeChambre);
    }

    /**
     * Show the form for editing the specified TypeChambre.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $typeChambre = $this->typeChambreRepository->find($id);

        if (empty($typeChambre)) {
            Flash::error('Type Chambre not found');

            return redirect(route('typeChambres.index'));
        }

        return view('type_chambres.edit')->with('typeChambre', $typeChambre);
    }

    /**
     * Update the specified TypeChambre in storage.
     *
     * @param int $id
     * @param UpdateTypeChambreRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypeChambreRequest $request)
    {
        $typeChambre = $this->typeChambreRepository->find($id);

        if (empty($typeChambre)) {
            Flash::error('Type Chambre not found');

            return redirect(route('typeChambres.index'));
        }

        $typeChambre = $this->typeChambreRepository->update($request->all(), $id);

        Flash::success('Type Chambre updated successfully.');

        return redirect(route('typeChambres.index'));
    }

    /**
     * Remove the specified TypeChambre from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $typeChambre = $this->typeChambreRepository->find($id);

        if (empty($typeChambre)) {
            Flash::error('Type Chambre not found');

            return redirect(route('typeChambres.index'));
        }

        $this->typeChambreRepository->delete($id);

        Flash::success('Type Chambre deleted successfully.');

        return redirect(route('typeChambres.index'));
    }
}
