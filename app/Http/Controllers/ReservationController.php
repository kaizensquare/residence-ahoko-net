<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function faireReservation($slug, $date_start, $date_end)
    {

      $product = \App\Models\Product::where('slug', $slug)->first();

      //dd($product->options);

      $datediff = date_diff(date_create(base64_decode($date_start)),date_create(base64_decode($date_end)));

      $night =  ($datediff->days == 0? 1 : $datediff->days);

      $countries = \DB::table('countries')->get();

      //dd($countries);

      return view('reservations.reservation', compact('product', 'date_start','date_end','night','countries'));

    }

    public function terminateReservation($commande){

        try {


         $order = \App\Models\Order::where('ref_code', base64_decode($commande))->first();


        return view('reservations.terminate', compact('order'));


        } catch (\Exception $e) {

        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
