<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Flash;
use Response;
use App\Notifications\PushOrder;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {


        $products = \App\Models\Product::where('status', 1)->take(6)->get();

        $feeds = \App\Models\Feed::take(3)->get();

        $abidjan = \App\Models\Product::where('city', 193)->count();
        $yamoussokro = \App\Models\Product::where('city', 184)->count();
        $bassam = \App\Models\Product::where('city', 18)->count();
        $sanspedro = \App\Models\Product::where('city', 144)->count();

        return view('index', compact('products','feeds','abidjan','yamoussokro','bassam','sanspedro'));
    }

    public function getsearch(Request $request)
    {

      $option_items = \App\Models\OptionItem::all();
      $date = trim($request->input('daterange'));

      $place_id = $request->input('place_id');
      $city = $request->input('city');

      $city_first= explode(",", trim($city));
      if($city_first)
      $city_first_value = ($city_first ? $city_first[0] : '');

      $date_time_from = null;
      $date_time_to = null;
      if($request->input('daterange')){
        if($date){
          $data_ex = explode("-", trim($date));
          $date_time_from = date("d-m-Y", strtotime(trim($data_ex[0])));
          $date_time_to =  date("d-m-Y", strtotime(trim($data_ex[1])));
        }

      }elseif($request->start_date && $request->end_date){

        $date_time_from = date("d-m-Y", strtotime($request->start_date));
        $date_time_to =  date("d-m-Y", strtotime($request->end_date));
      }


      //dd($date_time_to);

      if ($date_time_from && $date_time_to) {

        if($request->min_price && $request->max_price){



          $min_price = explode(" ", $request->min_price);

          $max_price = explode(" ", $request->max_price);

          $value_min_price = str_replace(',', '', $min_price[0]);

          $value_max_price = str_replace(',', '', $max_price[1]);

          $rooms = \App\Models\Room::with('order')->whereHas('order', function ($q) use ($date_time_from, $date_time_to) {
              $q->where(function ($q2) use ($date_time_from, $date_time_to) {
                $q2->whereBetween('start_date', [$date_time_from, $date_time_to])
                ->orWhereBetween('end_date', [$date_time_from, $date_time_to]);
              });
          })->orWhereDoesntHave('order')->whereBetween('price', [intVal($value_min_price), intVal($value_max_price)])->distinct('id_hebergement')->get();

        }else{
          $rooms = \App\Models\Room::with('order')->whereHas('order', function ($q) use ($date_time_from, $date_time_to) {
              $q->where(function ($q2) use ($date_time_from, $date_time_to) {
                $q2->whereBetween('start_date', [$date_time_from, $date_time_to])
                ->orWhereBetween('end_date', [$date_time_from, $date_time_to]);

              });
          })->orWhereDoesntHave('order')->distinct('id_hebergement')->get();


        }


      } else {
          $rooms = null;
      }

    //  dd($rooms);

      return view('search', compact('rooms','date_time_from','date_time_to','city','place_id','city_first_value','option_items'));
    }

    public function getdetail($slug, $date_start = null, $date_end = null)
    {

      //dd($date_end);
      $product = \App\Models\Product::where('slug', $slug)->first();

      return view('detail', compact('product', 'date_start', 'date_end'));
    }

    public function getliste()
    {
        return view('hebergements.listeapparte');
    }

    public function getBlog($slug){

      $feed = \App\Models\Feed::where('slug', $slug)->first();

      return view('blog', compact('feed'));
    }

    public function getContact(){

      return view('contact');
    }

    public function postContact(Request $request){

      //dd($request);

      Mail::send(new ContactMail($request));

      return redirect('/contact')->with('message', 'Email envoyé avec succès');
    }

    public function postUploadImage(Request $request){
      try {


        $product = \App\Models\Product::where('id', $request->productid)->first();


        if($product && $request->hasFile('qqfile')){
            $cover_img = $request->file('qqfile');
            $extension = $cover_img->getClientOriginalExtension();
            $filename = time() . '.' . $extension;

            $storage_data = Storage::disk('public')->put($filename,  File::get($cover_img));

            $file_path = 'uploads/'.$filename;


             $upload = new \App\ProductImage;
             $upload->path = asset($file_path);
             $upload->product()->associate($product);
             $upload->save();
         }

         return response()->json(['success'=>true, 'data' => $upload]);


      } catch (\Exception $e) {
        return response()->json(['success'=>false, 'message' => $e->getMessage()]);

      }

    }

    public function destroyImage($id)
    {
        $product = \App\ProductImage::find($id);

        if (empty($product)) {
            Flash::error('Image not found');

            return redirect()->back();
        }

        $product->delete();

        Flash::success('Image deleted successfully.');

        return redirect()->back();
    }


    public function saveOrderPost(Request $request){

      try {

        $room_id = $request->typechambre;

        $room = \App\Models\Room::where('id', $room_id)->first();
        $night = $request->night;
        $quantite = $request->quantite;
        $customer_titre = $request->customer_titre;
        $customer_nom = $request->customer_nom;
        $customer_prenom = $request->customer_prenom;
        $customer_email = $request->customer_email;
        $customer_phone = $request->customer_phone;
        $customer_index_phone =  $request->customer_index_phone;
        $start_date =  $request->start_date;
        $end_date = $request->end_date;
        $address = $request->address;
        $lat_address = $request->lat_address;
        $lng_address = $request->lng_address;
        $formatted_address = $request->formatted_address;
        $country_short_address = $request->country_short_address;
        $place_id_address = $request->place_id_address;
        $country_customer = $request->country_customer;
        $customer_type_paiement = $request->customer_type_paiement;


        $customer = null;

        $customer_existe = \App\Models\Customer::where('email', trim($customer_email))->first();

        //dd($customer_existe);


        if($customer_existe == null){
          $customer_new = new \App\Models\Customer();
          $customer_new->first_name = $customer_nom;
          $customer_new->last_name = $customer_prenom;
          $customer_new->titre = $customer_titre;
          $customer_new->email = $customer_email;
          $customer_new->phone = $customer_phone;
          $customer_new->user_id = (Auth::user() ? Auth::user()->id : null);
          $customer_new->save();



          $customer = $customer_new;

        }else{
            $customer = $customer_existe;
        }


        $customer_existe_address = \DB::table('customer_addresses')
                ->where('customer_id', $customer->id)
                ->where('place_id', $place_id_address)
                ->first();

        if($customer_existe_address == null){
          $customer_addresse = \DB::table('customer_addresses')->insert(
                ['customer_id' => $customer->id, 'address' => $address, 'country'=>$country_customer, 'lat_address'=>$lat_address, 'lng_address'=>$lng_address,'country_short'=>$country_short_address,'place_id'=>$place_id_address]
            );

        }



        $new_order = new \App\Models\Order();
        $new_order->is_guest = (Auth::user() ? 0 : 1);
        $new_order->customer_id = ($customer? $customer->id : null);
        $new_order->customer_email = $customer_email;
        $new_order->customer_first_name = $customer_nom;
        $new_order->customer_last_name = $customer_prenom;
        if(intVal($quantite)){
          $new_order->total_item_count = ($room ? (intVal($room->price)*intVal($night))*intVal($quantite) : 0);
        }else{
          $new_order->total_item_count = ($room ? (intVal($room->price)*intVal($night)) : 0);
        }
        $new_order->method_payement = $customer_type_paiement;
        $new_order->duration = $night;
        $new_order->start_date =date('Y-m-d', strtotime($start_date));
        $new_order->end_date =date('Y-m-d', strtotime($end_date));
        $new_order->room_id = $room_id;
        $new_order->save();

        if($new_order){
          $order = \App\Models\Order::where('id', $new_order->id)->first();

          $ref_code = "C" . $order->id . "-" . rand();
          $order->ref_code = $ref_code;
          $order->status = 1;
          $order->update();



          $booking_value = \App\Models\Order::where('id', $new_order->id)->first();


          $room_value = \App\Models\Room::where('id', $booking_value->room_id)->first();


          if($room_value){
              //Generate Invoice and save
              $invoice = new \App\Models\Invoice();
              $invoice->reference = $ref_code;
              $invoice->id_editor = ($customer? $customer->id : null);
              $invoice->link = $this->generateInvoiceWithLink($booking_value, $ref_code);
              $invoice->invoice_date = date('Y-m-d H:i:s');
              $invoice->order_id = $booking_value->id;
              $invoice->payment_method = 'cash';
              $win_from_the_owner = $booking_value->duration *  $room_value->price;
              $customer_fees = $booking_value->room->price;
              $invoice->win_from_the_owner = $win_from_the_owner;
              $invoice->customer_fees = $customer_fees;
              $invoice->win_of_ahoko = $customer_fees - $win_from_the_owner;
              $invoice->customer_payed = false;
              $invoice->payed_to_owner= false;
              $invoice->order_status = $booking_value->status;
              $invoice->status = 2;
              $invoice->save();
          }


          $total_day_booking = $this->dayCount($booking_value->start_date, $booking_value->end_date);


          $data = array(
              'nom' => $customer_nom,
              'prenom' => $customer_prenom,
              'ref_code' => $ref_code,
              'number_date' => $total_day_booking,
              'pricing' => $booking_value->price,
              'invoice' =>$invoice->link
          );

          $customer->notify(new PushOrder($customer, $data));


        }
        return response()->json(['success'=>true, 'data'=>$booking_value]);


      } catch (\Exception $e) {
        return response()->json(['success'=>false, 'message' => $e->getMessage()]);

      }


    }


    /**
    * Générer la facture en pdf, l'enregistrer sur le serveur et retour un lien d'accès
    */
    public function generateInvoiceWithLink($booking, $reference){

      $invoiceUtils = new \App\Librairies\Invoice\InvoiceUtils();

      $customerInfo = $booking->customer->first_name."\nTél:".$booking->customer->phone;
      $room = $booking->room;
      $quantity = $booking->duration;
      $unitCost= $room->price;
      $itemDescription =  "Location Hébergement" ;
      /**
       * setup date object
       * @var Carbon
       */
      $date = Carbon::now();
      //$twoWeeks = Carbon\Carbon::now()->addWeeks(2);
      $invoiceData = array(
      "logo" => "http://ahoko.net/assets/img/logo.png", // url to logo
      "from" => "Ahoko Residenxe", // The name of your organization
      "to" => $customerInfo, // The entity being billed - multiple lines ok
      "number" => $reference, // Invoice number
      //"purchase_order" => "", // Purchase order number
      "date" => $date->toFormattedDateString(), // Invoice date
      "due_date" => false, // Invoice due date
      "payment_terms" => null, // Payment terms summary (i.e. NET 30)
      // items
      "items[0][name]" => $itemDescription,
      "items[0][quantity]" => $quantity,
      "items[0][unit_cost]" => $unitCost,

      // End items
      // Fields
      "fields[tax]" => true, // can be true, false
      "fields[discounts]" => false, // Subtotal discounts - numbers only
      "fields[shipping]" => false, // Can be true or false
      // End fields
      "tax"=> 0, // Tax - numbers only
      "amount_paid" => 0, // Amount paid - numbers only
      "notes" => "", // Notes - any extra information not included elsewhere
      "terms" => false, // Terms and conditions - all the details
      );
      $response =  $invoiceUtils->generate($invoiceData);
      $pdf = $response->getBody();
      $contents = $pdf;
      $name = $reference.".pdf";

      //Storage::disk('public')->put($name, $contents);

      //return "http://ahoko.net".Storage::disk('public')->url($name);

        return $this->UploadFileLink($name, $contents);
    }

    // Upload File Générate Link

    public function UploadFileLink($filename, $contents){

      if($contents && $filename){
        Storage::disk('public')->put($filename, $contents);

        $file_path = 'uploads/'.$filename;

        return asset($file_path);
      }else{
        return '';
      }


    }


    function dayCount($from, $to)
    {
        $first_date = strtotime($from);
        $second_date = strtotime($to);
        $days_diff = $second_date - $first_date;
        return date('d', $days_diff);
    }



    public function filterData(Request $request){
      try {

        $date_time_from = date("d-m-Y", strtotime($request->start_date));
        $date_time_to =  date("d-m-Y", strtotime($request->end_date));
        //dd(intVal($request->input('minimum_range')));
        //dd(intVal($request->input('maximum_range')));

        if ($date_time_from && $date_time_to) {

          //$rooms = \App\Models\Room::whereBetween('price', [intVal($request->input('minimum_range')), intVal($request->input('maximum_range'))])->get();
          if($request->type == 'price'){
            $rooms = \App\Models\Room::with('order')->whereHas('order', function ($q) use ($date_time_from, $date_time_to) {
                $q->where(function ($q2) use ($date_time_from, $date_time_to) {
                  $q2->whereBetween('start_date', [$date_time_from, $date_time_to])
                  ->orWhereBetween('end_date', [$date_time_from, $date_time_to]);
                });
            })->orWhereDoesntHave('order')->whereBetween('price', [intVal($request->input('minimum_range')), intVal($request->input('maximum_range'))])->distinct('id_hebergement')->get();


          }elseif($request->type == 'order'){

              $rooms = \App\Models\Room::with('order')->whereHas('order', function ($q) use ($date_time_from, $date_time_to) {
                  $q->where(function ($q2) use ($date_time_from, $date_time_to) {
                    $q2->whereBetween('start_date', [$date_time_from, $date_time_to])
                    ->orWhereBetween('end_date', [$date_time_from, $date_time_to]);
                  });
              })->orWhereDoesntHave('order')->distinct('id_hebergement')->OrderBy('price', $request->order)->get();


          }else{
            $rooms = \App\Models\Room::with('order')->whereHas('order', function ($q) use ($date_time_from, $date_time_to) {
                $q->where(function ($q2) use ($date_time_from, $date_time_to) {
                  $q2->whereBetween('start_date', [$date_time_from, $date_time_to])
                  ->orWhereBetween('end_date', [$date_time_from, $date_time_to]);
                });
            })->orWhereDoesntHave('order')->distinct('id_hebergement')->get();

          }

            $view = view("templates.items",compact('rooms','date_time_from','date_time_to'))->render();

            return response()->json(['success'=>true, "html"=> $view]);

        }

      } catch (\Exception $e) {
        return response()->json(['success' => false, 'message' => 'erreur', 'value' => $e->getMessage()]);
      }

    }


    public function addEtablissement(){

      //return view('hebergements.add_new');
      return view('hebergements.ajouter_process');
    }


}
