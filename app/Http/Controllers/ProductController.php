<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Repositories\ProductRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Flash;
use Response;

class ProductController extends AppBaseController
{
    /** @var  ProductRepository */
    private $productRepository;

    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepository = $productRepo;
    }

    /**
     * Display a listing of the Product.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $products = $this->productRepository->all();

        return view('products.index')
            ->with('products', $products);
    }

    /**
     * Show the form for creating a new Product.
     *
     * @return Response
     */
    public function create()
    {
         $options = \App\Models\OptionItem::where('status', 1)->get();

         $countries = \DB::table('countries')->get();

         $suppliers = \DB::table('suppliers')->get();

         $cities = \DB::table('cities')->get();

        return view('products.create', compact('options', compact('countries','suppliers','cities')));
    }

    /**
     * Store a newly created Product in storage.
     *
     * @param CreateProductRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $product = new \App\Models\Product();
        $product->name = $request->name;
        $product->category = $request->category;
        $product->quantity = 1;
        $product->description = '';
        $product->status = 1;
        $product->save();


      /*  if($product && count($input['option']) > 0){
         foreach ($input['option'] as $key => $value) {
              $product_option = new \App\Models\ProductOption();
              $product_option->product()->associate($product);
              $product_option->option_item_id = $value;
              $product_option->save();
          }

        }*/

      /*  if($request->hasFile('cover')){
          $cover_img = $request->file('cover');
          $extension = $cover_img->getClientOriginalExtension();
          $filename = time() . '.' . $extension;

          $storage_data = Storage::disk('public')->put($filename,  file_get_contents($cover_img));

          $file_path = Storage::url($filename);


           $upload = new \App\ProductImage;
           $upload->path = asset($file_path);
           $upload->product()->associate($product);
           $upload->save();
       }*/


        Flash::success('Product saved successfully.');

        return redirect(route('products.edit', $product->id));
    }

    /**
     * Display the specified Product.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified Product.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $product = $this->productRepository->find($id);

        $options = \App\Models\OptionItem::where('status', 1)->get();

        $countries = \DB::table('countries')->get();

        $suppliers = \DB::table('suppliers')->get();

        $cities = \DB::table('cities')->get();

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        return view('products.edit')->with('product', $product)->with('options', $options)->with('countries', $countries)->with('suppliers', $suppliers)->with('cities',$cities);
    }

    /**
     * Update the specified Product in storage.
     *
     * @param int $id
     * @param UpdateProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductRequest $request)
    {

        $input = $request->all();

        $product_get = $this->productRepository->find($id);

        //dd($product);

        if (empty($product_get)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $product = $this->productRepository->update($request->all(), $id);

        //dd($input);

        if($product && isset($input['option']) &&  count($input['option']) > 0){
         foreach ($input['option'] as $key => $value) {
              $product_option = new \App\Models\ProductOption();
              $product_option->product()->associate($product);
              $product_option->option_item_id = $value;
              $product_option->save();
          }

        }

        if($request->hasFile('cover')){
          $cover_img = $request->file('cover');
          $extension = $cover_img->getClientOriginalExtension();
          $filename = time() . '.' . $extension;

          $storage_data = Storage::disk('public')->put($filename,  File::get($cover_img));

          $file_path = 'uploads/'.$filename;


           $upload = new \App\ProductImage;
           $upload->path = asset($file_path);
           $upload->product()->associate($product);
           $upload->save();
       }


        Flash::success('Product updated successfully.');

      //  dd($id);

        return redirect(route('products.edit', $id));
    }

    /**
     * Remove the specified Product from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $this->productRepository->delete($id);

        Flash::success('Product deleted successfully.');

        return redirect(route('products.index'));
    }

}
