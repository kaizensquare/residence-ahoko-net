<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRoomRequest;
use App\Http\Requests\UpdateRoomRequest;
use App\Repositories\RoomRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class RoomController extends AppBaseController
{
    /** @var  RoomRepository */
    private $roomRepository;

    public function __construct(RoomRepository $roomRepo)
    {
        $this->roomRepository = $roomRepo;
    }

    /**
     * Display a listing of the Room.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $rooms = $this->roomRepository->all();

        $products = \App\Models\Product::where('status', 1)->get();

        return view('rooms.index', compact('rooms','products'));
    }

    /**
     * Show the form for creating a new Room.
     *
     * @return Response
     */
    public function create($id = null)
    {

        $product = \App\Models\Product::find($id);

        $rooms = \App\Models\Room::where('id_hebergement', $product->id)->get();

        $products = \App\Models\Product::where('status', 1)->get();

        $types = \App\Models\TypeChambre::where('status', 1)->get();

        $type_of_beds = \App\Models\TypeBed::where('status', 1)->get();

        $options = \App\Models\OptionItem::where('status', 1)->get();


        return view('rooms.create', compact('products','types','type_of_beds','options','product','rooms'));
    }

    /**
     * Store a newly created Room in storage.
     *
     * @param CreateRoomRequest $request
     *
     * @return Response
     */
    public function store(CreateRoomRequest $request)
    {
        $input = $request->all();

        //dd($input);

        $room = $this->roomRepository->create($input);


        if($room && isset($input['option']) && count($input['option']) > 0){
         foreach ($input['option'] as $key => $value) {
              $room_option = new \App\Models\RoomOption();
              $room_option->room()->associate($room);
              $room_option->option_item_id = $value;
              $room_option->save();
          }

        }
        if($room && isset($input['room_default']) && $input['room_default'] == 1){
          $product = \App\Models\Product::find($input['id_hebergement']);
          $product->default_room =$room->id;
          $product->save();
        }

        Flash::success('Room saved successfully.');

        return redirect(route('rooms.create.find', $input['id_hebergement']));
    }

    /**
     * Display the specified Room.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $room = $this->roomRepository->find($id);

        if (empty($room)) {
            Flash::error('Room not found');

            return redirect(route('rooms.index'));
        }

        return view('rooms.show')->with('room', $room);
    }

    /**
     * Show the form for editing the specified Room.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $room = $this->roomRepository->find($id);

        $products = \App\Models\Product::where('status', 1)->get();

        $types = \App\Models\TypeChambre::where('status', 1)->get();

        $type_of_beds = \App\Models\TypeBed::where('status', 1)->get();

        $options = \App\Models\OptionItem::where('status', 1)->get();



        if (empty($room)) {
            Flash::error('Room not found');

            return redirect(route('rooms.index'));
        }

        return view('rooms.edit', compact('room','products','types','type_of_beds','options'));
    }

    /**
     * Update the specified Room in storage.
     *
     * @param int $id
     * @param UpdateRoomRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoomRequest $request)
    {
        $room = $this->roomRepository->find($id);

        if (empty($room)) {
            Flash::error('Room not found');

            return redirect(route('rooms.index'));
        }

        $room = $this->roomRepository->update($request->all(), $id);

        Flash::success('Room updated successfully.');

        return redirect(route('rooms.index'));
    }

    /**
     * Remove the specified Room from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $room = $this->roomRepository->find($id);

        if (empty($room)) {
            Flash::error('Room not found');

            return redirect(route('rooms.index'));
        }

        $this->roomRepository->delete($id);

        Flash::success('Room deleted successfully.');

        return redirect(route('rooms.index'));
    }
}
