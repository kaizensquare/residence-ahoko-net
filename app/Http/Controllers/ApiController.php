<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class ApiController extends Controller
{
    //




    public function getRoom($id){

      try {

        $room = \App\Models\Room::find($id);

        return response()->json(['success'=>true, 'data' => $room]);

      } catch (\Exception $e) {

        return response()->json(['success'=>false, 'message' => $e->getMessage()]);

      }


    }
    // do login Auth
    public function loginUser(Request $request){

            $email	       = $request->email;
            $password      = $request->password;
            $rememberToken = $request->remember;
            // now we use the Auth to Authenticate the users Credentials
          // Attempt Login for members
          if (Auth::guard('web')->attempt(['email' => $email, 'password' => $password], $rememberToken)) {
            $msg = array(
              'status'  => 'success',
              'message' => 'Login Successful'
            );
            return response()->json($msg);
          } else {
            $msg = array(
              'status'  => 'error',
              'message' => 'Login Fail !'
            );
            return response()->json($msg);
          }
    }


    public function addUser(Request $request){
        // now we get our form data from Request
    	   $email 		= $request->email; // request coming from ajax
    	   $password 	= $request->password; // request comming from ajax
         $status     = "inactive"; // this can be use to check if user account is activated
        // note Laravel uses an encryption system called bcrypt
        // this has been the secure foundation for login queries
        // so here we are to encrypt as Laravel will accept when doing Auth
        $hash_password = bcrypt($password);
    	// do other process
        $users = new User();
        $users->email    = $email;
        $users->password = $hash_password;
        $users->status   = $status;
        $users->save();
    	// return a msg dumb msg with client email
    	$msg = $email." has been registered successfuly";
    	echo $msg; // or return data on json.
    }


}
