<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use Flash;

class HomeController extends Controller
{


  const BOOKING_WAITING = 1;
  const BOOKING_STARTED = 2;
  const BOOKING_ENDED = 3;
  const IS_IN_BOOKING = 4;
  const IS_NOT_IN_BOOKING = 5;

  const ACCOUNT_TYPE_CUSTOMER = 1;

  const ADMIN_ROLE = "admin";
  const ADMIN_CUSTOMER = "customer";


  public function __construct()
  {
      if (Auth::check())
      {
          $user = Auth::user();
          if ($user->isAdmin != 1)
          {
              return redirect('/dashboard');
          }
      }
      else
      {
          return redirect('/');
      }
  }


    //

    /**
   * Show Admin Dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(){

      $order = \App\Models\Order::all();

      $user = Auth::user();

      $bookingStartedCount = \App\Models\Order::where('status', self::BOOKING_STARTED)->count();

      $bookingWaitingCount = \App\Models\Order::where('status', self::BOOKING_WAITING)->count();

      $usersCount = \App\User::count();

      $roomAvailableCount= $this->getRoomAvailableCount();

      return view('admin.home', compact('bookingStartedCount','bookingWaitingCount','roomAvailableCount','usersCount'));
  }

  /**
   * Get Available car
   * @return int
   */
  private function getRoomAvailableCount()
  {
      $roomList = array();

      $rooms = \App\Models\Room::where('deleted_at', '=', null)->get();
      foreach ($rooms as $room){
          if(!$room->isInBooking()){
              array_push($roomList, $room);
          }
      }

      return count($roomList);
  }


  public function confirmOrder($id){

      $booking = \App\Models\Order::find($id);

      if (empty($booking)) {
          Flash::error('Order not found');

          return redirect()->back();
      }

      $booking->is_confirm = 1;
      $booking->status = 4; // confirmed
      $booking->update();

      $invoice = \App\Models\Invoice::where('order_id', $id)->first();
      if(!empty($invoice)){
          $invoice->status = 2; //started
          $invoice->update();
      }

      //Send Email to User
      $bookingRoom= $booking->room;

      if($invoice!= null){
          $invoiceReference = $invoice->reference;
      }else{
         $invoiceReference = "Non Définie";
      }

     $data = ["username" => $booking->customer_first_name,
          "booking_reference" => $invoice->reference,
          "booking_room_label" => "votre hébergement",
          "booking_duration" => $booking->duration(),
          "booking_from" => $booking->start_date,
          "booking_to" => $booking->end_date,
          "booking_url" => $booking->getUrl(),
          "booking_description" => $booking->duration()." du ".$booking->start_date." au ".$booking->end_date
          ];

      \Mail::send('emails.validate_booking', $data, function ($message) use ($booking, $invoiceReference) {
          $message->to($booking->customer_email, $booking->customer_first_name)->subject("Votre commande ". $invoiceReference." a été validée");
      });

      Flash::success('Booking updated successfully.');

      return redirect("/orders/".$id);
  }

  public function startOrder($id){

      $booking = \App\Models\Order::find($id);

      if (empty($booking)) {
          Flash::error('Order not found');

          return redirect()->back();
      }

      $booking->status = 2; //started
      $booking->update();

      $invoice = \App\Models\Invoice::where('order_id', $id)->first();
      if(!empty($invoice)){
          $invoice->status = 2; //started
          $invoice->update();
      }

      Flash::success('Order updated successfully.');

      return redirect("/orders/".$id);
  }

  public function cancelOrder($id){

      $booking = \App\Models\Order::find($id);

      if (empty($booking)) {
          Flash::error('Order not found');

        return redirect()->back();
      }

      $booking->status = 5; //canceled
      $booking->update();

      $invoice = \App\Models\Invoice::where('order_id', $id)->first();
      if(!empty($invoice)){
          $invoice->status = 2; //started
          $invoice->update();
      }

      Flash::success('order updated successfully.');

      return redirect("/orders/".$id);
  }

  public function done($id){

      $booking = \App\Models\Order::find($id);

      if (empty($booking)) {
          Flash::error('Order not found');

        return redirect()->back();
      }

      $booking->status = 3;
      $booking->update();

      $invoice = \App\Models\Invoice::where('order_id', $id)->first();
      if(!empty($invoice)){
          $invoice->status = 2; //started
          $invoice->update();
      }

      Flash::success('Booking updated successfully.');

      return redirect("/admin/bookings/".$id);
  }

}
