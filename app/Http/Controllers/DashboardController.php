<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
    public function __construct()
  {
      $this->middleware(['auth', 'verified']);
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index()
  {
      return view('home');
  }

  public function getCommande(){

     $user = Auth::user();

     $customer = \App\Models\Customer::where('user_id', $user->id)->first();

     $commandes = \App\Models\Order::where('customer_id', $customer->id)->get();

    return view('dashboard.commandes', compact('commandes'));
  }

  public function getSetting(){

    return view('dashboard.settings');
  }

}
