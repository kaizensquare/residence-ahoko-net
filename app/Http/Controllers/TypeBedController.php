<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTypeBedRequest;
use App\Http\Requests\UpdateTypeBedRequest;
use App\Repositories\TypeBedRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class TypeBedController extends AppBaseController
{
    /** @var  TypeBedRepository */
    private $typeBedRepository;

    public function __construct(TypeBedRepository $typeBedRepo)
    {
        $this->typeBedRepository = $typeBedRepo;
    }

    /**
     * Display a listing of the TypeBed.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $typeBeds = $this->typeBedRepository->all();

        return view('type_beds.index')
            ->with('typeBeds', $typeBeds);
    }

    /**
     * Show the form for creating a new TypeBed.
     *
     * @return Response
     */
    public function create()
    {
        return view('type_beds.create');
    }

    /**
     * Store a newly created TypeBed in storage.
     *
     * @param CreateTypeBedRequest $request
     *
     * @return Response
     */
    public function store(CreateTypeBedRequest $request)
    {
        $input = $request->all();

        $typeBed = $this->typeBedRepository->create($input);

        Flash::success('Type Bed saved successfully.');

        return redirect(route('typeBeds.index'));
    }

    /**
     * Display the specified TypeBed.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $typeBed = $this->typeBedRepository->find($id);

        if (empty($typeBed)) {
            Flash::error('Type Bed not found');

            return redirect(route('typeBeds.index'));
        }

        return view('type_beds.show')->with('typeBed', $typeBed);
    }

    /**
     * Show the form for editing the specified TypeBed.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $typeBed = $this->typeBedRepository->find($id);

        if (empty($typeBed)) {
            Flash::error('Type Bed not found');

            return redirect(route('typeBeds.index'));
        }

        return view('type_beds.edit')->with('typeBed', $typeBed);
    }

    /**
     * Update the specified TypeBed in storage.
     *
     * @param int $id
     * @param UpdateTypeBedRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypeBedRequest $request)
    {
        $typeBed = $this->typeBedRepository->find($id);

        if (empty($typeBed)) {
            Flash::error('Type Bed not found');

            return redirect(route('typeBeds.index'));
        }

        $typeBed = $this->typeBedRepository->update($request->all(), $id);

        Flash::success('Type Bed updated successfully.');

        return redirect(route('typeBeds.index'));
    }

    /**
     * Remove the specified TypeBed from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $typeBed = $this->typeBedRepository->find($id);

        if (empty($typeBed)) {
            Flash::error('Type Bed not found');

            return redirect(route('typeBeds.index'));
        }

        $this->typeBedRepository->delete($id);

        Flash::success('Type Bed deleted successfully.');

        return redirect(route('typeBeds.index'));
    }
}
