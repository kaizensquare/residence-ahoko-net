<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOptionItemRequest;
use App\Http\Requests\UpdateOptionItemRequest;
use App\Repositories\OptionItemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class OptionItemController extends AppBaseController
{
    /** @var  OptionItemRepository */
    private $optionItemRepository;

    public function __construct(OptionItemRepository $optionItemRepo)
    {
        $this->optionItemRepository = $optionItemRepo;
    }

    /**
     * Display a listing of the OptionItem.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $optionItems = $this->optionItemRepository->all();

        return view('option_items.index')
            ->with('optionItems', $optionItems);
    }

    /**
     * Show the form for creating a new OptionItem.
     *
     * @return Response
     */
    public function create()
    {
        return view('option_items.create');
    }

    /**
     * Store a newly created OptionItem in storage.
     *
     * @param CreateOptionItemRequest $request
     *
     * @return Response
     */
    public function store(CreateOptionItemRequest $request)
    {
        $input = $request->all();

        $optionItem = $this->optionItemRepository->create($input);

        Flash::success('Option Item saved successfully.');

        return redirect(route('optionItems.index'));
    }

    /**
     * Display the specified OptionItem.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $optionItem = $this->optionItemRepository->find($id);

        if (empty($optionItem)) {
            Flash::error('Option Item not found');

            return redirect(route('optionItems.index'));
        }

        return view('option_items.show')->with('optionItem', $optionItem);
    }

    /**
     * Show the form for editing the specified OptionItem.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $optionItem = $this->optionItemRepository->find($id);

        if (empty($optionItem)) {
            Flash::error('Option Item not found');

            return redirect(route('optionItems.index'));
        }

        return view('option_items.edit')->with('optionItem', $optionItem);
    }

    /**
     * Update the specified OptionItem in storage.
     *
     * @param int $id
     * @param UpdateOptionItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOptionItemRequest $request)
    {
        $optionItem = $this->optionItemRepository->find($id);

        if (empty($optionItem)) {
            Flash::error('Option Item not found');

            return redirect(route('optionItems.index'));
        }

        $optionItem = $this->optionItemRepository->update($request->all(), $id);

        Flash::success('Option Item updated successfully.');

        return redirect(route('optionItems.index'));
    }

    /**
     * Remove the specified OptionItem from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $optionItem = $this->optionItemRepository->find($id);

        if (empty($optionItem)) {
            Flash::error('Option Item not found');

            return redirect(route('optionItems.index'));
        }

        $this->optionItemRepository->delete($id);

        Flash::success('Option Item deleted successfully.');

        return redirect(route('optionItems.index'));
    }
}
