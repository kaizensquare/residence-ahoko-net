<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFeedRequest;
use App\Http\Requests\UpdateFeedRequest;
use App\Repositories\FeedRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Flash;
use Response;

class FeedController extends AppBaseController
{
    /** @var  FeedRepository */
    private $feedRepository;

    public function __construct(FeedRepository $feedRepo)
    {
        $this->feedRepository = $feedRepo;
    }

    /**
     * Display a listing of the Feed.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $feeds = $this->feedRepository->all();

        return view('feeds.index')
            ->with('feeds', $feeds);
    }

    /**
     * Show the form for creating a new Feed.
     *
     * @return Response
     */
    public function create()
    {
        return view('feeds.create');
    }

    /**
     * Store a newly created Feed in storage.
     *
     * @param CreateFeedRequest $request
     *
     * @return Response
     */
    public function store(CreateFeedRequest $request)
    {
        $input = $request->all();

        $feed = $this->feedRepository->create($input);

        if($request->hasFile('image_url')){
          $image_url = $request->file('image_url');
          $extension = $image_url->getClientOriginalExtension();
          $filename = time() . '.' . $extension;

          $storage_data = Storage::disk('public')->put($filename,  file_get_contents($image_url));

          $file_path = Storage::url($filename);

           $upload =  \App\Models\Feed::find($feed->id);
           $upload->image = asset($file_path);
           $upload->save();
       }

        Flash::success('Feed saved successfully.');

        return redirect(route('feeds.index'));
    }

    /**
     * Display the specified Feed.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $feed = $this->feedRepository->find($id);

        if (empty($feed)) {
            Flash::error('Feed not found');

            return redirect(route('feeds.index'));
        }

        return view('feeds.show')->with('feed', $feed);
    }

    /**
     * Show the form for editing the specified Feed.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $feed = $this->feedRepository->find($id);

        if (empty($feed)) {
            Flash::error('Feed not found');

            return redirect(route('feeds.index'));
        }

        return view('feeds.edit')->with('feed', $feed);
    }

    /**
     * Update the specified Feed in storage.
     *
     * @param int $id
     * @param UpdateFeedRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFeedRequest $request)
    {
        $feed = $this->feedRepository->find($id);

        if (empty($feed)) {
            Flash::error('Feed not found');

            return redirect(route('feeds.index'));
        }

        $feed = $this->feedRepository->update($request->all(), $id);

        Flash::success('Feed updated successfully.');

        return redirect(route('feeds.index'));
    }

    /**
     * Remove the specified Feed from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $feed = $this->feedRepository->find($id);

        if (empty($feed)) {
            Flash::error('Feed not found');

            return redirect(route('feeds.index'));
        }

        $this->feedRepository->delete($id);

        Flash::success('Feed deleted successfully.');

        return redirect(route('feeds.index'));
    }
}
