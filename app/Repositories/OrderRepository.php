<?php

namespace App\Repositories;

use App\Models\Order;
use App\Repositories\BaseRepository;

/**
 * Class OrderRepository
 * @package App\Repositories
 * @version June 2, 2019, 10:59 pm UTC
*/

class OrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'status',
        'is_guest',
        'customer_email',
        'customer_first_name',
        'customer_last_name',
        'coupon_code',
        'total_item_count',
        'customer_id',
        'method_payement',
        'duration',
        'ref_code',
        'is_confirm',
        'id_invoice',
        'start_date',
        'end_date'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Order::class;
    }
}
