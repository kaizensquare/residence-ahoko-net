<?php

namespace App\Repositories;

use App\Models\TypeBed;
use App\Repositories\BaseRepository;

/**
 * Class TypeBedRepository
 * @package App\Repositories
 * @version June 3, 2019, 10:49 am UTC
*/

class TypeBedRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TypeBed::class;
    }
}
