<?php

namespace App\Repositories;

use App\Models\Feed;
use App\Repositories\BaseRepository;

/**
 * Class FeedRepository
 * @package App\Repositories
 * @version June 5, 2019, 12:29 am UTC
*/

class FeedRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'subtitle',
        'slug',
        'description',
        'image'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Feed::class;
    }
}
