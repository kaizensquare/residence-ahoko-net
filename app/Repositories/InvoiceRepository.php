<?php

namespace App\Repositories;

use App\Models\Invoice;
use App\Repositories\BaseRepository;

/**
 * Class InvoiceRepository
 * @package App\Repositories
 * @version June 7, 2019, 4:15 am UTC
*/

class InvoiceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'reference',
        'id_editor',
        'link',
        'order_id',
        'payment_method',
        'win_from_the_owner',
        'customer_fees',
        'win_of_ahoko',
        'customer_payed',
        'payed_to_owner',
        'status',
        'archived',
        'customer_transaction_reference',
        'owner_transaction_reference'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Invoice::class;
    }
}
