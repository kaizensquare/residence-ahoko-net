<?php

namespace App\Repositories;

use App\Models\OptionItem;
use App\Repositories\BaseRepository;

/**
 * Class OptionItemRepository
 * @package App\Repositories
 * @version June 3, 2019, 11:26 am UTC
*/

class OptionItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OptionItem::class;
    }
}
