<?php

namespace App\Repositories;

use App\Models\TypeChambre;
use App\Repositories\BaseRepository;

/**
 * Class TypeChambreRepository
 * @package App\Repositories
 * @version June 3, 2019, 8:31 am UTC
*/

class TypeChambreRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TypeChambre::class;
    }
}
