<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TypeChambre
 * @package App\Models
 * @version June 3, 2019, 8:31 am UTC
 *
 * @property string name
 * @property integer status
 */
class TypeChambre extends Model
{
    use SoftDeletes;

    public $table = 'type_chambres';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
