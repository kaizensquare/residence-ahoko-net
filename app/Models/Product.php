<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Class Product
 * @package App\Models
 * @version June 2, 2019, 10:32 pm UTC
 *
 * @property string name
 * @property string slug
 * @property integer quantity
 * @property integer price
 * @property integer status
 */
class Product extends Model
{
    use SoftDeletes;
    use Sluggable;


    public $table = 'products';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'slug',
        'quantity',
        'description',
        'category',
        'status',
        'country_id',
        'city',
        'address_place_id',
        'address',
        'supplier_id',
        'lat_address',
        'lng_address'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'category'=>'integer',
        'quantity' => 'integer',
        'description' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'quantity' => 'required',
        'status' => 'required'
    ];

    public function product_images()
   {
     return $this->hasMany(\App\ProductImage::class);
   }

  public function rooms(){

    return $this->hasMany(\App\Models\Room::class, 'id_hebergement');

  }

  public function options(){

    return $this->hasMany(\App\Models\ProductOption::class);

  }

  public function default_room(){
    return \App\Models\Room::where('id', $this->default_room)->first();
  }

  public function order()
  {
      return $this->HasOne(\App\Models\Order::class, 'product_id')->withTrashed();
  }


}
