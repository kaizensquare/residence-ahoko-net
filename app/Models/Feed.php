<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
/**
 * Class Feed
 * @package App\Models
 * @version June 5, 2019, 12:29 am UTC
 *
 * @property string title
 * @property string subtitle
 * @property string slug
 * @property string description
 * @property string image
 */
class Feed extends Model
{
    use SoftDeletes;
    use Sluggable;

    public $table = 'feeds';


    protected $dates = ['deleted_at'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }



    public $fillable = [
        'title',
        'subtitle',
        'slug',
        'description',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'subtitle' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
