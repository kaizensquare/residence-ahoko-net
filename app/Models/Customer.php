<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Customer
 * @package App\Models
 * @version June 7, 2019, 1:56 am UTC
 *
 * @property string first_name
 * @property string last_name
 * @property string titre
 * @property string date_of_birth
 * @property string email
 * @property string phone
 * @property string index_phone
 * @property integer status
 * @property string subscribed_to_news_letter
 */
class Customer extends Model
{
    use SoftDeletes;
    use Notifiable;

    public $table = 'customers';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'first_name',
        'last_name',
        'titre',
        'email',
        'phone',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'first_name' => 'string',
        'last_name' => 'string',
        'titre' => 'string',
        'date_of_birth' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'index_phone' => 'string',
        'status' => 'integer',
        'subscribed_to_news_letter' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function order()
    {
      return $this->belongsTo(\App\Models\Order::class);
    }


}
