<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Product;

/**
 * Class RoomOption
 * @package App\Models
 * @version June 3, 2019, 11:51 am UTC
 *
 */
class ProductOption extends Model
{
    use SoftDeletes;

    public $table = 'product_options';


    protected $dates = ['deleted_at'];


    public function product()
    {
      return $this->belongsTo(Product::class);
    }

    public function option()
    {
        return $this->HasOne(OptionItem::class, 'id', 'product_option_id');
    }


}
