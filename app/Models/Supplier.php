<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Supplier
 * @package App\Models
 * @version June 7, 2019, 9:02 am UTC
 *
 * @property string company_name
 * @property string supplier_name
 * @property string supplier_email
 * @property string supplier_phone
 * @property string supplier_address
 * @property integer user_id
 * @property integer status
 */
class Supplier extends Model
{
    use SoftDeletes;

    public $table = 'suppliers';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'company_name',
        'supplier_name',
        'supplier_email',
        'supplier_phone',
        'supplier_address',
        'user_id',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'company_name' => 'string',
        'supplier_name' => 'string',
        'supplier_email' => 'string',
        'supplier_phone' => 'string',
        'supplier_address' => 'string',
        'user_id' => 'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
