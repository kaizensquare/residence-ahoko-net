<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Order
 * @package App\Models
 * @version June 2, 2019, 10:59 pm UTC
 *
 * @property string status
 * @property integer is_guest
 * @property string customer_email
 * @property string customer_first_name
 * @property string customer_last_name
 * @property string coupon_code
 * @property integer total_item_count
 * @property integer customer_id
 * @property integer method_payement
 * @property integer duration
 * @property string ref_code
 * @property integer is_confirm
 * @property integer id_invoice
 * @property string start_date
 * @property string end_date
 */
class Order extends Model
{
    use SoftDeletes;

    public $table = 'orders';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'status',
        'is_guest',
        'customer_email',
        'customer_first_name',
        'customer_last_name',
        'coupon_code',
        'total_item_count',
        'customer_id',
        'method_payement',
        'duration',
        'ref_code',
        'is_confirm',
        'id_invoice',
        'start_date',
        'end_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'status' => 'string',
        'is_guest' => 'integer',
        'customer_email' => 'string',
        'customer_first_name' => 'string',
        'customer_last_name' => 'string',
        'coupon_code' => 'string',
        'total_item_count' => 'integer',
        'customer_id' => 'integer',
        'method_payement' => 'integer',
        'duration' => 'integer',
        'ref_code' => 'string',
        'is_confirm' => 'integer',
        'id_invoice' => 'integer',
        'start_date' => 'string',
        'end_date' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


    public function customer()
    {
        return $this->HasOne(\App\Models\Customer::class, 'id', 'customer_id');
    }



    public function room()
    {
        return $this->belongsTo(\App\Models\Room::class, 'room_id')->withTrashed();
    }

    public function currency(){
        return "FCFA";
    }


    public function duration(){
        $datetime1 = new \DateTime($this->start_date);
        $datetime2 = new \DateTime($this->end_date);
        $interval = date_diff($datetime1, $datetime2);
        return  $interval->format('%a Jours');

    }

    public function invoice(){
      return \App\Models\Invoice::where("order_id", $this->id)->first();
    }

    public function isUnPaidCustomer(){
      $invoice = \App\Models\Invoice::where("order_id", $this->id)->first();
      if($invoice == null){
        return  true;
      }else {
        return $invoice->status != 1;
      }

    }

    public function isUnPaidOwner(){

      $invoice = \App\Models\Invoice::where("order_id", $this->id)->first();
      if($invoice == null){
        return  true;
      }else {
        return $invoice->payed_to_owner != 1;
      }

    }


    /**
     * @return mixed
     */
    public function isPaid(){
        return $this->isPaidOwner();
    }

    /**
     * @return mixed
     */
    public function isUnPaid(){
        return $this->isUnPaidOwner();
    }

    public function isPaidOwner(){
      $invoice = \App\Models\Invoice::where("order_id", $this->id)->first();
      if($invoice == null){
        return  true;
      }else {
          return $invoice->payed_to_owner == 1;
      }
    }


    public function isWaiting(){
        return $this->status == 1;
    }

    public function isStarted(){
        return $this->status == 2;
    }

    public function  isEnded(){
        return $this->status == 3;
    }

    public function  isConfirmed(){
        return $this->status == 4;
    }

    public function  isCanceled(){
        return $this->status == 5;
    }

    public function getUrl(){
        return "http://ahoko.net";
    }




}
