<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Room;
use App\Models\OptionItem;
/**
 * Class RoomOption
 * @package App\Models
 * @version June 3, 2019, 11:51 am UTC
 *
 */
class RoomOption extends Model
{
    use SoftDeletes;

    public $table = 'room_options';


    protected $dates = ['deleted_at'];


    public function room()
    {
      return $this->belongsTo(Room::class);
    }


    public function option()
    {
        return $this->belongsTo(OptionItem::class);
    }

}
