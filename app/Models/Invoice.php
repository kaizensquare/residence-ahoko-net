<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Invoice
 * @package App\Models
 * @version June 7, 2019, 4:15 am UTC
 *
 * @property string reference
 * @property string id_editor
 * @property string link
 * @property integer order_id
 * @property string payment_method
 * @property string win_from_the_owner
 * @property string customer_fees
 * @property string win_of_ahoko
 * @property string customer_payed
 * @property string payed_to_owner
 * @property integer status
 * @property boolean archived
 * @property string customer_transaction_reference
 * @property string owner_transaction_reference
 */
class Invoice extends Model
{
    use SoftDeletes;

    public $table = 'invoices';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'reference',
        'id_editor',
        'link',
        'order_id',
        'payment_method',
        'win_from_the_owner',
        'customer_fees',
        'win_of_ahoko',
        'customer_payed',
        'payed_to_owner',
        'status',
        'archived',
        'customer_transaction_reference',
        'owner_transaction_reference'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'reference' => 'string',
        'id_editor' => 'string',
        'link' => 'string',
        'order_id' => 'integer',
        'payment_method' => 'string',
        'win_from_the_owner' => 'string',
        'customer_fees' => 'string',
        'win_of_ahoko' => 'string',
        'customer_payed' => 'string',
        'payed_to_owner' => 'string',
        'status' => 'integer',
        'archived' => 'boolean',
        'customer_transaction_reference' => 'string',
        'owner_transaction_reference' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
