<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\RoomOption;
use App\Models\ProductOption;

/**
 * Class OptionItem
 * @package App\Models
 * @version June 3, 2019, 11:26 am UTC
 *
 * @property string name
 * @property integer status
 */
class OptionItem extends Model
{
    use SoftDeletes;

    public $table = 'option_items';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


    public function room_option()
    {
        return $this->hasOne(RoomOption::class);
    }

    public function product_option()
    {
        return $this->hasOne(ProductOption::class);
    }

}
