<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Room
 * @package App\Models
 * @version June 3, 2019, 8:18 am UTC
 *
 * @property integer id_hebergement
 * @property integer type_id
 * @property integer type_of_bed
 * @property string price
 * @property string cost
 * @property integer quantity
 * @property integer status
 */
class Room extends Model
{
    use SoftDeletes;

    public $table = 'rooms';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_hebergement',
        'type_id',
        'type_of_bed',
        'price',
        'cost',
        'quantity',
        'status',
        'shower'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_hebergement' => 'integer',
        'type_id' => 'integer',
        'type_of_bed' => 'integer',
        'price' => 'string',
        'cost' => 'string',
        'quantity' => 'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function options()
   {
     return $this->hasMany(\App\Models\RoomOption::class);
   }

   public function product()
   {
     return $this->belongsTo(\App\Models\Product::class);
   }

   public function orderOne()
   {
     return $this->belongsTo(\App\Models\Order::class);
   }

   public function product_one(){
     return \App\Models\Product::where('id', $this->id_hebergement)->first();
   }


   public function order()
   {
       return $this->HasOne(\App\Models\Order::class, 'room_id')->withTrashed();
   }


   public function type_chambre()
   {
     return \App\Models\TypeChambre::where('id', $this->type_id)->first();

   }

   /**
    * @return bool
    */
   public function isInBooking(){

       $bookings = \App\Models\Order::where('room_id', $this->id)->where('status' ,'!=', 3)->get();

       if (empty($bookings)){
           $isAvailable = false;
       }else{
           if($bookings->isNotEmpty()){
               $isAvailable = true;


           }else{
               $isAvailable = false;

           }

       }

       return $isAvailable;

   }


}
