<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Product;

class ProductImage extends Model
{

  use SoftDeletes;

  protected $fillable = [
     'path'
   ];

   public function product()
   {
     return $this->belongsTo(Product::class);
   }

}
