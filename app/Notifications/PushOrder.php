<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PushOrder extends Notification
{
    use Queueable;

    public $customer;

    public $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($customer, $order)
    {

      $this->customer = $customer;
      $this->order = $order;
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
     public function toMail($notifiable)
     {

       $greeting = 'Salut '. $this->customer->first_name;
       $order_value = 'Ceci est votre confirmation pour le numéro de commande : '.$this->order['ref_code'];

         return (new MailMessage)
                     ->from('noreply@ahoko.net', 'Ahoko Residence')
                     ->subject('Votre commande')
                     ->greeting($greeting)
                     ->line('Merci de votre commande.')
                     ->line($order_value)
                     ->action('Voir la facture', $this->order['invoice'])
                     ->line("Merci d'avoir utilisé Ahoko Rent!");
     }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
