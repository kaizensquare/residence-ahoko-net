<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes(['verify'=>true]);

Route::get('/add/etablissement', 'HomeController@addEtablissement');

Route::get('/', 'HomeController@index')->name('home');

Route::get('/search', 'HomeController@getSearch');

Route::get('/detail/{slug}/{date_start}/{date_end}', 'HomeController@getDetail')->name('getdetails');

Route::get('liste', 'HomeController@getliste')->name('getlistes'); //liste Appartement

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::get('/commandes', 'DashboardController@getCommande')->name('commande');

Route::get('/settings', 'DashboardController@getSetting')->name('setting');

Route::get('/blog/{slug}', 'HomeController@getBlog'); //liste Appartement

Route::get('/contact', 'HomeController@getContact'); //liste Appartement

Route::post('/send/contact', 'HomeController@postContact'); //liste Appartement

//Lien Faire Reservation
Route::get('reservation/{slug}/{date_start}/{date_end}', 'ReservationController@faireReservation')->name('faireReservations');

Route::get('reservation/{commande}/terminate', 'ReservationController@terminateReservation');


Route::get('/image/delete/{id}', 'HomeController@destroyImage'); //liste Appartement

Route::post('/filtre/search/engine', 'HomeController@filterData');


Route::post('data/uploads/images', 'HomeController@postUploadImage'); //liste Appartement

Route::group(['namespace' => 'Admin', 'prefix' => 'admin','middleware' => 'admin'], function () {

    Route::get('/', 'HomeController@index');

    Route::get('orders/{id}/confirm-order', 'HomeController@confirmOrder');
    Route::get('orders/{id}/start-order', 'HomeController@startOrder');
    Route::get('orders/{id}/cancel-order', 'HomeController@cancelOrder');
    Route::get('orders/{id}/done', 'HomeController@done');
    Route::get('orders/{id}/invoice', 'HomeController@getInvoice');

});

Route::post('save/user/order', 'HomeController@saveOrderPost'); //liste Appartement


Route::resource('products', 'ProductController');

Route::resource('users', 'UserController');

Route::resource('orders', 'OrderController');

Route::resource('rooms', 'RoomController');

Route::get('/rooms/create/{id}', 'RoomController@create')->name('rooms.create.find');

Route::resource('typeChambres', 'TypeChambreController');

Route::resource('typeBeds', 'TypeBedController');

Route::resource('optionItems', 'OptionItemController');

Route::resource('feeds', 'FeedController');



//ApiController
Route::get('/api/room/{id}', 'ApiController@getRoom'); //liste Appartement

Route::post('/api/register/user', 'ApiController@addUser');
Route::post('/api/login/user', 'ApiController@loginUser');


Route::resource('customers', 'CustomerController');


Route::resource('invoices', 'InvoiceController');

Route::resource('suppliers', 'SupplierController');


Route::get('sendemail', function () {
    $data = array(
        'name' => "Ahoko Rent Test",
    );
    Mail::send('emails.test', $data, function ($message) {
        $message->to('jacques.bagui@gmail.com')->subject('Location vehicule Ahoko Rent');
    });
    return "Message Envoyé avec success";
});



Route::get('/foo', function () {
    Artisan::call('storage:link');
});
