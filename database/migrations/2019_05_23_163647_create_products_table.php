<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category');
            $table->string('name');
            $table->string('slug');
            $table->integer('quantity');
            $table->string('description');
            $table->integer('country_id');
            $table->string('address_place_id');
            $table->string('address');
            $table->string('city');
            $table->integer('default_room')->nullable($value = true);
            $table->integer('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
