<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoomOptionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('room_id')->unsigned()->index();
            $table->integer('option_item_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('room_id')
            ->references('id')
            ->on('rooms')
            ->onDelete('cascade');
            $table->foreign('option_item_id')
            ->references('id')
            ->on('option_items')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('room_options');
    }
}
