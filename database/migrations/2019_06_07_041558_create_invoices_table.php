<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvoicesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference');
            $table->string('id_editor');
            $table->string('link');
            $table->integer('order_id');
            $table->string('payment_method');
            $table->string('win_from_the_owner');
            $table->string('customer_fees');
            $table->string('win_of_ahoko');
            $table->boolean('customer_payed');
            $table->boolean('payed_to_owner');
            $table->integer('status');
            $table->boolean('archived');
            $table->string('customer_transaction_reference');
            $table->string('owner_transaction_reference');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoices');
    }
}
