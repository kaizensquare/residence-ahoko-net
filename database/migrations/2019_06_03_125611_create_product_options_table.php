<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_options', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('product_id')->unsigned()->index();
          $table->integer('option_item_id')->unsigned()->index();
          $table->timestamps();
          $table->softDeletes();
          $table->foreign('product_id')
          ->references('id')
          ->on('products')
          ->onDelete('cascade');
          $table->foreign('option_item_id')
          ->references('id')
          ->on('option_items')
          ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_options', function (Blueprint $table) {
            //
        });
    }
}
