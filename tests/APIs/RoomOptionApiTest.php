<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeRoomOptionTrait;
use Tests\ApiTestTrait;

class RoomOptionApiTest extends TestCase
{
    use MakeRoomOptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_room_option()
    {
        $roomOption = $this->fakeRoomOptionData();
        $this->response = $this->json('POST', '/api/roomOptions', $roomOption);

        $this->assertApiResponse($roomOption);
    }

    /**
     * @test
     */
    public function test_read_room_option()
    {
        $roomOption = $this->makeRoomOption();
        $this->response = $this->json('GET', '/api/roomOptions/'.$roomOption->id);

        $this->assertApiResponse($roomOption->toArray());
    }

    /**
     * @test
     */
    public function test_update_room_option()
    {
        $roomOption = $this->makeRoomOption();
        $editedRoomOption = $this->fakeRoomOptionData();

        $this->response = $this->json('PUT', '/api/roomOptions/'.$roomOption->id, $editedRoomOption);

        $this->assertApiResponse($editedRoomOption);
    }

    /**
     * @test
     */
    public function test_delete_room_option()
    {
        $roomOption = $this->makeRoomOption();
        $this->response = $this->json('DELETE', '/api/roomOptions/'.$roomOption->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/roomOptions/'.$roomOption->id);

        $this->response->assertStatus(404);
    }
}
