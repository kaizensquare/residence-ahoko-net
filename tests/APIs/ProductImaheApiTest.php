<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeProductImaheTrait;
use Tests\ApiTestTrait;

class ProductImaheApiTest extends TestCase
{
    use MakeProductImaheTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_product_imahe()
    {
        $productImahe = $this->fakeProductImaheData();
        $this->response = $this->json('POST', '/api/productImahes', $productImahe);

        $this->assertApiResponse($productImahe);
    }

    /**
     * @test
     */
    public function test_read_product_imahe()
    {
        $productImahe = $this->makeProductImahe();
        $this->response = $this->json('GET', '/api/productImahes/'.$productImahe->id);

        $this->assertApiResponse($productImahe->toArray());
    }

    /**
     * @test
     */
    public function test_update_product_imahe()
    {
        $productImahe = $this->makeProductImahe();
        $editedProductImahe = $this->fakeProductImaheData();

        $this->response = $this->json('PUT', '/api/productImahes/'.$productImahe->id, $editedProductImahe);

        $this->assertApiResponse($editedProductImahe);
    }

    /**
     * @test
     */
    public function test_delete_product_imahe()
    {
        $productImahe = $this->makeProductImahe();
        $this->response = $this->json('DELETE', '/api/productImahes/'.$productImahe->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/productImahes/'.$productImahe->id);

        $this->response->assertStatus(404);
    }
}
