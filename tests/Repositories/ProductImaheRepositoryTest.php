<?php namespace Tests\Repositories;

use App\Models\ProductImahe;
use App\Repositories\ProductImaheRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeProductImaheTrait;
use Tests\ApiTestTrait;

class ProductImaheRepositoryTest extends TestCase
{
    use MakeProductImaheTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProductImaheRepository
     */
    protected $productImaheRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->productImaheRepo = \App::make(ProductImaheRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_product_imahe()
    {
        $productImahe = $this->fakeProductImaheData();
        $createdProductImahe = $this->productImaheRepo->create($productImahe);
        $createdProductImahe = $createdProductImahe->toArray();
        $this->assertArrayHasKey('id', $createdProductImahe);
        $this->assertNotNull($createdProductImahe['id'], 'Created ProductImahe must have id specified');
        $this->assertNotNull(ProductImahe::find($createdProductImahe['id']), 'ProductImahe with given id must be in DB');
        $this->assertModelData($productImahe, $createdProductImahe);
    }

    /**
     * @test read
     */
    public function test_read_product_imahe()
    {
        $productImahe = $this->makeProductImahe();
        $dbProductImahe = $this->productImaheRepo->find($productImahe->id);
        $dbProductImahe = $dbProductImahe->toArray();
        $this->assertModelData($productImahe->toArray(), $dbProductImahe);
    }

    /**
     * @test update
     */
    public function test_update_product_imahe()
    {
        $productImahe = $this->makeProductImahe();
        $fakeProductImahe = $this->fakeProductImaheData();
        $updatedProductImahe = $this->productImaheRepo->update($fakeProductImahe, $productImahe->id);
        $this->assertModelData($fakeProductImahe, $updatedProductImahe->toArray());
        $dbProductImahe = $this->productImaheRepo->find($productImahe->id);
        $this->assertModelData($fakeProductImahe, $dbProductImahe->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_product_imahe()
    {
        $productImahe = $this->makeProductImahe();
        $resp = $this->productImaheRepo->delete($productImahe->id);
        $this->assertTrue($resp);
        $this->assertNull(ProductImahe::find($productImahe->id), 'ProductImahe should not exist in DB');
    }
}
