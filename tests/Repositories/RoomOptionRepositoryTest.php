<?php namespace Tests\Repositories;

use App\Models\RoomOption;
use App\Repositories\RoomOptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeRoomOptionTrait;
use Tests\ApiTestTrait;

class RoomOptionRepositoryTest extends TestCase
{
    use MakeRoomOptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RoomOptionRepository
     */
    protected $roomOptionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->roomOptionRepo = \App::make(RoomOptionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_room_option()
    {
        $roomOption = $this->fakeRoomOptionData();
        $createdRoomOption = $this->roomOptionRepo->create($roomOption);
        $createdRoomOption = $createdRoomOption->toArray();
        $this->assertArrayHasKey('id', $createdRoomOption);
        $this->assertNotNull($createdRoomOption['id'], 'Created RoomOption must have id specified');
        $this->assertNotNull(RoomOption::find($createdRoomOption['id']), 'RoomOption with given id must be in DB');
        $this->assertModelData($roomOption, $createdRoomOption);
    }

    /**
     * @test read
     */
    public function test_read_room_option()
    {
        $roomOption = $this->makeRoomOption();
        $dbRoomOption = $this->roomOptionRepo->find($roomOption->id);
        $dbRoomOption = $dbRoomOption->toArray();
        $this->assertModelData($roomOption->toArray(), $dbRoomOption);
    }

    /**
     * @test update
     */
    public function test_update_room_option()
    {
        $roomOption = $this->makeRoomOption();
        $fakeRoomOption = $this->fakeRoomOptionData();
        $updatedRoomOption = $this->roomOptionRepo->update($fakeRoomOption, $roomOption->id);
        $this->assertModelData($fakeRoomOption, $updatedRoomOption->toArray());
        $dbRoomOption = $this->roomOptionRepo->find($roomOption->id);
        $this->assertModelData($fakeRoomOption, $dbRoomOption->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_room_option()
    {
        $roomOption = $this->makeRoomOption();
        $resp = $this->roomOptionRepo->delete($roomOption->id);
        $this->assertTrue($resp);
        $this->assertNull(RoomOption::find($roomOption->id), 'RoomOption should not exist in DB');
    }
}
