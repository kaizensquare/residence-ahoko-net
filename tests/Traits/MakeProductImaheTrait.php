<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\ProductImahe;
use App\Repositories\ProductImaheRepository;

trait MakeProductImaheTrait
{
    /**
     * Create fake instance of ProductImahe and save it in database
     *
     * @param array $productImaheFields
     * @return ProductImahe
     */
    public function makeProductImahe($productImaheFields = [])
    {
        /** @var ProductImaheRepository $productImaheRepo */
        $productImaheRepo = \App::make(ProductImaheRepository::class);
        $theme = $this->fakeProductImaheData($productImaheFields);
        return $productImaheRepo->create($theme);
    }

    /**
     * Get fake instance of ProductImahe
     *
     * @param array $productImaheFields
     * @return ProductImahe
     */
    public function fakeProductImahe($productImaheFields = [])
    {
        return new ProductImahe($this->fakeProductImaheData($productImaheFields));
    }

    /**
     * Get fake data of ProductImahe
     *
     * @param array $productImaheFields
     * @return array
     */
    public function fakeProductImaheData($productImaheFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'product_id' => $fake->randomDigitNotNull,
            'type' => $fake->randomDigitNotNull,
            'path' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $productImaheFields);
    }
}
