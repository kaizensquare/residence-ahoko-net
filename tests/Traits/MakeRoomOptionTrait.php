<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\RoomOption;
use App\Repositories\RoomOptionRepository;

trait MakeRoomOptionTrait
{
    /**
     * Create fake instance of RoomOption and save it in database
     *
     * @param array $roomOptionFields
     * @return RoomOption
     */
    public function makeRoomOption($roomOptionFields = [])
    {
        /** @var RoomOptionRepository $roomOptionRepo */
        $roomOptionRepo = \App::make(RoomOptionRepository::class);
        $theme = $this->fakeRoomOptionData($roomOptionFields);
        return $roomOptionRepo->create($theme);
    }

    /**
     * Get fake instance of RoomOption
     *
     * @param array $roomOptionFields
     * @return RoomOption
     */
    public function fakeRoomOption($roomOptionFields = [])
    {
        return new RoomOption($this->fakeRoomOptionData($roomOptionFields));
    }

    /**
     * Get fake data of RoomOption
     *
     * @param array $roomOptionFields
     * @return array
     */
    public function fakeRoomOptionData($roomOptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $roomOptionFields);
    }
}
