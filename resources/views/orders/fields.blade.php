<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Guest Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_guest', 'Is Guest:') !!}
    {!! Form::text('is_guest', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_email', 'Customer Email:') !!}
    {!! Form::text('customer_email', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer First Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_first_name', 'Customer First Name:') !!}
    {!! Form::text('customer_first_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Last Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_last_name', 'Customer Last Name:') !!}
    {!! Form::text('customer_last_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Coupon Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('coupon_code', 'Coupon Code:') !!}
    {!! Form::text('coupon_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Total Item Count Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total_item_count', 'Total Item Count:') !!}
    {!! Form::text('total_item_count', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::text('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Method Payement Field -->
<div class="form-group col-sm-6">
    {!! Form::label('method_payement', 'Method Payement:') !!}
    {!! Form::text('method_payement', null, ['class' => 'form-control']) !!}
</div>

<!-- Duration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('duration', 'Duration:') !!}
    {!! Form::text('duration', null, ['class' => 'form-control']) !!}
</div>

<!-- Ref Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ref_code', 'Ref Code:') !!}
    {!! Form::text('ref_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Confirm Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_confirm', 'Is Confirm:') !!}
    {!! Form::text('is_confirm', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Invoice Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_invoice', 'Id Invoice:') !!}
    {!! Form::text('id_invoice', null, ['class' => 'form-control']) !!}
</div>

<!-- Start Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_date', 'Start Date:') !!}
    {!! Form::text('start_date', null, ['class' => 'form-control']) !!}
</div>

<!-- End Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('end_date', 'End Date:') !!}
    {!! Form::text('end_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('orders.index') !!}" class="btn btn-default">Cancel</a>
</div>
