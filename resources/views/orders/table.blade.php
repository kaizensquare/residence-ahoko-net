<div class="table-responsive">
    <table class="table" id="orders-table">
        <thead>
            <tr>
        <th>Status</th>
        <th>Référence</th>
        <th>Date de Début</th>
        <th>Date de Fin</th>
        <th>Invité</th>
        <th>Email</th>
        <th>Nom et Prénom</th>
        <th>Téléphone</th>
        <th>Montant</th>
        <th>Mode de Paiement</th>
        <th>Durée</th>
        <!--th>Facture</th-->
        <th>Status paiement</th>
        <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($orders as $order)
            <tr>
            <td>
              @if($order->status == 1)
                  <span class="label label-warning"> En attente</span>
              @elseif($order->status == 4)
                  <span class="label label-success"> Commande Confirmé</span>
              @elseif($order->status == 3)
                  <span class="label label-info">Commande Terminé</span>
              @elseif($order->status == 2)
                  <span class="label label-success">Commande Démaré</span>
              @else
                  <span class="label label-danger">Commande Annulé</span>
              @endif
            </td>
            <td>{!! $order->ref_code !!}</td>
            <td>{!! $order->start_date !!}</td>
            <td>{!! $order->end_date !!}</td>
            <td>@if($order->is_guest == 1) Oui @else Non @endif</td>
            <td>{!! $order->customer_email !!}</td>
            <td>{!! $order->customer_first_name !!} {!! $order->customer_last_name !!}</td>
            <td>{!! $order->customer->phone !!}</td>
            <td>{{number_format($order->total_item_count)}} FCFA</td>
            <td>
              @if($order->method_payement == 3)
                <span class="label label-default">Paiement Cash</span>
              @endif
            </td>
            <td>{!! $order->duration !!} Nuit(s)</td>
            <!--td> <a href="#" class="btn btn-info">Voir la Facture</a> </td-->
            <td></td>
                <td>
                    {!! Form::open(['route' => ['orders.destroy', $order->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('orders.show', [$order->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('orders.edit', [$order->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
