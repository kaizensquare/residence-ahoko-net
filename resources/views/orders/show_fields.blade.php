

<div class="col-md-12">
  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Customer</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-condensed">
                <tbody><tr>
                  <th style="width: 10px">#</th>
                  <th style="width: 300px">Nom et Prénom</th>
                  <th style="width: 300px">Numéro de Téléphone</th>
                  <th style="width: 300px">Adresse Email</th>
                </tr>
                <tr>
                  <td></td>
                  <td>{!! $order->customer_first_name !!} {!! $order->customer_last_name !!}</td>
                  <td>
                    {!! $order->customer->phone !!}
                  </td>
                  <td>{!! $order->customer_email !!}</td>
                </tr>
              </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>


<div class="col-md-12">
  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Commandes</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-condensed">
                <tbody><tr>
                  <th style="width: 10px">#</th>
                  <th style="width: 10px">Status</th>
                  <th style="width: 300px">Référence</th>
                  <th style="width: 300px">Hébergement</th>
                  <th style="width: 300px">Room</th>
                  <th style="width: 300px">Quantité</th>
                  <th style="width: 300px">Durée</th>
                  <th style="width: 300px">Montant</th>
                  <th style="width: 300px">Date de Début</th>
                  <th style="width: 300px">Date de Fin</th>
                  <th style="width: 300px">Mode de paiement</th>
                  <th style="width: 300px">Date de commande</th>
                </tr>
                <tr>
                  <td></td>
                  <td>
                    @if($order->status == 1)
                        <span class="label label-danger"> En attente</span>
                    @elseif($order->status == 4)
                        <span class="label label-default"> Commande Confirmé</span>
                    @elseif($order->status == 3)
                        <span class="label label-default">Commande Terminé</span>
                    @elseif($order->status == 2)
                        <span class="label label-default">Commande Démaré</span>
                    @else
                        <span class="label label-default">Commande Annulé</span>
                    @endif
                  </td>
                  <td>{!! $order->ref_code !!}</td>
                  <td>{!! $order->room->product_one()->name !!}</td>
                  <td>{!! $order->room->type_chambre()->name !!}</td>
                  <td>1</td>
                  <td> <span class="label label-default">{!! $order->duration !!} Night</span> </td>
                  <td>{!! $order->total_item_count !!}</td>
                  <td>{!! $order->start_date !!}</td>
                  <td>{!! $order->end_date !!}</td>
                  <td>
                    @if($order->method_payement == 3)
                      <span class="label label-default">Paiement Cash</span>
                    @endif
                  </td>
                  <td>
                    {!! $order->created_at !!}
                  </td>
                </tr>
              </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
</div>

<div class="col-md-12">
  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manager commande</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if($order->status == 1)
              <a href="/admin/orders/{{$order->id}}/confirm-order" class="btn btn-success">
                <span style="font-size:16px; padding-bottom:10px">
                  <i class="fa  fa-check"></i> Valider la commande
                </span>
              </a>
              @elseif($order->status == 4)
              <a href="/admin/orders/{{$order->id}}/cancel-order" class="btn btn-danger">
                <span style="font-size:16px; padding-bottom:10px">
                  <i class="fa fa-remove"></i> Annuler la commande
                </span>
              </a>
              <a href="/admin/orders/{{$order->id}}/start-order" class="btn btn-warning">Démarer la commande</a>
              @elseif($order->status == 2)
              <a href="/admin/orders/{{$order->id}}/cancel-order" class="btn btn-danger">
                <span style="font-size:16px; padding-bottom:10px">
                  <i class="fa fa-remove"></i> Annuler la commande
                </span>
              </a>
              <a href="/admin/orders/{{$order->id}}/done" class="btn btn-warning">Terminer la commande</a>
              @endif
              <a class="btn btn-info">
                <span style="font-size:16px; padding-bottom:10px">
                  <i class="fa fa-file-archive-o"></i> Voir la facture
                </span>
              </a>
            </div>
            <!-- /.box-body -->
          </div>
</div>
