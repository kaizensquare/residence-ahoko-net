@extends('layouts.login')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card">

                <div class="card-body">

                  <div class="col-md-12">
                    <div class="card-title"><div id="logo">
                      <a href="/"><img src="{{asset('images/logo.png')}}" style="width:90px" alt=""></a>
                    </div> </div>


                    <div class="title" style="font-weight:bold; color:#000; font-size:25px;">{{ __('Créez un compte') }}</div>
                    <div class="desc mt-2">
                      <span>
                        Créez un compte afin de profiter des services Ahoko Residence en toute simplicité.
                      </span>
                    </div>
                  </div>


                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="col-md-12">
                          <div class="form-group">
                              <label for="name" class="col-form-label text-md-right">{{ __('Name') }}</label>
                                  <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                  @error('name')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror
                          </div>

                          <div class="form-group">
                              <label for="email" class="col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                  @error('email')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror
                          </div>

                          <div class="form-group">
                              <label for="password" class="col-form-label text-md-right">{{ __('Password') }}</label>

                                  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                  @error('password')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror
                          </div>
                          <div class="form-group">
                              <label for="password-confirm" class="col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                          </div>


                          <div class="form-group mb-0">
                                  <button type="submit" class="btn btn-success btn-lg btn-block">
                                      {{ __('Commencer') }}
                                  </button>
                          </div>
                        </div>


                    </form>
                </div>
            </div>

            <div class="card mt-4">
              <div class="card-body">
                <h5 class="card-title">Vous avez déjà un compte Ahoko Résidence ?</h5>
                <a href="{{ route('login') }}" class="card-link" style="font-weight:bold; font-size:20px">Se connecter</a>
              </div>
            </div>
        </div>


    </div>
</div>
@endsection
