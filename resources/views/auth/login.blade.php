@extends('layouts.login')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card">
                <div class="card-body">

                  <div class="col-md-12">
                    <div class="card-title"><div id="logo">
            					<a href="/"><img src="{{asset('images/logo.png')}}" style="width:90px" alt=""></a>
            				</div> </div>


                    <div class="title" style="font-weight:bold; color:#000; font-size:25px;">{{ __('Se connecter') }}</div>
                    <div class="desc mt-2">
                      <span>
                        Connectez-vous à l'aide de votre compte Ahoko Residence et accédez à nos services.
                      </span>
                    </div>
                  </div>

                    <form method="POST" action="{{ route('login') }}" class="mt-3">
                        @csrf


                        <div class="col-md-12">
                          <div class="form-group">
                              <label for="email" class="col-form-label text-md-right">{{ __('Adresse E-mail') }}</label>
                                  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                  @error('email')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group">
                              <label for="password" class="col-form-label text-md-right">{{ __('Mot de passe') }}</label>

                                  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                  @error('password')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror
                          </div>
                        </div>

                        <!--div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div-->

                        <div class="col-md-12">
                          <div class="form-group">
                                  <button type="submit" class="btn btn-success btn-lg btn-block">
                                      {{ __('Se connecter') }}
                                  </button>

                                  @if (Route::has('password.request'))
                                      <a class="btn btn-link" href="{{ route('password.request') }}">
                                          {{ __('Mot de passe oublié?') }}
                                      </a>
                                  @endif
                          </div>
                        </div>
                    </form>
                </div>
            </div>


            <div class="card mt-4">
              <div class="card-body">
                <h5 class="card-title">Vous n'avez pas encore de compte ?</h5>
                <p class="card-text">Créez un compte facilement afin de profiter des services ahokoresidence.com</p>
                <a href="{{ route('register') }}" class="card-link" style="font-weight:bold; font-size:20px">Créer un compte</a>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
