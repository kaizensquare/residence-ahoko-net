<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $customer->id !!}</p>
</div>

<!-- First Name Field -->
<div class="form-group">
    {!! Form::label('first_name', 'First Name:') !!}
    <p>{!! $customer->first_name !!}</p>
</div>

<!-- Last Name Field -->
<div class="form-group">
    {!! Form::label('last_name', 'Last Name:') !!}
    <p>{!! $customer->last_name !!}</p>
</div>

<!-- Titre Field -->
<div class="form-group">
    {!! Form::label('titre', 'Titre:') !!}
    <p>{!! $customer->titre !!}</p>
</div>

<!-- Date Of Birth Field -->
<div class="form-group">
    {!! Form::label('date_of_birth', 'Date Of Birth:') !!}
    <p>{!! $customer->date_of_birth !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $customer->email !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $customer->phone !!}</p>
</div>

<!-- Index Phone Field -->
<div class="form-group">
    {!! Form::label('index_phone', 'Index Phone:') !!}
    <p>{!! $customer->index_phone !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $customer->status !!}</p>
</div>

<!-- Subscribed To News Letter Field -->
<div class="form-group">
    {!! Form::label('subscribed_to_news_letter', 'Subscribed To News Letter:') !!}
    <p>{!! $customer->subscribed_to_news_letter !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $customer->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $customer->updated_at !!}</p>
</div>

