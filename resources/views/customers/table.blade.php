<div class="table-responsive">
    <table class="table" id="customers-table">
        <thead>
            <tr>
                <th>First Name</th>
        <th>Last Name</th>
        <th>Titre</th>
        <th>Date Of Birth</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Index Phone</th>
        <th>Status</th>
        <th>Subscribed To News Letter</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($customers as $customer)
            <tr>
                <td>{!! $customer->first_name !!}</td>
            <td>{!! $customer->last_name !!}</td>
            <td>{!! $customer->titre !!}</td>
            <td>{!! $customer->date_of_birth !!}</td>
            <td>{!! $customer->email !!}</td>
            <td>{!! $customer->phone !!}</td>
            <td>{!! $customer->index_phone !!}</td>
            <td>{!! $customer->status !!}</td>
            <td>{!! $customer->subscribed_to_news_letter !!}</td>
                <td>
                    {!! Form::open(['route' => ['customers.destroy', $customer->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('customers.show', [$customer->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('customers.edit', [$customer->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
