<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Titre:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Subtitle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subtitle', 'Sous Titre:') !!}
    {!! Form::text('subtitle', null, ['class' => 'form-control']) !!}
</div>


<!-- Description Field -->
<div class="form-group col-sm-6">
  <label for="">Description</label>
  <textarea class="textarea" name="description" rows="10" cols="80">@if(isset($feed)){{$feed->description}}@endif</textarea>
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    <input type="file" name="image_url" id="image_url" class="dropify-fr" data-default-file="" data-max-file-size="7M" />
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('feeds.index') !!}" class="btn btn-default">Annuler</a>
</div>
