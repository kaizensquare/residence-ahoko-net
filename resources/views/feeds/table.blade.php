<div class="table-responsive">
    <table class="table" id="feeds-table">
        <thead>
            <tr>
                <th>Title</th>
        <th>Subtitle</th>
        <th>Slug</th>
        <th>Description</th>
        <th>Image</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($feeds as $feed)
            <tr>
                <td>{!! $feed->title !!}</td>
            <td>{!! $feed->subtitle !!}</td>
            <td>{!! $feed->slug !!}</td>
            <td>{!! $feed->description !!}</td>
            <td>{!! $feed->image !!}</td>
                <td>
                    {!! Form::open(['route' => ['feeds.destroy', $feed->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('feeds.show', [$feed->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('feeds.edit', [$feed->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
