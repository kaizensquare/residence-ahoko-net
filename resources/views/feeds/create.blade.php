@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Blog - Actualité
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'feeds.store','files' => 'true','enctype'=>'multipart/form-data']) !!}

                        @include('feeds.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script>
  $(function () {
    $('.textarea').wysihtml5();

  })
</script>
@endsection
