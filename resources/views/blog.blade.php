@extends('layouts.master')

@section('content')

<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Blog</h2>
				<span>Restez à jour avec les dernières nouvelles</span>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="/">Accueil</a></li>
						<li>Blog</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>



<!-- Content
================================================== -->
<div class="container">

	<!-- Blog Posts -->
	<div class="blog-page">
	<div class="row">


		<!-- Post Content -->
		<div class="col-md-8">


			<!-- Blog Post -->
			<div class="blog-post single-post">

				<!-- Img -->
				<img class="post-img" src="{{$feed->image}}" alt="">


				<!-- Content -->
				<div class="post-content">
					<h3>{{$feed->title}}</h3>

					<ul class="post-meta">
						<li>Novemer 9, 2016</li>
					</ul>

          {!! $feed->description !!}


					<!-- Share Buttons -->
					<ul class="share-buttons margin-top-40 margin-bottom-0">
						<li><a class="fb-share" href="#"><i class="fa fa-facebook"></i> Partager</a></li>
						<li><a class="twitter-share" href="#"><i class="fa fa-twitter"></i> Tweet</a></li>
					</ul>
					<div class="clearfix"></div>

				</div>
			</div>
			<!-- Blog Post / End -->


			<div class="margin-top-50"></div>

	</div>
	<!-- Content / End -->



	<!-- Sidebar
	================================================== -->

	<!-- Widgets -->
	<div class="col-md-4">
		<div class="sidebar right">

			<!-- Widget -->
			<div class="widget">
				<h3>Vous avez des questions??</h3>
				<div class="info-box margin-bottom-10">
					<p>Si vous avez des questions, n'hésitez pas à demander.</p>
					<a href="/contact" class="button fullwidth margin-top-20"><i class="fa fa-envelope-o"></i> Écrivez-nous</a>
				</div>
			</div>
			<!-- Widget / End -->

			<!-- Widget -->
			<div class="widget">
				<h3 class="margin-top-0 margin-bottom-25">Social</h3>
				<ul class="social-icons rounded">
					<li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
				</ul>

			</div>
			<!-- Widget / End-->

			<div class="clearfix"></div>
			<div class="margin-bottom-40"></div>
		</div>
	</div>
	</div>
	<!-- Sidebar / End -->


</div>
</div>
@endsection
