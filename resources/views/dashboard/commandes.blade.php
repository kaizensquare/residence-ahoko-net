@extends('layouts.master')

@push('css')
<link rel="stylesheet" href="{{asset('css/dashboard.css')}}">
@endpush

@section('content')


<div class="container">
	<div class="row" style="margin-top: 35px;">

    <div class="col-md-12">
      <h3>Bonjour {{Auth::user()->name}}, heureux de vous rencontrer</h3>

    </div>
		<!-- Widget -->
		<div class="col-md-4" style="margin-top: 35px;">
			<div class="sidebar left">

				<div class="my-account-nav-container">

					<ul class="my-account-nav">
						<li class="sub-nav-title">Manage Account</li>
						<li><a href="/dashboard"><i class="sl sl-icon-home"></i> Accueil</a></li>
						<li><a href="/commandes" class="current"><i class="sl sl-icon-docs"></i> Mes commandes</a></li>
						<!--li><a href="#"><i class="sl sl-icon-star"></i> Mes Favoris</a></li-->
					</ul>

					<!--ul class="my-account-nav">
						<li class="sub-nav-title">Manage Listings</li>
						<li><a href="#"><i class="sl sl-icon-docs"></i> Mes Hébergements</a></li>
						<li><a href="#"><i class="sl sl-icon-action-redo"></i> Publier un Hébergement</a></li>
					</ul-->

					<ul class="my-account-nav">
            <!--li><a href="/settings"><i class="sl sl-icon-user"></i> Paramètres</a></l--i>
						<!--li><a href="#"><i class="sl sl-icon-lock"></i>Modifier Mot de passe</a></li-->
						<li>
							<a href="{!! url('/logout') !!}"
									onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									<i class="sl sl-icon-power"></i> Déconnexion
							</a>
							<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
							</form>

							</li>
					</ul>

				</div>

			</div>
		</div>

		<div class="col-md-8" style="margin-bottom: 85px;">

      <div class="row">
				<h3>Mes commandes</h3>
				<table class="table basic-table">
					<thead>
						<tr>
							<th>Status</th>
							<th>Réf.</th>
							<th>Héb.</th>
							<th>Room</th>
							<th>Qte</th>
							<th>Durée</th>
							<th>Montant</th>
							<th>Début</th>
							<th>Fin</th>
					</tr>
					</thead>
				<tbody>
				@if($commandes)
				@foreach($commandes as $order)
				<tr>
					<td>
						@if($order->status == 1)
								<span class="label label-warning"> En attente</span>
						@elseif($order->status == 4)
								<span class="label label-info"> Commande Confirmé</span>
						@elseif($order->status == 3)
								<span class="label label-success">Commande Terminé</span>
						@elseif($order->status == 2)
								<span class="label label-info">Commande Démaré</span>
						@else
								<span class="label label-danger">Commande Annulé</span>
						@endif
					</td>
					<td>{!! $order->ref_code !!}</td>
					<td>{!! $order->room->product_one()->name !!}</td>
					<td>{!! $order->room->type_chambre()->name !!}</td>
					<td>1</td>
					<td> <span class="label label-default">{!! $order->duration !!} Night</span> </td>
					<td>{{number_format($order->total_item_count)}} FCFA</td>
					<td>{!! $order->start_date !!}</td>
					<td>{!! $order->end_date !!}</td>
				</tr>
				@endforeach
				@endif

			</tbody></table>
      </div>

		</div>

	</div>
</div>
@endsection
