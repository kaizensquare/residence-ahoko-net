<li class="{{ Request::is('admin*') ? 'active' : '' }}">
    <a href="/admin"><i class="fa fa-dashboard"></i><span>Dashboard</span></a>
</li>


<li class="treeview">
  <a href="#">
    <i class="fa fa-folder"></i> <span>Hébergements</span>
    <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
    </span>
  </a>
  <ul class="treeview-menu" style="display: none;">
    <li class="{{ Request::is('products*') ? 'active' : '' }}"><a href="{!! route('products.index') !!}"><i class="fa fa-circle-o"></i> Hébergements</a></li>
    <li class="{{ Request::is('rooms*') ? 'active' : '' }}"><a href="{!! route('rooms.index') !!}"><i class="fa fa-circle-o"></i> Chambres</a></li>
    <li class="{{ Request::is('typeChambres*') ? 'active' : '' }}">
        <a href="{!! route('typeChambres.index') !!}"><i class="fa fa-circle-o"></i><span>Type Chambres</span></a>
    </li>
    <li class="{{ Request::is('typeBeds*') ? 'active' : '' }}">
        <a href="{!! route('typeBeds.index') !!}"><i class="fa fa-circle-o"></i><span>Type Lit</span></a>
    </li>
    <li class="{{ Request::is('optionItems*') ? 'active' : '' }}">
        <a href="{!! route('optionItems.index') !!}"><i class="fa fa-circle-o"></i><span>Caractéristique</span></a>
    </li>
  </ul>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>Utilisateurs</span></a>
</li>

<li class="{{ Request::is('orders*') ? 'active' : '' }}">
    <a href="{!! route('orders.index') !!}"><i class="fa fa-edit"></i><span>Commandes</span></a>
</li>
<li class="{{ Request::is('feeds*') ? 'active' : '' }}">
    <a href="{!! route('feeds.index') !!}"><i class="fa fa-edit"></i><span>Blog - Actualité</span></a>
</li>
<li class="{{ Request::is('customers*') ? 'active' : '' }}">
    <a href="{!! route('customers.index') !!}"><i class="fa fa-edit"></i><span>Clients</span></a>
</li>

<li class="{{ Request::is('invoices*') ? 'active' : '' }}">
    <a href="{!! route('invoices.index') !!}"><i class="fa fa-edit"></i><span>Factures</span></a>
</li>

<li class="{{ Request::is('suppliers*') ? 'active' : '' }}">
    <a href="{!! route('suppliers.index') !!}"><i class="fa fa-edit"></i><span>Proprietaires</span></a>
</li>
