<!DOCTYPE html>
<html>
<head>

<!-- Basic Page Needs
================================================== -->
<title>Ahoko Residence : 28 516 348 options residences et d'établissements dans le monde entier. Réservez maintenant !</title>
<meta charset="utf-8">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- CSS
================================================== -->
<link rel="stylesheet" href="{{asset('css/style.css')}}">
<link rel="stylesheet" href="{{asset('css/colors/green.css')}}" id="colors">
<link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
<link rel="stylesheet" href="{{asset('css/datepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('build/css/intlTelInput.css')}}">
<link rel="stylesheet" href="{{asset('build/css/intlTelInput.min.css')}}">
<link rel="stylesheet" href="{{asset('css/select2.css')}}">
<script type="text/javascript" src="{{asset('js/sweetalert.min.js')}}"></script>
</head>
@stack('css')
<style media="screen">
.label {
	display: inline;
	padding: .2em .6em .3em;
	font-size: 75%;
	font-weight: 700;
	line-height: 1;
	color: #fff;
	text-align: center;
	white-space: nowrap;
	vertical-align: baseline;
	border-radius: .25em;
}
.label-success {
    background-color: #5cb85c;
}
select {
    height: 47px ! important;
    line-height: 40px ! important;
    padding: 0 20px ! important;
    outline: none ! important;
    font-size: 15px ! important;
    color: gray ! important;
    margin: 0 0 16px ! important;
    max-width: 100% ! important;
    width: 100% ! important;
    box-sizing: border-box ! important;
    display: block ! important;
    background-color: #fcfcfc ! important;
    font-weight: 500 ! important;
    border: 1px solid #e0e0e0 ! important;
    opacity: 1 ! important;
    border-radius: 3px ! important;
}
.city-autocomplete {
    position: absolute;
    z-index: 10000;
    background: #fff;
    border: 1px solid #c3cacf;
    display: none;
    font-family: Arial, sans-serif;
    -webkit-box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2);
    -moz-box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2);
    box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2);
    -webkit-border-bottom-right-radius: 5px;
    -moz-border-radius-bottomright: 5px;
    border-bottom-right-radius: 5px;
    -webkit-border-bottom-left-radius: 5px;
    -moz-border-radius-bottomleft: 5px;
    border-bottom-left-radius: 5px;
}

.city-autocomplete > div {
    font-size: 16px;
    padding: 15px 10px;
		border-bottom: 1px solid #ccc;
    cursor: pointer;
}

.city-autocomplete > div:hover {
    background: #EEE;
}
.sb-autocomplete--photo {
    display: inline-block;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 30px;
    height: 30px;
    margin-right: 5px;
    border-radius: 3px;
    float: left;
}

.sb-autocomplete__item-with_photo {
    line-height: 30px;
}
#dropdown-user ul li a {
    line-height: 20px !important;
    padding: 10px 20px 10px 15px !important;
}
.divider {
    height: 0;
    margin: .5rem 0;
    overflow: hidden;
    border-top: 1px solid #e9ecef;
}
</style>
<body>
<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
================================================== -->
<header id="header-container">
	<div class="clearfix"></div>
	<!-- Topbar / End -->


	<!-- Header -->
	<div id="header">
		<div class="container">
			<!-- Left Side Content -->
			<div class="left-side">
				<!-- Logo -->
				<div id="logo">
					<a href="/"><img src="{{asset('images/logo.png')}}" alt=""></a>
				</div>

        <!-- Mobile Navigation -->
        <div class="mmenu-trigger">
          <button class="hamburger hamburger--collapse" type="button">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
            </span>
          </button>
        </div>

        <div class="clearfix"></div>
			</div>
			<!-- Left Side Content / End -->
			<!-- Right Side Content / End -->
			<div class="right-side">
				<!-- Header Widget -->
				<div class="header-widget" id="dropdown-user">
					@if (!Auth::guest())
					<div class="top-bar-dropdown" style="margin-top:15px">
						<span class="user_avatar user_avatar--circle ge-no-yellow-img_border">
							<img src="https://res.cloudinary.com/ahoko/image/upload/v1553539390/default-profile_kmtq5b.png" class="user-avatar-header-img img-circle" style="width:40px; height:40px;border:1px #DDD solid " alt="Photo de profil">
						</span>
							<span>{{Auth::user()->name}}</span>
							<ul class="options" style="text-align:left ! important">
								<li><div class="arrow"></div></li>
								<li><a href="/dashboard">Tableau de Bord</a></li>
								<li><a href="/commandes">Mes Commandes</a></li>
								<!--li><a href="/dashboard">Mes Favoris</a></li-->
								<li><a href="/settings">Paramètres</a></li>
								<li class="divider"></li>
								<li>
									<a href="{!! url('/logout') !!}"
											onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
											Déconnexion
									</a>
									<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
											{{ csrf_field() }}
									</form>
								</li>
							</ul>
						</div>
					@else
					<a href="{{ route('register') }}" class="sign-in"><i class="fa fa-user"></i> S'inscrire</a>
					<a href="{{ route('login') }}" class="button">Se connecter</a>
					@endif

          <!--a href="/add/etablissement" class="button border">Ajoutez votre établissement </a-->
				</div>
				<!-- Header Widget / End -->
			</div>
			<!-- Right Side Content / End -->
		</div>

    <!-- Topbar -->
    <div>
      <div class="container">
        <!-- Left Side Content -->
        <div class="left-side">
          <!-- Top bar -->
          <ul class="top-bar-menu">
            <li style="background-color: #f7f7f7; padding:10px"><a class="active" href="/" style="color:#444; font-size:14px">Hébergements</a> </li>
            <li><a href="http://ahoko.net/" target="_blank" style="background-color: rgba(255,255,255,0.1); color:#444; font-size:14px">Location de voiture</a></li>
          </ul>
        </div>
        <!-- Left Side Content / End -->
      </div>
    </div>
	</div>
	<!-- Header / End -->



</header>
<div class="clearfix"></div>
<!-- Header Container / End -->

@yield('content')


</div>
<!-- Footer
================================================== -->
<div id="footer" class="sticky-footer">
	<!-- Main --
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-6 ">
				<ul class="footer-links">
					<li><a href="#">Login</a></li>
					<li><a href="#">Sign Up</a></li>
					<li><a href="#">My Account</a></li>
					<li><a href="#">Add Property</a></li>
					<li><a href="#">Pricing</a></li>
					<li><a href="#">Privacy Policy</a></li>
				</ul>

				<ul class="footer-links">
					<li><a href="#">FAQ</a></li>
					<li><a href="#">Blog</a></li>
					<li><a href="#">Our Agents</a></li>
					<li><a href="#">How It Works</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
				<div class="clearfix"></div>
			</div>

      <div class="col-md-4 col-sm-6 ">
				<ul class="footer-links">
					<li><a href="#">Login</a></li>
					<li><a href="#">Sign Up</a></li>
					<li><a href="#">My Account</a></li>
					<li><a href="#">Add Property</a></li>
					<li><a href="#">Pricing</a></li>
					<li><a href="#">Privacy Policy</a></li>
				</ul>

				<ul class="footer-links">
					<li><a href="#">FAQ</a></li>
					<li><a href="#">Blog</a></li>
					<li><a href="#">Our Agents</a></li>
					<li><a href="#">How It Works</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
				<div class="clearfix"></div>
			</div>

			<div class="col-md-3  col-sm-12">
				<h4>Contact Us</h4>
				<div class="text-widget">
					<span>12345 Little Lonsdale St, Melbourne</span> <br>
					Phone: <span>(123) 123-456 </span><br>
					E-Mail:<span> <a href="#"><span class="__cf_email__" data-cfemail="036c65656a606643667b626e736f662d606c6e">[email&#160;protected]</span></a> </span><br>
				</div>

				<ul class="social-icons margin-top-20">
					<li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
					<li><a class="gplus" href="#"><i class="icon-gplus"></i></a></li>
					<li><a class="vimeo" href="#"><i class="icon-vimeo"></i></a></li>
				</ul>

			</div>

		</div>

		<!-- Copyright -->
		<div class="row">
			<div class="col-md-12">
				<div class="copyrights">© 2019 AHOKO. All Rights Reserved.</div>
			</div>
		</div>

	</div>

<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>

</div>
<!-- Wrapper / End -->

<!-- Scripts
================================================== -->
<!--script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js">

</script-->
<script type="text/javascript" src="{{asset('scripts/jquery-2.2.0.min.js')}}"></script>
<script type="text/javascript" src="{{asset('scripts/chosen.min.js')}}"></script>
<script type="text/javascript" src="{{asset('scripts/magnific-popup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('scripts/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('scripts/rangeSlider.js')}}"></script>
<script type="text/javascript" src="{{asset('scripts/sticky-kit.min.js')}}"></script>
<script type="text/javascript" src="{{asset('scripts/slick.min.js')}}"></script>
<script type="text/javascript" src="{{asset('scripts/masonry.min.js')}}"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{asset('scripts/mmenu.min.js')}}"></script>
<script type="text/javascript" src="{{asset('scripts/tooltips.min.js')}}"></script>
<script type="text/javascript" src="{{asset('scripts/custom.js')}}"></script>
<script type="text/javascript" src="{{asset('js/daterangepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('js/moment.js')}}"></script>
<script type="text/javascript" src="{{asset('js/select2.js')}}"></script>




<!-- Google Autocomplete -->
<script>
  function initAutocomplete() {
    var input = document.getElementById('autocomplete-input');
		if(input){
			var autocomplete = new google.maps.places.Autocomplete(input);

			autocomplete.addListener('place_changed', function() {
				var place = autocomplete.getPlace();
				if (!place.geometry) {
					window.alert("No details available for input: '" + place.name + "'");
					return;
				}
			});
		}

}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDqqwfoyJERekoo-c243pZUj4azUHqvR_U&amp;libraries=places&amp;callback=initAutocomplete"></script>

<!-- Style Switcher
================================================== -->
<script src="{{asset('scripts/switcher.js')}}"></script>

<script src="{{asset('build/js/intlTelInput.js')}}"></script>
<script src="{{asset('js/datepicker.min.js')}}"></script>
<script src="{{asset('js/city-autocomplete.js')}}"></script>
<script src="{{asset('js/jquery.geocomplete.js')}}"></script>
<script src="{{asset('scripts/maps.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.blockui.js')}}"></script>
  <script>

    var input = document.querySelector("#telephone");
		if(input){
			window.intlTelInput(input, {

				utilsScript: "{{asset('build/js/utils.js')}}",

			});
		}
		$(function(){
				$('[data-toggle="datepicker"]').datepicker({
					autoHide:true,
					language: 'fr-FR',
					format:'mm/dd/yyyy'
				});

				$("#address").geocomplete({
				  details: ".details",
				  detailsAttribute: "data-geo"
				});

				$('.select2').select2();


	  })

  </script>

<script>
var input = document.getElementById("autocompletecity");


if(input){
	$('input#autocompletecity').cityAutocomplete();
}
</script>

	@stack('js')

</body>
</html>
