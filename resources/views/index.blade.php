@extends('layouts.master')

@section('content')

@section('script')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

<!-- Banner
================================================== -->
<div class="parallax" data-background="{{asset('images/314-jir9189-jj.jpg')}}" data-color="#36383e" data-color-opacity="0.45" data-img-width="2500" data-img-height="1600">
  <div class="parallax-content">

    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <!-- Main Search Container -->
          <div class="main-search-container">
            <h2>Trouvez les meilleures offres d'hébergement</h2>

            <!-- Main Search -->
            <form method="get" action='/search' class="main-search-form">


              <!-- Box -->
              <div class="main-search-box">

                <!-- Main Search Input -->
                <div class="main-search-input larger-input">
                  <input id="autocompletecity" type="text" name="city" class="ico-01" placeholder="Ou allez vous ?" autocomplete="off" data-country="ci" required/>
                  <input type="hidden" name="place_id" id="geo_place_id"/>
                  <input type="hidden" name="geo_id" id="geo_id"/>
                </div>

                <!-- Row -->
                <div class="row with-forms">

                  <!-- Property Type -->
                  <div class="col-md-8">
                   <input type="text" name="daterange" value="{{date('m/d/Y')}}-{{date('m/d/Y', strtotime(date('m/d/Y').' +1 days'))}}">
                  </div>
                  <!-- Min Price --
                  <div class="col-md-3">

                    <!-- Select Input --
                    <div class="select-input">
                      <input type="number" placeholder="Personne(s)" maxlength="1" data-unit="">
                    </div>
                    <!-- Select Input / End --
                  </div>
                     -->
                  <!-- Max Price -->
                  <div class="col-md-4">
                    <button class="button">Rechercher</button>
                  </div>

                </div>
                <!-- Row / End -->
              </div>
              <!-- Box / End -->
            </form>
            <!-- Main Search -->
          </div>
          <!-- Main Search Container / End -->
        </div>
      </div>
    </div>

  </div>
</div>

<!-- Content
================================================== -->
<div class="container">
	<div class="row">

		<div class="col-md-12">
			<h3 class="headline margin-bottom-25 margin-top-65">Bien plus que des hôtels... Découvrez le confort de nos résidences</h3>
		</div>

		<!-- Carousel -->
		<div class="col-md-12">
			<div class="carousel">

        @if($products)
        @foreach($products as $product)
        @if($product && $product->default_room)
				<!-- Listing Item -->
					<div class="carousel-item">
					<div class="listing-item">

						<a href="/detail/{{$product->slug}}/{{base64_encode(date('m/d/Y'))}}/{{base64_encode(date('m/d/Y', strtotime(date('m/d/Y').' +1 days')))}}" class="listing-img-container">

							<div class="listing-img-content">
								<span class="listing-price">{{number_format($product->default_room()->price)}} FCFA <i>Jour</i></span>
								<!--span class="like-icon with-tip" data-tip-content="Ajouter au favoris"></span-->
							</div>

							<div class="listing-carousel">
                @if($product->product_images)
                @foreach($product->product_images as $value)
								<div><img src="{{$value->path}}" alt="" style="max-height:300px"></div>
                @endforeach
                @endif
							</div>

						</a>

						<div class="listing-content">

							<div class="listing-title">
								<h4><a href="/detail/{{$product->slug}}/{{base64_encode(date('m/d/Y'))}}/{{base64_encode(date('m/d/Y', strtotime(date('m/d/Y').' +1 days')))}}">{{$product->name}}</a></h4>
								<a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="listing-address popup-gmaps">
									<i class="fa fa-map-marker"></i>
									{{$product->address}}
								</a>
							</div>

							<ul class="listing-features">
								<li>Pièces <span>{{$product->quantity}}</span></li>
                <li>Chambres <span>{{$product->default_room()->quantity}}</span></li>
								<li>Salles de bain <span>{{$product->default_room()->quantity}}</span></li>
							</ul>

							<div class="listing-footer">
								<a href="#">Fabuleux</a>
								<!--span> 594 expériences vécues</span-->
							</div>

						</div>

					</div>
				</div>
				<!-- Listing Item / End -->
        @endif
        @endforeach
        @endif

				<!-- Listing Item --
				<div class="carousel-item">
					<div class="listing-item">

						<a href="/detail" class="listing-img-container">

							<div class="listing-img-content">
								<span class="listing-price">20,000 FCFA <i>mois</i></span>
								<span class="like-icon with-tip" data-tip-content="Add to Bookmarks"></span>
							</div>

							<img src="images/listing-02.jpg" alt="">

						</a>

						<div class="listing-content">

							<div class="listing-title">
								<h4><a href="/detail">Serene Uptown</a></h4>
								<a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="listing-address popup-gmaps">
									<i class="fa fa-map-marker"></i>
									Assinie
								</a>
							</div>

							<ul class="listing-features">
								<li>Area <span>4</span></li>
                <li>Chambres <span>2</span></li>
								<li>Salles de bain <span>2</span></li>
							</ul>

							<div class="listing-footer">
								<a href="#">Fabuleux</a>
                <span> 594 expériences vécues</span>
							</div>

						</div>

					</div>
				</div>
				<!-- Listing Item / End --


				<!-- Listing Item --
				<div class="carousel-item">
					<div class="listing-item">

						<a href="/detail" class="listing-img-container">

							<div class="listing-img-content">
								<span class="listing-price">25,000 FCFA <i>mois</i></span>
								<span class="like-icon with-tip" data-tip-content="Add to Bookmarks"></span>
							</div>

							<img src="images/listing-03.jpg" alt="">

						</a>

						<div class="listing-content">

							<div class="listing-title">
								<h4><a href="/detail">Meridian Villas</a></h4>
								<a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="listing-address popup-gmaps">
									<i class="fa fa-map-marker"></i>
									Assinie
								</a>
							</div>

							<ul class="listing-features">
								<li>Area <span>14</span></li>
                <li>Chambres <span>2</span></li>
								<li>Salles de bain <span>2</span></li>
							</ul>

							<div class="listing-footer">
								<a href="#"> Fabuleux</a>
                <span> 594 expériences vécues</span>
							</div>

						</div>
						<!-- Listing Item / End --

					</div>
				</div>
				<!-- Listing Item / End --


				<!-- Listing Item --
				<div class="carousel-item">
					<div class="listing-item">


						<a href="/detail" class="listing-img-container">

							<div class="listing-img-content">
								<span class="listing-price">40,000 FCFA <i> / Jour</i></span>
								<span class="like-icon with-tip" data-tip-content="Add to Bookmarks"></span>
							</div>

							<div class="listing-carousel">
								<div><img src="images/listing-04.jpg" alt=""></div>
								<div><img src="images/listing-04b.jpg" alt=""></div>
							</div>

						</a>

						<div class="listing-content">

							<div class="listing-title">
								<h4><a href="/detail">Selway Apartments</a></h4>
								<a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="listing-address popup-gmaps">
									<i class="fa fa-map-marker"></i>
									Assinie
								</a>
							</div>

							<ul class="listing-features">
								<li>Area <span>5</span></li>
								<li>Chambres <span>2</span></li>
								<li>Salles de bain <span>2</span></li>
							</ul>

							<div class="listing-footer">
								<a href="#">Fabuleux</a>
                <span> 594 expériences vécues</span>
							</div>

						</div>
						<!-- Listing Item / End --

					</div>
				</div>
				<!-- Listing Item / End --


				<!-- Listing Item --
				<div class="carousel-item">
					<div class="listing-item">


						<a href="single-property-page-1.html" class="listing-img-container">

							<div class="listing-img-content">
								<span class="listing-price">$535,000 <i>$640 / sq ft</i></span>
								<span class="like-icon with-tip" data-tip-content="Add to Bookmarks"></span>
							</div>

							<img src="images/listing-05.jpg" alt="">
						</a>

						<div class="listing-content">

							<div class="listing-title">
								<h4><a href="single-property-page-1.html">Oak Tree Villas</a></h4>
								<a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="listing-address popup-gmaps">
									<i class="fa fa-map-marker"></i>
									71 Lower River Dr. Bronx, NY
								</a>
							</div>

							<ul class="listing-features">
								<li>Area <span>3</span></li>
								<li>Bedrooms <span>2</span></li>
								<li>Bathrooms <span>1</span></li>
							</ul>

							<div class="listing-footer">
								<a href="#"><i class="fa fa-user"></i> Mabel Gagnon</a>
								<span><i class="fa fa-calendar-o"></i> 4 days ago</span>
							</div>

						</div>
						<!-- Listing Item / End --

					</div>
				</div>
				<!-- Listing Item / End -->



			</div>
		</div>
		<!-- Carousel / End -->

	</div>
</div>

<!-- Container -->
<div class="container">
	<div class="row">

		<div class="col-md-12">
			<h3 class="headline centered margin-bottom-35 margin-top-10">Lieux les plus populaires <span>Propriétés dans les endroits les plus populaires</span></h3>
		</div>

		<div class="col-md-8">

			<!-- Image Box -->
			<a href="/search?daterange={{date('m/d/Y')}}-{{date('m/d/Y', strtotime(date('m/d/Y').' +1 days'))}}" class="img-box" data-background-image="images/bassam.jpg">

				<!-- Badge -->
				<div class="listing-badges">
					<span class="featured">Featured</span>
				</div>

				<div class="img-box-content visible">
					<h4>Bassam</h4>
					<span>{{$bassam}} residences</span>
				</div>
			</a>

		</div>

		<div class="col-md-4">

			<!-- Image Box -->
			<a href="/search?daterange={{date('m/d/Y')}}-{{date('m/d/Y', strtotime(date('m/d/Y').' +1 days'))}}" class="img-box" data-background-image="images/yamoussoukro-principale.jpg">
				<div class="img-box-content visible">
					<h4>Yamoussokro</h4>
					<span>{{$yamoussokro}} residences</span>
				</div>
			</a>

		</div>

		<div class="col-md-8">

			<!-- Image Box -->
			<a href="/search?daterange={{date('m/d/Y')}}-{{date('m/d/Y', strtotime(date('m/d/Y').' +1 days'))}}" class="img-box" data-background-image="images/abidjan.jpg">
				<div class="img-box-content visible">
					<h4>Abidjan </h4>
					<span>{{$abidjan}} residences</span>
				</div>
			</a>

		</div>

		<div class="col-md-4">

			<!-- Image Box -->
			<a href="/search?daterange={{date('m/d/Y')}}-{{date('m/d/Y', strtotime(date('m/d/Y').' +1 days'))}}" class="img-box" data-background-image="images/sans-pedro.jpg">
				<div class="img-box-content visible">
					<h4>San Pedro</h4>
					<span>{{$sanspedro}} residences</span>
				</div>
			</a>

		</div>

	</div>
</div>
<!-- Container / End -->


<!-- Fullwidth Section -->
<section class="fullwidth margin-top-95 margin-bottom-0">

	<!-- Box Headline -->
	<h3 class="headline-box">Actualités</h3>

	<div class="container">
		<div class="row">

      @if($feeds)
        @foreach($feeds as $feed)
			<div class="col-md-4">

				<!-- Blog Post -->
				<div class="blog-post">

					<!-- Img -->
					<a href="/blog/{{$feed->slug}}" class="post-img">
						<img src="{{$feed->image}}" alt="">
					</a>

					<!-- Content -->
					<div class="post-content">
						<h3><a href="/blog/{{$feed->slug}}">{{$feed->title}}</a></h3>
						<a href="/blog/{{$feed->slug}}" class="read-more">Lire plus <i class="fa fa-angle-right"></i></a>
					</div>

				</div>
				<!-- Blog Post / End -->

			</div>
        @endforeach
      @endif


		</div>
	</div>
</section>
<!-- Fullwidth Section / End -->


<!-- Flip banner --
<a href="#" class="flip-banner parallax" data-background="images/flip-banner-bg.jpg" data-color="#8ebf57">
	<div class="flip-banner-content">
		<h2 class="">Ajouter votre etablissement <i class="sl sl-icon-arrow-right"></i></h2>
	</div>
</a>
<!-- Flip banner / End -->

<script type="text/javascript" src="{{asset('scripts/jquery-2.2.0.min.js')}}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript"></script>
<script>
$(function() {
  $('input[name="daterange"]').daterangepicker({
    opens: 'left',
    format: 'dd/mm/yyyy',
  }, function(start, end, label) {
    console.log(start);
    console.log(end);
    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });
});
</script>


@endsection
