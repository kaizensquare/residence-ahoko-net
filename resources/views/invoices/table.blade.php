<div class="table-responsive">
    <table class="table" id="invoices-table">
        <thead>
            <tr>
                <th>Reference</th>
        <th>Id Editor</th>
        <th>Link</th>
        <th>Order Id</th>
        <th>Payment Method</th>
        <th>Win From The Owner</th>
        <th>Customer Fees</th>
        <th>Win Of Ahoko</th>
        <th>Customer Payed</th>
        <th>Payed To Owner</th>
        <th>Status</th>
        <th>Archived</th>
        <th>Customer Transaction Reference</th>
        <th>Owner Transaction Reference</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($invoices as $invoice)
            <tr>
            <td>{!! $invoice->reference !!}</td>
            <td>{!! $invoice->id_editor !!}</td>
            <td> <a target="_blank" href="{!! $invoice->link !!}" class="btn btn-primary">Facture</a> </td>
            <td>{!! $invoice->order_id !!}</td>
            <td>{!! $invoice->payment_method !!}</td>
            <td>{!! $invoice->win_from_the_owner !!}</td>
            <td>{!! $invoice->customer_fees !!}</td>
            <td>{!! $invoice->win_of_ahoko !!}</td>
            <td>{!! $invoice->customer_payed !!}</td>
            <td>{!! $invoice->payed_to_owner !!}</td>
            <td>{!! $invoice->status !!}</td>
            <td>{!! $invoice->archived !!}</td>
            <td>{!! $invoice->customer_transaction_reference !!}</td>
            <td>{!! $invoice->owner_transaction_reference !!}</td>
                <td>
                    {!! Form::open(['route' => ['invoices.destroy', $invoice->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('invoices.show', [$invoice->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('invoices.edit', [$invoice->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
