<!-- Reference Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reference', 'Reference:') !!}
    {!! Form::text('reference', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Editor Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_editor', 'Id Editor:') !!}
    {!! Form::text('id_editor', null, ['class' => 'form-control']) !!}
</div>

<!-- Link Field -->
<div class="form-group col-sm-6">
    {!! Form::label('link', 'Link:') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<!-- Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_id', 'Order Id:') !!}
    {!! Form::text('order_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Method Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_method', 'Payment Method:') !!}
    {!! Form::text('payment_method', null, ['class' => 'form-control']) !!}
</div>

<!-- Win From The Owner Field -->
<div class="form-group col-sm-6">
    {!! Form::label('win_from_the_owner', 'Win From The Owner:') !!}
    {!! Form::text('win_from_the_owner', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Fees Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_fees', 'Customer Fees:') !!}
    {!! Form::text('customer_fees', null, ['class' => 'form-control']) !!}
</div>

<!-- Win Of Ahoko Field -->
<div class="form-group col-sm-6">
    {!! Form::label('win_of_ahoko', 'Win Of Ahoko:') !!}
    {!! Form::text('win_of_ahoko', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Payed Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_payed', 'Customer Payed:') !!}
    {!! Form::text('customer_payed', null, ['class' => 'form-control']) !!}
</div>

<!-- Payed To Owner Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payed_to_owner', 'Payed To Owner:') !!}
    {!! Form::text('payed_to_owner', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Archived Field -->
<div class="form-group col-sm-6">
    {!! Form::label('archived', 'Archived:') !!}
    {!! Form::text('archived', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Transaction Reference Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_transaction_reference', 'Customer Transaction Reference:') !!}
    {!! Form::text('customer_transaction_reference', null, ['class' => 'form-control']) !!}
</div>

<!-- Owner Transaction Reference Field -->
<div class="form-group col-sm-6">
    {!! Form::label('owner_transaction_reference', 'Owner Transaction Reference:') !!}
    {!! Form::text('owner_transaction_reference', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('invoices.index') !!}" class="btn btn-default">Cancel</a>
</div>
