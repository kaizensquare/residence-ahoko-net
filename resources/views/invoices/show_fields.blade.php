<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $invoice->id !!}</p>
</div>

<!-- Reference Field -->
<div class="form-group">
    {!! Form::label('reference', 'Reference:') !!}
    <p>{!! $invoice->reference !!}</p>
</div>

<!-- Id Editor Field -->
<div class="form-group">
    {!! Form::label('id_editor', 'Id Editor:') !!}
    <p>{!! $invoice->id_editor !!}</p>
</div>

<!-- Link Field -->
<div class="form-group">
    {!! Form::label('link', 'Link:') !!}
    <p>{!! $invoice->link !!}</p>
</div>

<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{!! $invoice->order_id !!}</p>
</div>

<!-- Payment Method Field -->
<div class="form-group">
    {!! Form::label('payment_method', 'Payment Method:') !!}
    <p>{!! $invoice->payment_method !!}</p>
</div>

<!-- Win From The Owner Field -->
<div class="form-group">
    {!! Form::label('win_from_the_owner', 'Win From The Owner:') !!}
    <p>{!! $invoice->win_from_the_owner !!}</p>
</div>

<!-- Customer Fees Field -->
<div class="form-group">
    {!! Form::label('customer_fees', 'Customer Fees:') !!}
    <p>{!! $invoice->customer_fees !!}</p>
</div>

<!-- Win Of Ahoko Field -->
<div class="form-group">
    {!! Form::label('win_of_ahoko', 'Win Of Ahoko:') !!}
    <p>{!! $invoice->win_of_ahoko !!}</p>
</div>

<!-- Customer Payed Field -->
<div class="form-group">
    {!! Form::label('customer_payed', 'Customer Payed:') !!}
    <p>{!! $invoice->customer_payed !!}</p>
</div>

<!-- Payed To Owner Field -->
<div class="form-group">
    {!! Form::label('payed_to_owner', 'Payed To Owner:') !!}
    <p>{!! $invoice->payed_to_owner !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $invoice->status !!}</p>
</div>

<!-- Archived Field -->
<div class="form-group">
    {!! Form::label('archived', 'Archived:') !!}
    <p>{!! $invoice->archived !!}</p>
</div>

<!-- Customer Transaction Reference Field -->
<div class="form-group">
    {!! Form::label('customer_transaction_reference', 'Customer Transaction Reference:') !!}
    <p>{!! $invoice->customer_transaction_reference !!}</p>
</div>

<!-- Owner Transaction Reference Field -->
<div class="form-group">
    {!! Form::label('owner_transaction_reference', 'Owner Transaction Reference:') !!}
    <p>{!! $invoice->owner_transaction_reference !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $invoice->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $invoice->updated_at !!}</p>
</div>

