@extends('layouts.master')

@section('content')
<!-- Content
================================================== -->
<div class="container" style="margin-top:30px">
  <div class="row sticky-wrapper">


    <!-- Sidebar
    ================================================== -->
    <div class="col-md-4">
      <div class="sidebar sticky right">

        <!-- Widget -->
        <div class="widget margin-bottom-40">
          <h3 class="margin-top-0 margin-bottom-25">Rechercher</h3>

            <form class="" action="/search" method="get">
              <!-- Row -->
              <div class="row with-forms">
                <!-- Status -->
                <div class="col-md-12">
                  <label for="">Destination</label>
                  <input id="autocompletecity" type="text" name="city" class="ico-01" placeholder="Ou allez vous ?" autocomplete="OFF" data-country="ci" value="{{$city}}" required/>
                  <input type="hidden" name="place_id" id="geo_place_id" value="{{$place_id}}"/>
                  <input type="hidden" name="geo_id" id="geo_id"/>
                </div>
              </div>
              <!-- Row / End -->


              <!-- Row -->
              <div class="row with-forms">
                <!-- Type -->
                <div class="col-md-12">
                  <label for="">Du</label>
                  <input type="text" class="ico-01" name="start_date" data-toggle="datepicker" placeholder="" value="@if($date_time_from) {{date('m/d/Y', strtotime($date_time_from))}} @else {{date('m/d/Y')}} @endif" required>
                </div>
              </div>
              <!-- Row / End -->

              <!-- Row -->
              <div class="row with-forms">
                <!-- Type -->
                <div class="col-md-12">
                  <label for="">Au</label>
                  <input type="text" class="ico-01" name="end_date" data-toggle="datepicker" placeholder="" value="@if($date_time_from) {{date('m/d/Y', strtotime($date_time_to))}} @else {{date('m/d/Y', strtotime(date('m/d/Y').' +1 days'))}} @endif" required>
                </div>
              </div>
              <!-- Row / End -->


              <!-- Row --
              <div class="row with-forms">
                <!-- States --
                <div class="col-md-12">
                   <select class="chosen-select" >
                    <option>1 adulte</option>
                    <option selected>2 adultes</option>
                    <option>3 adultes</option>
                    <option>4 adultes</option>
                    <option>5 adultes</option>
                    <option>6 adultes</option>
                    <option>7 adultes</option>
                    <option>8 adultes</option>
                    <option>9 adultes</option>
                    <option>10 adultes</option>
                  </select>
                </div>
              </div>
              <!-- Row / End -->
              <!-- Row --
              <div class="row with-forms">
                <!-- Min Area --
                <div class="col-md-6">
                  <select class="chosen-select-no-single" >
                    <option selected>Aucun enfant</option>
                    <option>1 enfant</option>
                    <option>2 enfants</option>
                    <option>3 enfants</option>
                    <option>4 enfants</option>
                    <option>5 enfants</option>
                  </select>
                </div>

                <!-- Max Area --
                <div class="col-md-6">
                  <select class="chosen-select-no-single" >
                    <option>1 chambre</option>
                    <option>2 chambres</option>
                    <option>3 chambres</option>
                    <option>4 chambres</option>
                    <option>5 chambres</option>
                  </select>
                </div>

              </div>
              <!-- Row / End -->
              <button type="submit" class="button fullwidth margin-top-30">Search</button>

            </form>

            <br>
            <h3>Filtrer Par</h3>
            <!-- Price Range -->
            <div class="range-slider">
              <label>Échelle des prix</label>
              <div id="price-range" data-min="0" data-max="500000" data-unit="CFA "></div>
              <div class="clearfix"></div>
            </div>



            <!-- More Search Options --
            <a href="#" class="more-search-options-trigger margin-bottom-10 margin-top-30" data-open-title="Caractéristiques" data-close-title="Caractéristiques"></a>

            <div class="more-search-options relative">

              <!-- Checkboxes --
              <div class="checkboxes one-in-row margin-bottom-10">
                @if($option_items)
                  @foreach($option_items as $item)
                    <input id="check-{{$item->id}}" type="checkbox" name="check" value="{{$item->id}}">
                    <label for="check-{{$item->id}}">{{$item->name}}</label>
                  @endforeach
                @endif
              </div>
              <!-- Checkboxes / End --

            </div>
            <!-- More Search Options / End -->

        </div>
        <!-- Widget / End -->

      </div>
    </div>
    <!-- Sidebar / End -->

    <div class="col-md-8">

      <!-- Sorting / Layout Switcher -->
      <div class="row margin-bottom-15">

        <h3 class="margin-top-0">{{$city_first_value}} : {{count($rooms)}} établissements trouvés </h3>
        <p>3 bonnes raisons d'y aller  : <strong>exploration de la ville, historiques et visites ! </strong> </p>

        <div class="col-md-6">
          <!-- Sort by -->
          <div class="sort-by">
            <label>Filtrer par:</label>

            <div class="sort-by-select"  style="margin-left:15px">
              <select data-placeholder="Default order" id="order_select" class="form-control" >
                <option>Défaut</option>
                <option value="asc">Prix ​​croissant</option>
                <option value="desc">Prix ​​décroissant</option>
                <!--option>Nouvelles propriétés</option>
                <option>Les plus anciennes propriétés</option-->
              </select>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <!-- Layout Switcher -->
          <div class="layout-switcher">
            <a href="#" class="list"><i class="fa fa-th-list"></i></a>
            <a href="#" class="grid"><i class="fa fa-th-large"></i></a>
          </div>
        </div>
      </div>


      <!-- Listings -->
      <div class="listings-container list-layout" id="content-listings-data">

        @if($rooms)
          @foreach($rooms as $room)
          @if($room && $room->product_one() != null)
          <!-- Listing Item -->
          <div class="listing-item">

            <a href="/detail/{{$room->product_one()->slug}}/{{base64_encode($date_time_from)}}/{{base64_encode($date_time_to)}}" class="listing-img-container" style="height: 280px ! important;">

              <!--div class="listing-badges">
                <span class="featured">Featured</span>
              </div-->

              <div class="listing-img-content">
                <span class="listing-price">{{number_format($room->product_one()->default_room()->price)}} FCFA <i>jour</i></span>
                <!--span class="like-icon with-tip" data-tip-content="Add to Bookmarks"></span-->
              </div>

              <div class="listing-carousel">
              @if($room->product_one()->product_images)
                @foreach($room->product_one()->product_images as $value)
                <div><img src="{{$value->path}}" alt="" style="max-height:350px;"></div>
               @endforeach
              @endif
              </div>


            </a>

            <div class="listing-content">

              <div class="listing-title">
                <h4><a href="/detail/{{$room->product_one()->slug}}/{{base64_encode($date_time_from)}}/{{base64_encode($date_time_to)}}"></a>{{$room->product_one()->name}}</h4>
                <a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="listing-address popup-gmaps">
                  <i class="fa fa-map-marker"></i>
                  {{$room->product_one()->address}}
                </a>

                <a href="/detail/{{$room->product_one()->slug}}/{{base64_encode($date_time_from)}}/{{base64_encode($date_time_to)}}" class="details button border">Details</a>
              </div>

              <ul class="listing-details">
                <li>Pièces <span>{{$room->product_one()->quantity}}</span></li>
                <li>Chambres <span>{{$room->quantity}}</span></li>
								<li>Salles de bain <span>{{$room->quantity}}</span></li>
              </ul>

              <!--div class="listing-footer">
                <span style="color:red; font-size:12px">Forte demande plus que 5 restants</span>
                <a href="#"> David Strozier</a>

              </div-->

            </div>
            <!-- Listing Item / End -->

          </div>
          <!-- Listing Item / End -->
          @endif
        @endforeach
        @endif


      </div>
      <!-- Listings Container / End -->


      <!-- Pagination --
      <div class="pagination-container margin-top-20">
        <nav class="pagination">
          <ul>
            <li><a href="#" class="current-page">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li class="blank">...</li>
            <li><a href="#">22</a></li>
          </ul>
        </nav>

        <nav class="pagination-next-prev">
          <ul>
            <li><a href="#" class="prev">Previous</a></li>
            <li><a href="#" class="next">Next</a></li>
          </ul>
        </nav>
      </div>
      <!-- Pagination / End -->

    </div>
  </div>
</div>

@endsection

@push('js')
<script type="text/javascript">
  $('#price-range').change(function(){
    console.log($(this).val());
  })
</script>
@endpush
