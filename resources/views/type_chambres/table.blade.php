<div class="table-responsive">
    <table class="table" id="typeChambres-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($typeChambres as $typeChambre)
            <tr>
                <td>{!! $typeChambre->name !!}</td>
            <td>{!! $typeChambre->status !!}</td>
                <td>
                    {!! Form::open(['route' => ['typeChambres.destroy', $typeChambre->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('typeChambres.show', [$typeChambre->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('typeChambres.edit', [$typeChambre->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
