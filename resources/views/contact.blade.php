@extends('layouts.master')

@section('content')

<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Contact</h2>
				<span>Si vous avez des questions, n'hésitez pas à demander.</span>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="/">Accueil</a></li>
						<li>Contact</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>



<div class="clearfix"></div>
<div class="margin-top-50"></div>
<div class="container">

	<div class="row">

		<!-- Contact Details -->
		<div class="col-md-4">

			<h4 class="headline margin-bottom-30">Nous trouver là</h4>

			<!-- Contact Details -->
			<div class="sidebar-textbox">

				<ul class="contact-details">
					<li><i class="im im-icon-Globe"></i> <strong>Web:</strong> <span><a href="#">residence.ahoko.net</a></span></li>
					<li><i class="im im-icon-Envelope"></i> <strong>E-Mail:</strong> <span><a href="#">info@ahoko.net</a></span></li>
				</ul>
			</div>

		</div>

		<!-- Contact Form -->
		<div class="col-md-8">
      @if (\Session::has('message'))
        <div class="alert alert-info" style="margin-top:30px">
            <ul>
                <li>{!! \Session::get('message') !!}</li>
            </ul>
        </div>
      @endif
			<section id="contact">

				<h4 class="headline margin-bottom-35">Contact</h4>

				<div id="contact-message"></div>

					<form method="post" action="/send/contact"  autocomplete="on">
            @csrf
					<div class="row">
						<div class="col-md-6">
							<div>
								<input name="name" type="text" id="name" placeholder="Votre nom" required="required">
							</div>
						</div>

						<div class="col-md-6">
							<div>
								<input name="email" type="email" id="email" placeholder="Adresse Email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" required="required">
							</div>
						</div>
					</div>

					<div>
						<input name="subject" type="text" id="subject" placeholder="Objet" required="required">
					</div>

					<div>
						<textarea name="comments" cols="40" rows="3" id="comments" placeholder="Message" spellcheck="true" required="required"></textarea>
					</div>

					<input type="submit" class="submit button" id="submit" value="Envoyer votre Message">

					</form>
			</section>
		</div>
		<!-- Contact Form / End -->

	</div>

</div>
@endsection
