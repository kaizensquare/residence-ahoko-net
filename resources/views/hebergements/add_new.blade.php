@extends('layouts.master')

@push('css')
<style media="screen">
@media only screen and (max-width: 1366px) and (min-width: 1240px){
  .search-container {
      padding: 80px 0;
  }
}
  .search-container h2{
    margin-bottom: 0px ! important;
  }

  .panel-head {
    padding: 10px 15px;
}
</style>
@endpush

@section('content')

<!-- Banner
================================================== -->
<div class="parallax" data-background="{{asset('images/258-ted5641-eye.jpg')}}" data-color="#444" data-color-opacity="0.5"
data-img-width="2500"
data-img-height="1600">

	<div class="container">
		<div class="row">
			<div class="col-md-7">

				<div class="search-container">
          <h2>Inscrivez votre</h2>
          <h2>votre hébergement</h2>
          <h2>sur Ahoko Résidence</h2>
				</div>
			</div>
      <div class="col-md-5">
        <div class="search-container">
          <div class="card">
            <div class="panel panel-default">
               <div class="panel-head">
                 <h3 class="panel-title">Inscrire un autre établissement</h3>
               </div>
              <div class="panel-body">
                  <form class="" action="index.html" method="post">
                    <div class="form-group">
                      <label for="name">Vos prénom et nom</label>
                      <input type="text" class="form-control" id="name" placeholder="">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email address</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
                    </div>
                    <button type="submit" class="button">Commencer</button>
                  </form>
              </div>
            </div>
        </div>
        </div>
		</div>
	</div>

</div>
</div>
<!-- Content
================================================== -->
<!-- Fullwidth Section -->
<section class="fullwidth border-bottom margin-top-0 margin-bottom-0 padding-top-50 padding-bottom-50"
data-background-color="#ffffff">
<!-- Content -->
<div class="container">
  <div class="row">
    <div class="col-md-4">
      <!-- Icon Box -->
      <div class="icon-box-1 alternative">
        <div class="icon-container">
          <i class="im im-icon-Checked-User"></i>
        </div>
        <h3>Vous gérez vous-même vos disponibilités</h3>
        <p></p>
      </div>
    </div>
    <div class="col-md-4">
      <!-- Icon Box -->
      <div class="icon-box-1 alternative">
        <div class="icon-container">
          <i class="im im-icon-Checked-User"></i>
        </div>
        <h3>Les clients ne paient pas de frais</h3>
        <p></p>
      </div>
    </div>
    <div class="col-md-4">
      <!-- Icon Box -->
      <div class="icon-box-1 alternative">
        <div class="icon-container">
          <i class="im im-icon-Checked-User"></i>
        </div>
        <h3>Utilisez Ahoko Résidence en complément d'autres plateformes</h3>
        <p></p>
      </div>
    </div>
  </div>
</div>
</section>


@endsection

@push('js')

<script type="text/javascript" src="{{asset('scripts/jquery-2.2.0.min.js')}}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript"></script>
<script>
$(function() {
  $('input[name="daterange"]').daterangepicker({
    opens: 'left',
    format: 'dd/mm/yyyy',
  }, function(start, end, label) {
    console.log(start);
    console.log(end);
    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });
});
</script>

@endpush
