@extends('layouts.appbar')

@push('css')
<style media="screen">
  .sw-theme-default{
    -webkit-box-shadow: none ! important;
  	-moz-box-shadow: none  ! important;
  	box-shadow: none  ! important;
  }
  .sw-theme-default .sw-toolbar{
    background-color: transparent ! important;
  }
  .sw-theme-default .step-content{
    background-color: transparent;
  }
  .qq-gallery .qq-upload-button{
    width: 205px ! important;
  }
</style>
@endpush

@section('content')
<div class="container-fluid">

        <!-- SmartWizard html -->
        <div id="smartwizard">
            <ul>
                <li><a href="#step-1">Général<br /></a></li>
                <li><a href="#step-2">Disposition et Tarif<br /></a></li>
                <li><a href="#step-3">Équipements et services<br /></a></li>
                <li><a href="#step-4">Équipements<br /></a></li>
                <li><a href="#step-5">Photos<br /></a></li>
                <li><a href="#step-6">Conditions<br /></a></li>
                <li><a href="#step-7">Paiements<br /></a></li>
                <li><a href="#step-8">Accord<br /></a></li>
            </ul>

            <div>
                <div id="step-1" class="mt-4">
                    <div class="row">
                      <div class="col-md-8">
                        <h6 class="border-bottom border-gray pb-2">Commencez par nous indiquer le nom de votre établissement, son adresse et vos coordonnées.</h6>

                        <div class="card mt-4">
                          <div class="card-body">
                              <div class="form-group">
                                <label for="pays">Quel est le type de votre établissement ?</label>
                                <select class="form-control" name="type_hebergement">
                                  <option value="">Veuillez sélectionner</option>
                                  <option value="1">Hôtel  (chambre)</option>
                                  <option value="2">Résidence (Entier)</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="pays">Quel est le nom de votre établissement ?</label>
                                <input type="text" class="form-control" name="hebergement_name" id="hebergement_name" placeholder=""/>
                                <small id="hebergement_nameHelp" class="form-text text-muted">Il s'agit du nom que les clients verront lorsqu'ils rechercheront un hébergement.</small>
                              </div>
                            </div>
                          </div>
                          <div class="card mt-4">
                            <div class="card-body">
                                <h5 style="font-weight:normal">Quelles sont les informations de contact pour cet établissement ?</h5>
                                <div class="form-group">
                                  <label for="owner_name">Nom du contact</label>
                                  <input type="text" class="form-control" name="owner_name" id="owner_name" placeholder=""/>
                                </div>
                                <div class="form-group">
                                  <label for="owner_phone">Numéro de téléphone (pour que nous puissions vous aider avec votre inscription si nécessaire)</label>
                                  <input type="tel" class="form-control" name="owner_phone" id="owner_phone" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="owner_multiple_appartement">Êtes-vous propriétaire de plusieurs appartements ou faites-vous partie d'une agence ou d'un groupe immobiliers ?</label>
                                  <select class="form-control" name="owner_multiple_appartement">
                                    <option value="1">OUI</option>
                                    <option value="0">NON</option>
                                  </select>
                                </div>
                              </div>
                            </div>

                        <div class="card mt-4">
                          <div class="card-body">
                              <h5 style="font-weight:normal">Où se trouve votre établissement ?</h5>
                              <div class="form-group">
                                <label for="ville">Ville</label>
                                <input type="text" class="form-control" id="ville" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="pays">Pays / Région</label>
                                <select class="form-control" name="">
                                  <option value="110">Côte-d'Ivoire</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="address">Trouvez votre adresse</label>
                                <input type="address" class="form-control" id="address" placeholder="Commencez à saisir votre adresse">
                                <div class="details">
                                  <input type="hidden" name="lat_address" data-geo="lat" />
                                  <input type="hidden" name="lng_address" data-geo="lng" />
                                  <input type="hidden" name="formatted_address" data-geo="formatted_address" />
                                  <input type="hidden" name="country_short_address" data-geo="country_short" />
                                  <input type="hidden" name="address_place_id" data-geo="place_id" />
                                </div>
                              </div>
                            </div>
                          </div>

                      </div>
                      <div class="col-md-4">
                        <div class="card mt-4">
                          <div class="card-body">
                            Une fois votre inscription terminée, vous pourrez apporter des modifications à votre établissement avant sa mise en ligne.
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <div id="step-2" class="">
                  <div class="row">
                    <div class="col-md-8">
                        <h3 class="border-bottom border-gray pb-2">Disposition et tarif</h3>
                        <div class="card mt-4">
                          <div class="card-body">
                                <h5 style="font-weight:normal">Appartement</h5>
                                <div class="mt-4">
                                  <div class="row">
                                    <div class="col">
                                      <label for="">Nom de l'etablissement</label>
                                        <select class="form-control" name="type_hebergement">
                                          <option value="">Veuillez sélectionner</option>
                                        </select>
                                    </div>
                                  </div>
                                  <div class="row mt-4">
                                    <div class="col">
                                      <label for="">Nombre de chambres</label>
                                      <select class="form-control" name="nbre_chambre">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                      </select>
                                    </div>
                                    <div class="col">
                                      <label for="">Nombre de salons</label>
                                      <select class="form-control" name="nbre_salons">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                      </select>
                                    </div>
                                    <div class="col">
                                      <label for="">Nombre de salles de bains</label>
                                      <select class="form-control" name="nbre_salle_bains">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="row mt-4">
                                    <div class="col">
                                      <label for="">Nombre d’appartements (de ce type)</label>
                                      <input type="text" class="form-control" name="nmbre_type_hebergement" value="1">
                                    </div>
                                  </div>
                                </div>
                          </div>
                        </div>


                        <div class="card mt-2">
                          <div class="card-body">
                            <h5 style="font-weight:normal">Literie</h5>
                            <div class="row mt-4">
                              <div class="col">
                                <label for="">Quels types de lits sont disponibles dans cet hébergement ?</label>
                                  <select class="form-control" name="">
                                  </select>
                                <select class="form-control mt-4" name="">
                                  <option value="">Sélectionnez le nombre de lits</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="card mt-2">
                          <div class="card-body">
                            <h5 style="font-weight:normal">Tarif de base par nuit</h5>
                            <div class="row mt-4">
                              <div class="col-md-4">
                                <label for="">Quel est le tarif par nuit ?</label>
                                <div class="input-group">
                                  <div class="input-group-prepend">
                                    <div class="input-group-text">XOF par nuit</div>
                                  </div>
                                  <input type="text" class="form-control" id="inlineFormInputGroupUsername" placeholder="0">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                    </div>
                    <div class="col-md-4">

                    </div>
                  </div>

                </div>
                <div id="step-3" class="">
                  <div class="row">
                    <div class="col-md-8">
                        <h3 class="border-bottom border-gray pb-2">Équipements et services</h3>
                        <p>
                          À présent, donnez-nous des informations générales sur votre établissement telles que les équipements disponibles, la connexion Internet, le stationnement et les langues parlées.
                        </p>
                        <div class="card mt-4">
                          <div class="card-body">
                                <h5 style="font-weight:normal">Internet</h5>
                                <div class="mt-2">
                                  <div class="row">
                                    <div class="col">
                                      <label for="">Les clients peuvent-ils bénéficier du Wi-Fi ?</label>
                                      <div class="radio-block" id="internet_availability">
                                            <div class="radio">
                                              <label>
                                                <input type="radio" name="internet_availability" value="yes_free" checked="">
                                                <span>Oui, du Wi-Fi gratuit</span>
                                              </label>
                                            </div>
                                            <div class="radio">
                                              <label>
                                                <input type="radio" name="internet_availability" value="yes_paid">
                                                <span>Oui, du Wi-Fi payant</span>
                                              </label>
                                            </div>
                                            <div class="radio">
                                              <label>
                                                <input type="radio" name="internet_availability" value="no">
                                                <span>Non</span>
                                              </label>
                                            </div>
                                        </div>
                                        <!-- select class="form-control" name="is_wifi" id="is_wifi">
                                          <option value="1">Oui, du Wi-Fi gratuit</option>
                                          <option value="2">Oui, du Wi-Fi payant</option>
                                          <option value="3">Non</option>
                                        </select-->
                                    </div>
                                  </div>
                                </div>
                          </div>
                        </div>
                        <div class="card mt-2">
                          <div class="card-body">
                            <h5 style="font-weight:normal">Parking</h5>
                            <div class="row mt-2">
                              <div class="col">
                                <label for="parking_available">Est-il possible de stationner à ou près de votre établissement ?</label>
                                <select id="parking_available" name="parking_available" class="form-control " style="">
                                    <option value="no">Non</option>
                                    <option value="paid">Oui avec supplément</option>
                                    <option value="free">Oui, gratuitement</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="card mt-2">
                          <div class="card-body">
                            <h5 style="font-weight:normal">Petit-déjeuner</h5>
                            <div class="row mt-2">
                              <div class="col">
                                <label for="">Le petit-déjeuner est-il proposé ?</label>
                                <select id="breakfast_included" name="breakfast_included" class="form-control " style="">
                                    <option value="no">Non</option>
                                    <option value="included">Oui, il est compris dans le tarif</option>
                                    <option value="optional">Oui, il est proposé en option</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="card mt-2">
                          <div class="card-body">
                            <h5 style="font-weight:normal">Langues parlées</h5>
                            <div class="row mt-2">
                              <div class="col">
                                <label for="">Quelles langues parlez-vous (vous ou votre personnel) ?</label>
                                <select id="lang_spoken_by_staff" class="form-control lang_spoken" name="staff_language">
                                    <option value="">Veuillez sélectionner</option>
                                    <option value="ne"></option>
                                    <option value="dv"></option>
                                    <option value="af">Afrikaans</option>
                                    <option value="sq">Albanais</option>
                                    <option value="de">Allemand</option>
                                    <option value="en">Anglais</option>
                                    <option value="ar">Arabe</option>
                                    <option value="hy">Arménien</option>
                                    <option value="az">Azéri</option>
                                    <option value="eu">Basque</option>
                                    <option value="bn">Bengali</option>
                                    <option value="my">Birman</option>
                                    <option value="be">Biélorusse</option>
                                    <option value="bs">Bosnien</option>
                                    <option value="bg">Bulgare</option>
                                    <option value="yu">Cantonais</option>
                                    <option value="ca">Catalan</option>
                                    <option value="zh">Chinois</option>
                                    <option value="ko">Coréen</option>
                                    <option value="hr">Croate</option>
                                    <option value="da">Danois</option>
                                    <option value="es">Espagnol</option>
                                    <option value="et">Estonien</option>
                                    <option value="fa">Farsi</option>
                                    <option value="fi">Finnois</option>
                                    <option value="fr" selected="selected">Français</option>
                                    <option value="fo">Féroïen</option>
                                    <option value="gl">Galicien</option>
                                    <option value="cy">Gallois</option>
                                    <option value="el">Grec</option>
                                    <option value="kl">Groenlandais</option>
                                    <option value="gu">Gujarati</option>
                                    <option value="ka">Géorgien</option>
                                    <option value="ha">Haoussa</option>
                                    <option value="hi">Hindi</option>
                                    <option value="hu">Hongrois</option>
                                    <option value="he">Hébreu</option>
                                    <option value="id">Indonésien</option>
                                    <option value="ga">Irlandais</option>
                                    <option value="is">Islandais</option>
                                    <option value="it">Italien</option>
                                    <option value="ja">Japonais</option>
                                    <option value="kn">Kannada</option>
                                    <option value="km">Khmer</option>
                                    <option value="lo">Lao</option>
                                    <option value="lv">Letton</option>
                                    <option value="lt">Lituanien</option>
                                    <option value="mk">Macédonien</option>
                                    <option value="ms">Malais</option>
                                    <option value="ml">Malayalam</option>
                                    <option value="mt">Maltais</option>
                                    <option value="mr">Marathi</option>
                                    <option value="mo">Moldave</option>
                                    <option value="mn">Mongole</option>
                                    <option value="no">Norvégien</option>
                                    <option value="nl">Néerlandais</option>
                                    <option value="or">Odia</option>
                                    <option value="ur">Ourdou</option>
                                    <option value="pl">Polonais</option>
                                    <option value="pt">Portugais</option>
                                    <option value="pa">Punjabi</option>
                                    <option value="ro">Roumain</option>
                                    <option value="ru">Russe</option>
                                    <option value="sr">Serbe</option>
                                    <option value="sk">Slovaque</option>
                                    <option value="sl">Slovène</option>
                                    <option value="sv">Suédois</option>
                                    <option value="sw">Swahili</option>
                                    <option value="tl">Tagalog</option>
                                    <option value="ta">Tamoul</option>
                                    <option value="cs">Tchèque</option>
                                    <option value="th">Thaï</option>
                                    <option value="tr">Turc</option>
                                    <option value="te">Télougou</option>
                                    <option value="uk">Ukrainien</option>
                                    <option value="vi">Vietnamien</option>
                                    <option value="xh">Xhosa</option>
                                    <option value="yo">Yoruba</option>
                                    <option value="zu">Zoulou</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="card mt-2" id="facilities_section">
                          <div class="card-body" id="top_facilities">
                            <h5 style="font-weight:normal">Équipements populaires auprès des clients</h5>
                            <div class="describe-block">
                                <p class="describe-text">
                                    Ces équipements sont les plus prisés par les clients lorsqu'ils effectuent une recherche d'établissements.
                                </p>
                            </div>
                            <div class="block-facilities_change_opacity">
                            <div class="row mt-4">
                              <ul class="col-sm-6">

                              <li id="container_7" class="facility_items row" data-id="7" data-kind="boolean">
                                  <div class="facility_items_body clearfix">

                                  <div class="checkbox col-sm-12">
                                      <label class="has_facility" for="facility_7">
                                          <input data-message="" type="checkbox" class="cb facility_checkbox" id="facility_7">
                                              Bar
                                      </label>
                                  </div>
                              </div>
                              </li>
                        <li id="container_10" class="facility_items row" data-id="10" data-kind="free_or_paid">
                            <div class="facility_items_body clearfix">

                            <div class="checkbox col-sm-5">
                                <label class="has_facility" for="facility_10">
                                    <input data-message="" type="checkbox" class="cb facility_checkbox" id="facility_10">
                                        Sauna
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <select class="form-control facility_select" style="display:none;">
                                    <option value="3">Veuillez sélectionner</option>
                                    <option value="4">Gratuit</option>
                                    <option value="5">Payant</option>
                                </select>
                            </div>
                        </div>
                        </li>
                          <li id="container_14" class="facility_items row" data-id="14" data-kind="boolean">
                              <div class="facility_items_body clearfix">
                              <div class="checkbox col-sm-12">
                                  <label class="has_facility" for="facility_14">
                                      <input data-message="" type="checkbox" class="cb facility_checkbox" id="facility_14">
                                          Jardin
                                  </label>
                              </div>
                          </div>
                          </li>
                          <li id="container_15" class="facility_items row" data-id="15" data-kind="boolean">
                              <div class="facility_items_body clearfix">
                              <div class="checkbox col-sm-12">
                                  <label class="has_facility" for="facility_15">
                                      <input data-message="" type="checkbox" class="cb facility_checkbox" id="facility_15">
                                          Terrasse
                                  </label>
                              </div>
                          </div>
                          </li>
                          <li id="container_16" class="facility_items row" data-id="16" data-kind="boolean">
                              <div class="facility_items_body clearfix">
                              <div class="checkbox col-sm-12">
                                  <label class="has_facility" for="facility_16">
                                      <input data-message="" type="checkbox" class="cb facility_checkbox" id="facility_16">
                                          Chambres non-fumeurs
                                  </label>
                              </div>
                          </div>
                          </li>
                            <li id="container_28" class="facility_items row" data-id="28" data-kind="boolean">
                                <div class="facility_items_body clearfix">
                                <div class="checkbox col-sm-12">
                                    <label class="has_facility" for="facility_28">
                                        <input data-message="Désolés, vous ne pouvez pas sélectionner cette option après avoir choisi «&nbsp;réservé aux adultes&nbsp;»." type="checkbox" class="cb facility_checkbox" id="facility_28">
                                            Chambres familiales
                                    </label>
                                </div>
                            </div>
                            </li>
                            </ul>
                            <ul class="col-sm-6">
                              <li id="container_63" class="facility_items row" data-id="63" data-kind="free_or_paid">
                                  <div class="facility_items_body clearfix">
                                  <div class="checkbox col-sm-5">
                                      <label class="has_facility" for="facility_63">
                                          <input data-message="" type="checkbox" class="cb facility_checkbox" id="facility_63">
                                              Bain à remous/jacuzzi
                                      </label>
                                  </div>
                                  <div class="col-sm-5">
                                      <select class="form-control facility_select" style="display:none;">
                                          <option value="3">Veuillez sélectionner</option>
                                          <option value="4">Gratuit</option>
                                          <option value="5">Payant</option>
                                      </select>
                                  </div>
                              </div>
                              </li>
                              <li id="container_109" class="facility_items row" data-id="109" data-kind="free_or_paid">
                                  <div class="facility_items_body clearfix">
                                  <div class="checkbox col-sm-5">
                                      <label class="has_facility" for="facility_109">
                                          <input data-message="" type="checkbox" class="cb facility_checkbox" id="facility_109">
                                              Climatisation
                                      </label>
                                  </div>
                                  <div class="col-sm-5">
                                      <select class="form-control facility_select" style="display:none;">
                                          <option value="3">Veuillez sélectionner</option>
                                          <option value="4">Gratuit</option>
                                          <option value="5">Payant</option>
                                      </select>
                                  </div>
                              </div>
                              </li>
                              <li id="container_182" class="facility_items row" data-id="182" data-kind="boolean">
                                  <div class="facility_items_body clearfix">
                                  <div class="checkbox col-sm-12">
                                      <label class="has_facility" for="facility_182">
                                          <input data-message="" type="checkbox" class="cb facility_checkbox" id="facility_182">
                                              <span class="facility_tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Les véhicules électriques peuvent y être rechargés">Borne de recharge pour les véhicules électriques</span>
                                      </label>
                                  </div>
                              </div>
                              </li>
                            <li id="container_301" class="facility_items row" data-id="301" data-kind="boolean">
                                <div class="facility_items_body clearfix">
                                <div class="checkbox col-sm-12">
                                    <label class="has_facility" for="facility_301">
                                        <input data-message="" type="checkbox" class="cb facility_checkbox" id="facility_301">
                                        Piscine
                                    </label>
                                </div>
                            </div>
                            </li>
                            <li id="container_302" class="facility_items row" data-id="302" data-kind="boolean">
                                <div class="facility_items_body clearfix">
                                <div class="checkbox col-sm-12">
                                    <label class="has_facility" for="facility_302">
                                        <input data-message="" type="checkbox" class="cb facility_checkbox" id="facility_302">
                                            Plage
                                    </label>
                                </div>
                            </div>
                            </li>
                            </ul>
                            </div>
                          </div>
                          </div>
                        </div>

                    </div>
                    <div class="col-md-4">

                    </div>
                  </div>
                </div>
                <div id="step-4" class="">
                  <div class="row mt-2">
                    <div class="col-md-8 col-offset-2">
                      <h3 class="border-bottom border-gray pb-2">Équipements et services</h3>
                      <p>
                        Vous avez presque terminé ! Nous avons juste besoin de quelques détails supplémentaires concernant vos options pour les lits d'appoint ainsi que les équipements ou services spécifiques dont vous disposez.
                      </p>
                        <div class="card mt-2">
                          <div class="card-body">
                            <h5 style="font-weight:normal">Lits d’appoint</h5>
                            <div class="describe-block">
                                <p class="describe-text">
                                  Les types de lits suivants peuvent être ajoutés sur demande.
                                </p>
                            </div>
                            <div class="row mt-2">
                              <div class="col">
                                <label for="extra_beds_available">Proposez-vous des lits d’appoint ?</label>
                                <div class="radio-block" id="extra-beds">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="extra_beds_available" value="yes">
                                        <span>Oui</span>
                                    </label>
                                </div>
                                <div class="radio">
                                  <label>
                                    <input type="radio" name="extra_beds_available" value="no" checked="">
                                    <span>Non</span>
                                  </label>
                                </div>

                            </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="card mt-2">
                          <div class="card-body">
                            <h5 style="font-weight:normal">Équipements</h5>
                            <p>Dites-nous tout sur vos équipements</p>
                            <div class="amenities_accordion_wrapper_aligned amenities_accordion_wrapper">
                              <div class="top-amenities-wrapper">
                                    <h5>Les plus demandés par les clients</h5>
                                    <div class="top-amenities-list" id="top-amenities-list-wrapper">
                                        <ul class="accordion_options_wrapper accordion_options_wrapper-top-amenities">
                                                    <li class="accordion_row_wrapper">
                                                      <div class="ac_li_wrapper ac_padding">
                                                              <label class="ac_label" for="amenities_room_all_4_11">
                                                                  <input type="checkbox" name="top_room_all_11" id="amenities_room_all_4_11" class="select_all_rooms" value="1" data-type-id="4" data-amenity-id="11">
                                                                  Climatisation
                                                              </label>
                                                      </div>
                                                  </li>
                                                    <li class="accordion_row_wrapper">
                                                      <div class="ac_li_wrapper ac_padding">
                                                              <label class="ac_label" for="amenities_room_all_4_93">
                                                                  <input type="checkbox" name="top_room_all_93" id="amenities_room_all_4_93" class="select_all_rooms" value="1" data-type-id="4" data-amenity-id="93">
                                                                  Piscine privée
                                                              </label>
                                                      </div>
                                                  </li>
                                                    <li class="accordion_row_wrapper">
                                                      <div class="ac_li_wrapper ac_padding">
                                                              <label class="ac_label" for="amenities_room_all_4_34">
                                                                  <input type="checkbox" name="top_room_all_34" id="amenities_room_all_4_34" class="select_all_rooms" value="1" data-type-id="4" data-amenity-id="34">
                                                                  Lave-linge
                                                              </label>
                                                      </div>
                                                  </li>
                                                    <li class="accordion_row_wrapper">
                                                          <div class="ac_li_wrapper ac_padding">
                                                                  <label class="ac_label" for="amenities_room_all_5_38">
                                                                      <input type="checkbox" name="top_room_all_38" id="amenities_room_all_5_38" class="select_all_rooms" value="1" data-type-id="5" data-amenity-id="38">
                                                                      Salle de bains privative
                                                                  </label>
                                                          </div>
                                                      </li>
                                                    <li class="accordion_row_wrapper">
                                                        <div class="ac_li_wrapper ac_padding">
                                                                <label class="ac_label" for="amenities_room_all_6_75">
                                                                    <input type="checkbox" name="top_room_all_75" id="amenities_room_all_6_75" class="select_all_rooms" value="1" data-type-id="6" data-amenity-id="75">
                                                                    Télévision à écran plat
                                                                </label>
                                                        </div>
                                                    </li>
                                                    <li class="accordion_row_wrapper">
                                                        <div class="ac_li_wrapper ac_padding">
                                                                <label class="ac_label" for="amenities_room_all_7_45">
                                                                    <input type="checkbox" name="top_room_all_45" id="amenities_room_all_7_45" class="select_all_rooms" value="1" data-type-id="7" data-amenity-id="45">
                                                                    Cuisine
                                                                </label>
                                                        </div>
                                                    </li>
                                                    <li class="accordion_row_wrapper">
                                                          <div class="ac_li_wrapper ac_padding">
                                                                  <label class="ac_label" for="amenities_room_all_7_16">
                                                                      <input type="checkbox" name="top_room_all_16" id="amenities_room_all_7_16" class="select_all_rooms" value="1" data-type-id="7" data-amenity-id="16">
                                                                      Kitchenette
                                                                  </label>
                                                          </div>
                                                      </li>
                                                    <li class="accordion_row_wrapper">
                                                        <div class="ac_li_wrapper ac_padding">
                                                                <label class="ac_label" for="amenities_room_all_9_17">
                                                                    <input type="checkbox" name="top_room_all_17" id="amenities_room_all_9_17" class="select_all_rooms" value="1" data-type-id="9" data-amenity-id="17">
                                                                    Balcon
                                                                </label>
                                                        </div>
                                                    </li>
                                                    <li class="accordion_row_wrapper">
                                                        <div class="ac_li_wrapper ac_padding">
                                                                <label class="ac_label" for="amenities_room_all_9_81">
                                                                    <input type="checkbox" name="top_room_all_81" id="amenities_room_all_9_81" class="select_all_rooms" value="1" data-type-id="9" data-amenity-id="81">
                                                                    Vue
                                                                </label>
                                                        </div>
                                                    </li>
                                                    <li class="accordion_row_wrapper">
                                                        <div class="ac_li_wrapper ac_padding">
                                                                <label class="ac_label" for="amenities_room_all_9_123">
                                                                    <input type="checkbox" name="top_room_all_123" id="amenities_room_all_9_123" class="select_all_rooms" value="1" data-type-id="9" data-amenity-id="123">
                                                                    Terrasse
                                                                </label>
                                                        </div>
                                                    </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
                <div id="step-5" class="">
                  <div class="row mt-2">
                    <div class="col-md-8">
                      <h3 class="border-bottom border-gray pb-2">Photos de l'établissement</h3>
                      <p>
                        Les clients se font une meilleure idée des établissements affichant d'excellentes photos. Téléchargez des photos haute résolution pour mettre en valeur votre établissement. Ces photos apparaîtront sur la page de votre établissement sur le site Ahoko.net.
                       </p>
                      <div class="card mt-2">
                        <div class="card-body">
                          <div id="fine-uploader-gallery-2"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="step-6" class="">
                  <div class="row mt-2">
                    <div class="col-md-8">
                      <h3 class="border-bottom border-gray pb-2">Conditions</h3>
                      <p>
                        Indiquez des conditions basiques. Acceptez-vous les enfants ou les animaux domestiques ? Proposez-vous des conditions d'annulation flexibles ?
                      </p>
                      <div class="card mt-2">
                        <div class="card-body">
                          <h5 style="font-weight:normal">Annulations</h5>
                          <div class="col-sm-12" data-track-ga="click_cancellation_policies">
                            <label for="new_cancellation_policy">Combien de temps à l'avance vos clients peuvent-ils annuler leur réservation gratuitement ?</label>
                              <select id="new-cancellation-policy-mobile" name="new_cancellation_policy" class="form-control " style="">
                              <option value="2">1 jour avant l'arrivée</option>
                              <option value="3">7 jours avant l'arrivée</option>
                              <option value="4" selected="">14 jours avant l'arrivée</option>
                              <option value="5">30 jours avant l'arrivée</option>
                          </select>
                        </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="step-7" class="">

                </div>
                <div id="step-8" class="">

                </div>
            </div>
        </div>

        @include('layouts.gallery_template_2')

    </div>

@endsection

@push('js')
<script  src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="{{asset('js/jquery.fine-uploader.min.js')}}"></script>

<script type="text/javascript" src="{{asset('js/jquery.smartWizard.js')}}"></script>
<script type="text/javascript">
        $(document).ready(function(){
            // Step show event
            $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
               //alert("You are on step "+stepNumber+" now");
               if(stepPosition === 'first'){
                   $("#prev-btn").addClass('disabled');
               }else if(stepPosition === 'final'){
                   $("#next-btn").addClass('disabled');
               }else{
                   $("#prev-btn").removeClass('disabled');
                   $("#next-btn").removeClass('disabled');
               }
            });
            // Toolbar extra buttons
            var btnFinish = $('<button></button>').text('Finish')
                                             .addClass('btn btn-info')
                                             .on('click', function(){ alert('Finish Clicked'); });
            var btnCancel = $('<button></button>').text('Cancel')
                                             .addClass('btn btn-danger')
                                             .on('click', function(){ $('#smartwizard').smartWizard("reset"); });
            // Smart Wizard
            $('#smartwizard').smartWizard({
                    selected: 0,
                    theme: 'default',
                    transitionEffect:'fade',
                    showStepURLhash: true,
                    toolbarSettings: {toolbarButtonPosition: 'end',
                                      toolbarExtraButtons: [btnFinish, btnCancel]
                                    }
            });
            // External Button Events
            $("#reset-btn").on("click", function() {
                // Reset wizard
                $('#smartwizard').smartWizard("reset");
                return true;
            });
            $("#prev-btn").on("click", function() {
                // Navigate previous
                $('#smartwizard').smartWizard("prev");
                return true;
            });
            $("#next-btn").on("click", function() {
                // Navigate next
                $('#smartwizard').smartWizard("next");
                return true;
            });
            $("#theme_selector").on("change", function() {
                // Change theme
                $('#smartwizard').smartWizard("theme", $(this).val());
                return true;
            });
            // Set selected theme on page refresh
            $("#theme_selector").change();

            $('#fine-uploader-gallery-2').fineUploader({
                template: 'qq-template-gallery-2',
                debug: true,
                request: {
                    endpoint: '/data/uploads/images',
                    params: {
                      productid:$('#product_id').val()
                    },
                },
                thumbnails: {
                    placeholders: {
                        waitingPath: "{{asset('images/placeholders/waiting-generic.png')}}",
                        notAvailablePath: "{{asset('images/placeholders/not_available-generic.png')}}"
                    }
                },
                validation: {
                    allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
                }
            });
        });
    </script>

@endpush
