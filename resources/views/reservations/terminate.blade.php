@extends('layouts.master')

@push('css')
<style>

.img {
  float: left;
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 70%;
  padding: 0 20px 20px 0;
}

input[type="checkbox"],
input[type="radio"],
input.radio,
input.checkbox {
    vertical-align:text-top;
    width:13px;
    height:13px;
    padding:0;
    margin:0;
    position:relative;
    overflow:hidden;
    top:2px;
    display: inline-block;
}

.content {
    clear: both;
}


.bp_sidebar_content_block__section {
    padding: 10px 10px 0;
    padding-top: 10px;
    padding-right: 10px;
    padding-bottom: 0px;
    padding-left: 10px;
    margin: 10px -10px 0;
    margin-top: 10px;
    margin-right: -10px;
    margin-bottom: 0px;
    margin-left: -10px;
    border-top: 1px solid #cce1ff;
    border-top-width: 1px;
    border-top-style: solid;
    border-top-color: rgb(204, 225, 255);
}

<!-- Progress bar  -->

.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 20px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
<!---Fin Progress-->


.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
}


.agent-widget {
    background-color: #fff;
    color: #707070;
    border-radius: 4px;
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
    border-bottom-left-radius: 4px;
    padding: 32px;
    padding-top: 32px;
    padding-right: 32px;
    padding-bottom: 32px;
    padding-left: 32px;
    box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.1);
    transition: 0.25s;
    transition-property: all;
    transition-duration: 0.25s;
    transition-timing-function: ease;
    transition-delay: 0s;
    margin-bottom: 30px;
}
.list-group-item-ot {
    position: relative;
    display: block;
}
.valid-feedback {
    width: 100%;
    margin-top: .25rem;
    font-size: 80%;
    color: #28a745;
}
.payments {
  min-width: 320px;
}

/*
* SVG Icons
**********************************************************/
svg {
  display: block;
  position: absolute;
  top: 2px;
  right: 15px;
  width: 45px;
  height: 45px;
}

svg.svg-debit {
  top: 5px;
  right: 17px;
  width: 40px;
  height: 40px;
}

svg.svg-visa {
  right: 125px;
}

svg.svg-master {
  right: 70px;
}

svg.svg-sofort {
  top: -8px;
  right: 8px;
  width: 65px;
  height: 65px;
}

.svg-cash-hand {
  fill: #FFCA65;
}

.svg-cash-thumb {
  stroke: #FBAD3E;
  fill: none;
}

.svg-cash-money {
  fill: #34b154;
}

.svg-cash-money-inner {
  fill: #fff;
  opacity: .4;
}

.svg-cash-shirt-inner {
  fill: #def;
}

.svg-cash-shirt-outer {
  fill: #07b;
}

.svg-debit-card {
  fill: #e5e5e5;
}

.svg-debit-data {
  fill: #bbb;
}

.svg-debit-sign {
  fill: #fff;
}

.svg-debit-read {
  fill: #555;
}

.svg-visa-border {
  fill: #005098;
}

.svg-visa-letter {
  fill: #005098;
}

.svg-visa-corner {
  fill: #F6A500;
}

.svg-master-border {
  fill: #E30613;
}

.svg-master-circle1 {
  fill: #E40520;
}

.svg-master-circle2 {
  fill: #FAB31E;
}

.svg-master-letter {
  fill: #fff;
}

.svg-amex-border {
  fill: #0098D0;
}

.svg-amex-letter {
  fill: #0098D0;
}

.svg-sofort-line1 {
  fill: #ee7f00;
}

.svg-sofort-line2 {
  fill: #383a41;
}

.svg-sofort-fill {
  fill: #fff;
}

.svg-click-border {
  fill: #FF8000;
}

.svg-click-logo {
  fill: #FF8000;
}

.svg-click-letter {
  fill: #FF8000;
}

.svg-click-letter-center {
  fill: #808080;
}

.svg-paypal-border {
  fill: #1B557D;
}

.svg-paypal-letter1to3 {
  fill: #1B557D;
}

.svg-paypal-letter4to6 {
  fill: #107DB0;
}

/*
* Buttons
**********************************************************/
#payement .button {
  position: relative;
  height: 50px;
  padding: 0 0 0 50px;
  font-size: 14px;
  line-height: 48px;
  border-bottom: 1px solid #bbb;
  background: #fafafa;
  cursor: pointer;
}

#payement .button:hover {
  background: #f5f5f5;
}

#payement .button:after {
  content: '';
  position: absolute;
  top: 15px;
  left: 18px;
  display: block;
  width: 20px;
  height: 20px;
  border-radius: 50%;
  box-shadow: inset 0 0 0 1px #bbb, inset 0 0 0 7px #fff;
  background: #fff;
}

#payement .button:hover:after {
  background: #bbb;
}

#payement .button.active:after {
  background: #555;
}

/*
* Breakpoint
**********************************************************/
@media  all and (min-width: 500px) {
  svg {
    right: 25px;
    width: 55px;
    height: 55px;
  }

  svg.svg-debit {
    top: 4px;
    right: 27px;
    width: 50px;
    height: 50px;
  }

  svg.svg-visa {
    right: 175px;
  }

  svg.svg-master {
    right: 100px;
  }

  svg.svg-sofort {
    top: -14px;
    right: 15px;
    width: 85px;
    height: 85px;
  }

  #payement .button {
    height: 60px;
    padding: 0 0 0 60px;
    font-size: 18px;
    line-height: 58px;
  }

  #payement .button:after {
    top: 20px;
    left: 23px;
  }
}
/*
* Breakpoint
**********************************************************/
@media  all and (min-width: 700px) {
  svg {
    top: auto !important;
    right: 0 !important;
    left: 0;
    margin: auto;
    bottom: 10px;
    width: 60px;
    height: 60px;
  }

  svg.svg-cash {
    bottom: 5px;
    width: 70px;
    height: 70px;
  }

  svg.svg-debit {
    width: 60px;
    height: 60px;
  }

  svg.svg-visa {
    bottom: 50px;
    width: 40px;
    height: 40px;
  }

  svg.svg-master {
    bottom: 25px;
    width: 40px;
    height: 40px;
  }

  svg.svg-amex {
    bottom: 0;
    width: 40px;
    height: 40px;
  }

  svg.svg-sofort {
    bottom: 0;
    width: 85px;
    height: 85px;
  }

  svg.svg-click {
    bottom: -2px;
    width: 90px;
    height: 90px;
  }

  svg.svg-paypal {
    bottom: -8px;
    width: 100px;
    height: 100px;
  }

  .svg-visa-border,
  .svg-master-border,
  .svg-amex-border,
  .svg-click-border,
  .svg-paypal-border {
    display: none;
  }

  .payments {
    max-width: 700px;
    margin: 0 auto;
    padding: 25px;
    overflow: hidden;
  }

  #payement .button {
    float: left;
    width: 150px;
    height: 150px;
    margin-right: 15px;
    padding: 50px 0 0;
    font-size: 13px;
    line-height: 1;
    text-align: center;
    border: 0;
    border-radius: 3px;
    box-shadow: inset 0 0 0 1px #bbb;
  }

#payement .button:last-child {
    margin-right: 0;
  }

  #payement .button:after {
    top: 15px;
    left: 65px;
  }

#payement  .footer {
    border-top: 1px solid #bbb;
  }
}
.bcom-wrapper {
    border-left: 12px solid #749E3A;
    padding-top: 20px;
    padding-right: 25px;
    padding-bottom: 15px;
    padding-left: 25px;
    background-color: #FFF;
    -moz-box-shadow: 13px 13px 0px 0px #FFFFFF;
   -webkit-box-shadow: 13px 13px 0px 0px #FFFFFF;
    box-shadow: 13px 13px 0px 0px  #FFFFFF;
}
section:not(.block) {
    margin-top: 5px;
    margin-bottom: 30px;
}
.bcom-hgroup {
    display: block;
    vertical-align: middle;
}
.bcom-list {
    margin-top: 15px;
}
</style>
@endpush

@section('content')

<div class="container" style="margin-top: 15px">
  <div class="row">
    <div class="col-md-12">

      <!-- step wizard-->
      <div class="stepwizard">
          <div class="stepwizard-row setup-panel">
              <div class="stepwizard-step">
                  <a href="#step-1" type="button" class="btn btn-default btn-circle" disabled>1</a>
                  <p>Choisissez vos chambres</p>
              </div>
              <div class="stepwizard-step">
                  <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                  <p>Saisissez vos coordonnées</p>
              </div>
              <div class="stepwizard-step">
                  <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                  <p>Dernières informations</p>
              </div>
              <div class="stepwizard-step">
                  <a href="#step-4" type="button" class="btn btn-success btn-circle" disabled="disabled">3</a>
                  <p>Confirmation</p>
              </div>
          </div>
      </div>
      <!--end step wizard-->

      <div class="col-md-9">
        <section>
          <div class="bcom-wrapper">
            <div class="bcom-hgroup">
                <h2 class="bcom-title">Location confirmée</h2>
                <h3 class="bcom-sub-title">Merci d’avoir réservé avec Ahoko Résidence, <strong>M. {{$order->customer_first_name}} {{$order->customer_last_name}}</strong> !</h3>
            </div>
            <ul class="bcom-list">
              <li class="bcom-list-item">Votre fature a été envoyé à <strong>{{$order->customer_email}}</strong>.</li>
              <li class="bcom-list-item">Votre référence de commande est le :&nbsp;<strong>{{$order->ref_code}}</strong>.</li>
            </ul>
          </div>
          <div style="margin-top:30px; text-align:center">
            <a href="/" class="button" style="width:50%">
              Continuer
            </a>
          </div>
        </section>

    </div>


    </div>
  </div>
</div>
@endsection
