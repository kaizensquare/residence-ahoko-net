@extends('layouts.master')

@section('content')
<!-- Content
================================================== -->
<div class="container">
	<div class="row margin-bottom-50 margin-top-50">
    <div class="col-md-4">
      <div class="card text-center">
        <div class="card-body">
          <h3 class="card-title">{{number_format($product->default_room()->price)}} FCFA / Jour</h3>
        </div>
      </div>
      <a class="btn button fullwidth margin-bottom-30" href="{{route('faireReservations', [$product->slug, $date_start, $date_end])}}">Reserver</a>

      <!-- Widget -->
      <div class="widget margin-bottom-40">
        <h3 class="margin-top-0 margin-bottom-25">Rechercher</h3>

				<form class="" action="/search" method="get">

					        <!-- Row -->
					        <div class="row with-forms">
					          <!-- Status -->
					          <div class="col-md-12">
					            <label for="">Destination</label>
											<input id="autocompletecity" type="text" name="city" class="ico-01" placeholder="Ou allez vous ?" autocomplete="OFF" data-country="ci" value="@if(isset($city)){{$city}}@endif" required/>
											<input type="hidden" name="place_id" id="geo_place_id" value="@if(isset($place_id)){{$place_id}}@endif"/>
											<input type="hidden" name="geo_id" id="geo_id"/>
					          </div>
					        </div>
					        <!-- Row / End -->


					        <!-- Row -->
					        <div class="row with-forms">
					          <!-- Type -->
					          <div class="col-md-12">
					            <label for="">Du</label>
					            <input type="text" class="ico-01" name="start_date" data-toggle="datepicker" placeholder="" value="{{date('d/m/Y')}}" required>
					          </div>
					        </div>
					        <!-- Row / End -->

					        <!-- Row -->
					        <div class="row with-forms">
					          <!-- Type -->
					          <div class="col-md-12">
					            <label for="">Au</label>
					            <input type="text" class="ico-01" name="end_date" data-toggle="datepicker" placeholder="" value="{{date('d/m/Y', strtotime(date('m/d/Y').' +1 days'))}}" required>
					          </div>
					        </div>
					        <!-- Row / End -->


					        <!-- Row --
					        <div class="row with-forms">
					          <!-- States --
					          <div class="col-md-12">
					             <select class="chosen-select" >
					              <option>1 adulte</option>
					              <option selected>2 adultes</option>
					              <option>3 adultes</option>
					              <option>4 adultes</option>
					              <option>5 adultes</option>
					              <option>6 adultes</option>
					              <option>7 adultes</option>
					              <option>8 adultes</option>
					              <option>9 adultes</option>
					              <option>10 adultes</option>
					            </select>
					          </div>
					        </div>
					        <!-- Row / End -->
					        <!-- Row --
					        <div class="row with-forms">
					          <!-- Min Area --
					          <div class="col-md-6">
					            <select class="chosen-select-no-single" >
					              <option selected>Aucun enfant</option>
					              <option>1 enfant</option>
					              <option>2 enfants</option>
					              <option>3 enfants</option>
					              <option>4 enfants</option>
					              <option>5 enfants</option>
					            </select>
					          </div>

					          <!-- Max Area --
					          <div class="col-md-6">
					            <select class="chosen-select-no-single" >
					              <option>1 chambre</option>
					              <option>2 chambres</option>
					              <option>3 chambres</option>
					              <option>4 chambres</option>
					              <option>5 chambres</option>
					            </select>
					          </div>

					        </div>
					        <!-- Row / End -->

					        <br>
								 <h3>Filtrer Par</h3>
									<!-- Price Range -->
									<div class="range-slider">
										<label>Échelle des prix</label>
										<div id="price-range-detail" data-min="0" data-max="500000" data-unit="CFA "></div>
										<div class="clearfix"></div>
									</div>



					        <!-- More Search Options --
					        <a href="#" class="more-search-options-trigger margin-bottom-10 margin-top-30" data-open-title="Caractéristiques" data-close-title="Caractéristiques"></a>

									<div class="more-search-options relative">

										<!-- Checkboxes --
										<div class="checkboxes one-in-row margin-bottom-10">

											<input id="check-2" type="checkbox" name="check">
											<label for="check-2">Air Conditioning</label>

											<input id="check-3" type="checkbox" name="check">
											<label for="check-3">Swimming Pool</label>

											<input id="check-4" type="checkbox" name="check" >
											<label for="check-4">Central Heating</label>

											<input id="check-5" type="checkbox" name="check">
											<label for="check-5">Laundry Room</label>


											<input id="check-6" type="checkbox" name="check">
											<label for="check-6">Gym</label>

											<input id="check-7" type="checkbox" name="check">
											<label for="check-7">Alarm</label>

											<input id="check-8" type="checkbox" name="check">
											<label for="check-8">Window Covering</label>

										</div>
										<!-- Checkboxes / End --

									</div>
									<!-- More Search Options / End -->

					        <button type="submit" class="button fullwidth margin-top-30">Search</button>

				</form>

      </div>
      <!-- Widget / End -->
    </div>
		<div class="col-md-8">
      <h3 class="margin-top-0 margin-bottom-20">{{$product->name}}</h3>
      <a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="listing-address popup-gmaps">
        <i class="fa fa-map-marker"></i>
        {{$product->address}}
      </a>
			<!-- Slider Container -->
			<div class="property-slider-container">
				<!-- Slider -->
				<div class="property-slider no-arrows">
					@if($product->product_images)
					@foreach($product->product_images as $value)
					<a href="{{$value->path}}" data-background-image="{{$value->path}}" class="item mfp-gallery"></a>
					@endforeach
					@endif
				</div>
				<!-- Slider / End -->

			</div>
			<!-- Slider Container / End -->

			<!-- Slider Thumbs -->
			<div class="property-slider-nav">
				@if($product->product_images)
				@foreach($product->product_images as $value)
				<div  class="item"><img src="{{$value->path}}" alt=""></div>
				@endforeach
				@endif
			</div>



      <div class="property-description">

        <!-- Description -->
        <h3 class="desc-headline">Description</h3>
        <div class="show-more">
          <?php echo $product->description; ?>

          <a href="#" class="show-more-button">Voir plus <i class="fa fa-angle-down"></i></a>
        </div>

        <!-- Details --
        <h3 class="desc-headline">Détails</h3>
        <ul class="property-features margin-top-0">
          <li>Building Age: <span>2 Years</span></li>
          <li>Parking: <span>Attached Garage</span></li>
          <li>Cooling: <span>Central Cooling</span></li>
          <li>Heating: <span>Forced Air, Gas</span></li>
          <li>Sewer: <span>Public/City</span></li>
          <li>Water: <span>City</span></li>
          <li>Exercise Room: <span>Yes</span></li>
          <li>Storage Room: <span>Yes</span></li>
        </ul-->


        <!-- Features -->
        <h3 class="desc-headline">Caractéristiques</h3>
        <ul class="property-features checkboxes margin-top-0">
					@if($product && $product->options)
						@foreach($product->options as $value)
							@if($value && $value->option)
							<li>{{$value->option->name}}</li>
							@endif
						@endforeach
				@endif
        </ul>

        <!-- Location -->
        <h3 class="desc-headline no-border" id="location">Map</h3>
        <div id="propertyMap-container">
          <div id="propertyMap" data-latitude="{{$product->lat_address}}" data-longitude="{{$product->lng_address}}"></div>
          <!--a href="#" id="streetView">Street View</a-->
        </div>

      </div>


		</div>
	</div>
</div>
@endsection
