@extends('layouts.master')

@push('css')
<link rel="stylesheet" href="{{asset('css/dashboard.css')}}">
@endpush

@section('content')

<div class="container">
	<div class="row" style="margin-top: 35px;">

    <div class="col-md-12">
      <h3>Bonjour {{Auth::user()->name}}, heureux de vous rencontrer</h3>

    </div>
		<!-- Widget -->
		<div class="col-md-4" style="margin-top: 35px;">
			<div class="sidebar left">

				<div class="my-account-nav-container">

					<ul class="my-account-nav">
						<li class="sub-nav-title">Manage Account</li>
						<li><a href="/dashboard"><i class="sl sl-icon-home"></i> Accueil</a></li>
						<li><a href="#"><i class="sl sl-icon-docs"></i> Mes commandes</a></li>
						<li><a href="#"><i class="sl sl-icon-star"></i> Mes Favoris</a></li>
					</ul>

					<ul class="my-account-nav">
						<li class="sub-nav-title">Manage Listings</li>
						<li><a href="#"><i class="sl sl-icon-docs"></i> Mes Hébergements</a></li>
						<li><a href="#"><i class="sl sl-icon-action-redo"></i> Publier un Hébergement</a></li>
					</ul>

					<ul class="my-account-nav">
            <li><a href="#" class="current"><i class="sl sl-icon-user"></i> Paramètres</a></li>
						<li><a href="#"><i class="sl sl-icon-lock"></i>Modifier Mot de passe</a></li>
						<li><a href="#"><i class="sl sl-icon-power"></i> Déconnexion</a></li>
					</ul>

				</div>

			</div>
		</div>

		<div class="col-md-8" style="margin-top: 35px;">
			<div class="row">
				<div class="col-md-12 my-profile">
					<h4 class="margin-top-0 margin-bottom-30">My Account</h4>

					<label>Votre Nom</label>
					<input value="Jennie Wilson" type="text">

					<label>Votre Prénom</label>
					<input value="Agent In New York" type="text">

					<label>Téléphone Mobile</label>
					<input value="" type="text">

					<label>Adresse Email</label>
					<input value="jennie@example.com" type="text">

					<button class="button margin-top-20 margin-bottom-20">Enregistrer</button>
				</div>
			</div>
		</div>

	</div>
</div>

@endsection
