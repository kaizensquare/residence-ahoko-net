<div class="table-responsive">
    <table class="table" id="products-table">
        <thead>
        <tr>
        <th>Name</th>
        <th>Slug</th>
        <th>Quantity</th>
        <th>Status</th>
        <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <td>{!! $product->name !!}</td>
            <td>{!! $product->slug !!}</td>
            <td>{!! $product->quantity !!}</td>
            <td>{!! $product->status !!}</td>
                <td>
                    <div class='btn-group'>
                        <a href="{!! route('rooms.create.find', [$product->id]) !!}" class='btn btn-default btn-xs'>Modifier Chambre</a>
                    </div>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
