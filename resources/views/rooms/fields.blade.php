<!-- Id Hebergement Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_hebergement', 'Hebergement:') !!}
    <input type="hidden" name="id_hebergement" value="{{$product->id}}">
    <input type="text" class="form-control" name="name_hebergement" value="{{$product->name}}" disabled>
</div>

<!-- Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type_id', 'Type de chambre:') !!}
    <select class="form-control select2" name="type_id" required>
      <option value="">Sélectionner le type de chambre</option>
      @if($types)
        @foreach($types as $type)
        <option value="{{$type->id}}">{{$type->name}}</option>
        @endforeach
      @endif
    </select>
</div>

<!-- Type Of Bed Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type_of_bed', 'Type de lit:') !!}
    <select class="form-control select2" name="type_of_bed" required>
      <option value="">Sélectionner le type de lit</option>
      @if($type_of_beds)
        @foreach($type_of_beds as $type)
        <option value="{{$type->id}}">{{$type->name}}</option>
        @endforeach
      @endif
    </select>
</div>
<!-- Quantity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('quantity', 'Nombre de chambre:') !!}
    {!! Form::text('quantity', null, ['class' => 'form-control']) !!}
</div>
<!-- shower Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shower', 'Nombre de Douche:') !!}
    {!! Form::text('shower', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-3">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Cost Field -->
<div class="form-group col-sm-3">
    {!! Form::label('cost', 'Cost:') !!}
    {!! Form::text('cost', null, ['class' => 'form-control']) !!}
</div>



<!-- Status Field -->
<div class="form-group col-sm-3">
    {!! Form::label('status', 'Status:') !!}
    <select class="form-control" name="status" required>
      <option value="1">Actif</option>
      <option value="0">Non actif</option>
    </select>
</div>


<!-- Default Field -->
<div class="form-group col-sm-3">
    {!! Form::label('room_default', 'Chambre par défaut:') !!}
    <select class="form-control" required @if($product->default_room != null) disabled @else id="room_default" name="room_default"  @endif>
      <option value="1">Actif</option>
      <option value="0">Non actif</option>
    </select>
</div>



<div class="form-group col-sm-6">
  {!! Form::label('option', 'Options de chambre:') !!}
  <select class="form-control select2" name="option[]" multiple="multiple" data-placeholder="Selectionner les options" style="width: 100%;">
    <option value="">Sélectionner les options</option>
    @if($options)
      @foreach($options as $option)
      <option value="{{$option->id}}">{{$option->name}}</option>
      @endforeach
    @endif
  </select>
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregister', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('rooms.index') !!}" class="btn btn-default">Annuler</a>
</div>
