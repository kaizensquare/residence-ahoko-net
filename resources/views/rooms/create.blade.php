@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Ajouter une chambre
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'rooms.store']) !!}

                        @include('rooms.fields')

                    {!! Form::close() !!}

                </div>
                <div class="row" style="margin:15px">
                  <div class="table-responsive">
                      <table class="table" id="rooms-table">
                          <thead>
                              <tr>
                            <th>Hebergement</th>
                          <th>Type de chambre</th>
                          <th>Type de lit</th>
                          <th>Price</th>
                          <th>Cost</th>
                          <th>Quantity</th>
                          <th>Status</th>
                                  <th colspan="3">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            @if($rooms)
                          @foreach($rooms as $room)
                              <tr>
                              <td>{!! $room->id_hebergement !!}</td>
                              <td>{!! $room->name !!}</td>
                              <td>{!! $room->type_of_bed !!}</td>
                              <td>{!! $room->price !!}</td>
                              <td>{!! $room->cost !!}</td>
                              <td>{!! $room->quantity !!}</td>
                              <td>{!! $room->status !!}</td>
                                  <td>
                                      {!! Form::open(['route' => ['rooms.destroy', $room->id], 'method' => 'delete']) !!}
                                      <div class='btn-group'>
                                          <a href="{!! route('rooms.show', [$room->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                          <a href="{!! route('rooms.edit', [$room->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                          {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                      </div>
                                      {!! Form::close() !!}
                                  </td>
                              </tr>
                          @endforeach
                          @endif
                          </tbody>
                      </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
@endsection
