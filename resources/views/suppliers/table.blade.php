<div class="table-responsive">
    <table class="table" id="suppliers-table">
        <thead>
            <tr>
                <th>Company Name</th>
        <th>Supplier Name</th>
        <th>Supplier Email</th>
        <th>Supplier Phone</th>
        <th>Supplier Address</th>
        <th>User Id</th>
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suppliers as $supplier)
            <tr>
                <td>{!! $supplier->company_name !!}</td>
            <td>{!! $supplier->supplier_name !!}</td>
            <td>{!! $supplier->supplier_email !!}</td>
            <td>{!! $supplier->supplier_phone !!}</td>
            <td>{!! $supplier->supplier_address !!}</td>
            <td>{!! $supplier->user_id !!}</td>
            <td>{!! $supplier->status !!}</td>
                <td>
                    {!! Form::open(['route' => ['suppliers.destroy', $supplier->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('suppliers.show', [$supplier->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('suppliers.edit', [$supplier->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
