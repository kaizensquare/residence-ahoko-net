<!-- Company Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('company_name', 'Company Name:') !!}
    {!! Form::text('company_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Supplier Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('supplier_name', 'Supplier Name:') !!}
    {!! Form::text('supplier_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Supplier Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('supplier_email', 'Supplier Email:') !!}
    {!! Form::text('supplier_email', null, ['class' => 'form-control']) !!}
</div>

<!-- Supplier Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('supplier_phone', 'Supplier Phone:') !!}
    {!! Form::text('supplier_phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Supplier Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('supplier_address', 'Supplier Address:') !!}
    {!! Form::text('supplier_address', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'Utilisateur:') !!}
    <select class="form-control select2" name="user_id">
      <option value="">Sélectionner un Utilisateur</option>
      @if($users)
        @foreach($users as $user)
         <option value="{{$user->id}}" @if(isset($supplier) && $user->id == $supplier->user_id) selected @endif>{{$user->name}}</option>
        @endforeach
      @endif
    </select>
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <select class="form-control" name="status">
      <option value="1" @if(isset($supplier) && 1 == $supplier->status) selected @endif>Actif</option>
      <option value="0" @if(isset($supplier) && 0 == $supplier->status) selected @endif>Non Actif</option>
    </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('suppliers.index') !!}" class="btn btn-default">Cancel</a>
</div>
