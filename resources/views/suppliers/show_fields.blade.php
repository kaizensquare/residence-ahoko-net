<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $supplier->id !!}</p>
</div>

<!-- Company Name Field -->
<div class="form-group">
    {!! Form::label('company_name', 'Company Name:') !!}
    <p>{!! $supplier->company_name !!}</p>
</div>

<!-- Supplier Name Field -->
<div class="form-group">
    {!! Form::label('supplier_name', 'Supplier Name:') !!}
    <p>{!! $supplier->supplier_name !!}</p>
</div>

<!-- Supplier Email Field -->
<div class="form-group">
    {!! Form::label('supplier_email', 'Supplier Email:') !!}
    <p>{!! $supplier->supplier_email !!}</p>
</div>

<!-- Supplier Phone Field -->
<div class="form-group">
    {!! Form::label('supplier_phone', 'Supplier Phone:') !!}
    <p>{!! $supplier->supplier_phone !!}</p>
</div>

<!-- Supplier Address Field -->
<div class="form-group">
    {!! Form::label('supplier_address', 'Supplier Address:') !!}
    <p>{!! $supplier->supplier_address !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $supplier->user_id !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $supplier->status !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $supplier->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $supplier->updated_at !!}</p>
</div>

