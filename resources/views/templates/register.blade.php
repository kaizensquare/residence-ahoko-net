  <div class="row">
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">

                  <div class="col-md-12">
                    <div class="card-title"><div id="logo">
                      <a href="/"><img src="{{asset('images/logo.png')}}" style="width:90px" alt=""></a>
                    </div>
                  </div>
                  </div>
                  <div class="col-md-12" style="margin-top:20px; margin-bottom:40px">
                    <div class="title" style="font-weight:bold; color:#000; font-size:25px;">{{ __('Créez un compte') }}</div>
                    <div class="desc" style="margin-top:15px; font-size:14px">
                      <span>
                        Créez un compte afin de profiter des services Ahoko Residence en toute simplicité.
                      </span>
                    </div>
                  </div>

                    <form d="register-form" onsubmit="return SignupUser()" method="post" role="form">
                        @csrf

                        <div class="col-md-12">
                          <div class="form-group">
                              <label for="name" class="col-form-label text-md-right">{{ __('Nom et Prénom') }}</label>
                                  <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                  @error('name')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror
                          </div>

                          <div class="form-group">
                              <label for="email" class="col-form-label text-md-right">{{ __('Adresse Email') }}</label>

                                  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                  @error('email')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror
                          </div>

                          <div class="form-group">
                              <label for="password" class="col-form-label text-md-right">{{ __('Mot de passe') }}</label>

                                  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                  @error('password')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror
                          </div>
                          <div class="form-group mb-0">
                                  <button type="submit" class="btn btn-success btn-lg btn-block">
                                      {{ __('Commencer') }}
                                  </button>
                          </div>
                        </div>


                    </form>
                </div>
            </div>

            <div class="card mt-4" style="text-align:center">
              <div class="card-body">
                <h5 class="card-title">Vous avez déjà un compte Ahoko Résidence ?</h5>
                <a href="javascript:void(0);" data-toggle="modal" data-target="#seconnecterModal" class="card-link" style="font-weight:bold; font-size:20px">Se connecter</a>
              </div>
            </div>
        </div>


    </div>
