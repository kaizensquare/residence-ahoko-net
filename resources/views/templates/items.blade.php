@if($rooms)
  @foreach($rooms as $room)
  @if($room && $room->product_one() != null)
  <!-- Listing Item -->
  <div class="listing-item">

    <a href="/detail/{{$room->product_one()->slug}}/{{base64_encode($date_time_from)}}/{{base64_encode($date_time_to)}}" class="listing-img-container" style="height: 280px ! important;">

      <!--div class="listing-badges">
        <span class="featured">Featured</span>
      </div-->

      <div class="listing-img-content">
        <span class="listing-price">{{number_format($room->product_one()->default_room()->price)}} FCFA <i>jour</i></span>
        <!--span class="like-icon with-tip" data-tip-content="Add to Bookmarks"></span-->
      </div>

      <div class="listing-carousel">
      @if($room->product_one()->product_images)
        @foreach($room->product_one()->product_images as $value)
        <div><img src="{{$value->path}}" alt="" style="max-height:350px;"></div>
       @endforeach
      @endif
      </div>


    </a>

    <div class="listing-content">

      <div class="listing-title">
        <h4><a href="/detail/{{$room->product_one()->slug}}/{{base64_encode($date_time_from)}}/{{base64_encode($date_time_to)}}"></a>{{$room->product_one()->name}}</h4>
        <a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="listing-address popup-gmaps">
          <i class="fa fa-map-marker"></i>
          {{$room->product_one()->address}}
        </a>

        <a href="/detail/{{$room->product_one()->slug}}/{{base64_encode($date_time_from)}}/{{base64_encode($date_time_to)}}" class="details button border">Details</a>
      </div>

      <ul class="listing-details">
        <li>Pièces <span>{{$room->product_one()->quantity}}</span></li>
        <li>Chambres <span>{{$room->quantity}}</span></li>
        <li>Salles de bain <span>{{$room->quantity}}</span></li>
      </ul>

      <!--div class="listing-footer">
        <span style="color:red; font-size:12px">Forte demande plus que 5 restants</span>
        <a href="#"> David Strozier</a>

      </div-->

    </div>
    <!-- Listing Item / End -->

  </div>
  <!-- Listing Item / End -->
  @endif
@endforeach
@endif
