    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                  <div class="col-md-12">
                    <div class="card-title"><div id="logo">
            					<a href="/"><img src="{{asset('images/logo.png')}}" style="width:90px" alt=""></a>
            				</div>
                  </div>
                  </div>
                  <div class="col-md-12" style="margin-top:20px; margin-bottom:40px">
                    <div class="title" style="font-weight:bold; color:#000; font-size:25px;">{{ __('Se connecter') }}</div>
                    <div class="desc" style="margin-top:15px; font-size:14px">
                      <span>
                        Connectez-vous à l'aide de votre compte Ahoko Residence et accédez à nos services.
                      </span>
                    </div>
                  </div>

                    <div>
                      <form id="login-form" method="POST" onsubmit="return LoginUser()" role="form">
                          @csrf

                          <div class="col-md-12">
                            <div class="form-group">
                                <label for="email" class="col-form-label text-md-right">{{ __('Adresse E-mail') }}</label>
                                    <input id="email_login" type="email" class="form-control" name="email_login"  required autocomplete="email" autofocus>
                                        <span class="invalid-feedback" role="alert">
                                            <strong class="error-email"></strong>
                                        </span>
                            </div>
                          </div>

                          <div class="col-md-12">
                            <div class="form-group">
                                <label for="password" class="col-form-label text-md-right">{{ __('Mot de passe') }}</label>

                                    <input id="password_login" type="password" class="form-control" name="password_login" required autocomplete="current-password">
                                        <span class="invalid-feedback" role="alert">
                                            <strong class="error-password"</strong>
                                        </span>
                            </div>
                          </div>

                          <!--div class="form-group row">
                              <div class="col-md-6 offset-md-4">
                                  <div class="form-check">
                                      <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                      <label class="form-check-label" for="remember">
                                          {{ __('Remember Me') }}
                                      </label>
                                  </div>
                              </div>
                          </div-->

                          <div class="col-md-12">
                            <div class="form-group">
                                    <button type="submit" name="login-submit" id="login-submit" class="btn btn-success btn-lg btn-block">
                                        {{ __('Se connecter') }}
                                    </button>

                                    @if (Route::has('password.request'))
                                        <a target="_blank" class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Mot de passe oublié?') }}
                                        </a>
                                    @endif
                            </div>
                          </div>
                      </form>
                    </div>
                </div>
            </div>


            <div class="card mt-4" style="text-align:center">
              <div class="card-body">
                <h5 class="card-title">Vous n'avez pas encore de compte ?</h5>
                <a href="javascript:void(0);" data-toggle="modal" data-target="#registerModal" class="card-link" style="font-weight:bold; font-size:20px">Créer un compte</a>
              </div>
            </div>
        </div>
    </div>
