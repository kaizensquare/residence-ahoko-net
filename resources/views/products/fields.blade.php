
<div class="col-md-8">
  <div class="box box-solid">
    <!-- /.box-header -->
    <div class="box-body">
      <div class="box-group" id="accordion">
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <div class="panel box-default">
          <div class="box-header with-border">
            <h4 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                General
              </a>
            </h4>
          </div>
          <div id="collapseOne" class="panel-collapse collapse in">
            <div class="box-body">
              <input type="hidden" id="product_id" name="product_id" value="{{$product->id}}">
              <div class="form-group col-sm-10">
                  {!! Form::label('categorie', "Type d'hebergement:") !!}
                  <select class="form-control" name="category">
                    <option value="">Sélectionner le type d'hebergement</option>
                    <option value="1" @if(isset($product) && $product->category == 1) selected @endif>Hôtel  (chambre)</option>
                    <option value="2" @if(isset($product) && $product->category == 2) selected @endif>Résidence (Entier)</option>
                  </select>
              </div>

              <!-- Name Field -->
              <div class="form-group col-sm-10">
                  {!! Form::label('name', 'Nom de votre Hébergement (Résidence - Hôtel):') !!}
                  {!! Form::text('name', null, ['class' => 'form-control']) !!}
              </div>

              <div class="form-group col-sm-10">
                  {!! Form::label('status', 'Status:') !!}
                  <select class="form-control" name="status">
                    <option value="1" @if(isset($product) && $product->status == 1) selected @endif>Enabled</option>
                    <option value="0" @if(isset($product) && $product->status == 0) selected @endif>Disabled</option>
                  </select>
              </div>

              <!-- Quantity Field -->
              <div class="form-group col-sm-10">
                  {!! Form::label('quantity', 'Quantité de chambre:') !!}
                  {!! Form::number('quantity', null, ['class' => 'form-control']) !!}
              </div>

              <div class="form-group col-sm-10">
                {!! Form::label('option', 'Options de chambre:') !!}
                <select class="form-control select2" name="option[]" multiple="multiple" data-placeholder="Selectionner les options" style="width: 100%;">
                  <option value="">Sélectionner les options</option>
                  @if($options)
                    @foreach($options as $option)
                    <option value="{{$option->id}}">{{$option->name}}</option>
                    @endforeach
                  @endif
                </select>
              </div>

              <div class="form-group col-sm-10">
                {!! Form::label('option', 'Propriétaire:') !!}
                <select class="form-control select2" name="supplier_id" multiple="multiple" data-placeholder="Selectionner un propriétaire" style="width: 100%;">
                  <option value="">Sélectionner un propriétaire</option>
                  @if($suppliers)
                    @foreach($suppliers as $supplier)
                    <option value="{{$supplier->id}}" @if($supplier->id == $product->supplier_id) selected @endif>{{$supplier->supplier_name}} ({{$supplier->company_name}})</option>
                    @endforeach
                  @endif
                </select>
              </div>

            </div>
          </div>
        </div>
        <div class="panel box box-default">
          <div class="box-header with-border">
            <h4 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                Description
              </a>
            </h4>
          </div>
          <div id="collapseTwo" class="panel-collapse collapse in">
            <div class="box-body">
              <div class="">
                <label for="">Description</label>
                <textarea class="textarea" name="description" rows="10" cols="80">@if(isset($product)){{$product->description}}@endif</textarea>
              </div>
            </div>
          </div>
        </div>

        <div class="panel box box-default">
          <div class="box-header with-border">
            <h4 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                Adresse
              </a>
            </h4>
          </div>
          <div id="collapseTwo" class="panel-collapse collapse in">
            <div class="box-body">
              <div class="form-group">
                <label for="">Pays</label>
                <select class="form-control select2" name="country_id" id="country" required>
                  <option value=""></option>
                  @if($countries)
                    @foreach($countries as $countrie)
                    <option value="{{$countrie->id}}" @if(isset($product) && $product->country_id == $countrie->id) selected @endif>{{$countrie->name}}</option>
                    @endforeach
                  @endif
                </select>
              </div>
              <div class="form-group">
                <label for=""> Ville</label>
                <select class="form-control select2" name="city" id="city" required>
                  <option value=""></option>
                  @if($cities)
                    @foreach($cities as $citie)
                    <option value="{{$citie->id}}" @if(isset($product) && $product->city == $citie->id) selected @endif>{{$citie->lib_commune}}</option>
                    @endforeach
                  @endif
                </select>
              </div>
              <div class="form-group">
                <label for="">Adresse précise de l'hébergement</label>
                <input type="text" class="form-control" name="address" id="address" autocomplete="off" required value="@if(isset($product)){{$product->address}}@endif">
                <div class="details">
                  <input type="hidden" name="lat_address" data-geo="lat" />
                  <input type="hidden" name="lng_address" data-geo="lng" />
                  <input type="hidden" name="formatted_address" data-geo="formatted_address" />
                  <input type="hidden" name="country_short_address" data-geo="country_short" />
                  <input type="hidden" name="address_place_id" data-geo="place_id" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="panel box box-default">
          <div class="box-header with-border">
            <h4 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                Images
              </a>
            </h4>
          </div>
          <div id="collapseThree" class="panel-collapse collapse in">
            <div class="box-body">
              <!--input type="file" id="cover" name="cover" class="form-control" -->
              <div id="fine-uploader-gallery"></div>

                @if(isset($product) && $product->product_images)
                <div class=" qq-gallery">
                  <ul class="qq-upload-list-selector qq-upload-list"  aria-live="polite" aria-relevant="additions removals">
                      @foreach($product->product_images as $value)
                      <li>
                          <div class="qq-thumbnail-wrapper">
                              <img class="qq-thumbnail-selector" src="{{$value->path}}" style="width:150px; height:150px">
                          </div>
                          <div class="qq-file-info">
                              <a href="/image/delete/{{$value->id}}" class="btn btn-danger">
                                  Supprimer
                              </a>
                          </div>
                      </li>
                      @endforeach
                </ul>
              </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('products.index') !!}" class="btn btn-default">Annuler</a>
</div>
