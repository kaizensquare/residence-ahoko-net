@extends('layouts.app')

@section('css')
<style media="screen">
.city-autocomplete {
    position: absolute;
    z-index: 10000;
    background: #fff;
    border: 1px solid #c3cacf;
    display: none;
    font-family: Arial, sans-serif;
    -webkit-box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2);
    -moz-box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2);
    box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2);
    -webkit-border-bottom-right-radius: 5px;
    -moz-border-radius-bottomright: 5px;
    border-bottom-right-radius: 5px;
    -webkit-border-bottom-left-radius: 5px;
    -moz-border-radius-bottomleft: 5px;
    border-bottom-left-radius: 5px;
}

.city-autocomplete > div {
    font-size: 16px;
    padding: 15px 10px;
    border-bottom: 1px solid #ccc;
    cursor: pointer;
}

.city-autocomplete > div:hover {
    background: #EEE;
}
.sb-autocomplete--photo {
    display: inline-block;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 30px;
    height: 30px;
    margin-right: 5px;
    border-radius: 3px;
    float: left;
}

.sb-autocomplete__item-with_photo {
    line-height: 30px;
}
</style>
@endsection


@section('content')
    <section class="content-header">
        <h1>
            Ajouter une Residence
        </h1>
    </section>
    <div class="content" style="margin-top:10px">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'products.store','files' => 'true','enctype'=>'multipart/form-data']) !!}

                        @include('products.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
  $(function () {
    $('.textarea').wysihtml5();

  });
</script>



@endsection
