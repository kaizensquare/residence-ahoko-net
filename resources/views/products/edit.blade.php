@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Modifier l'hébergement
        </h1>
   </section>
   <div class="content" style="margin-top:10px">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($product, ['route' => ['products.update', $product->id], 'method' => 'patch','files' => 'true','enctype'=>'multipart/form-data']) !!}

                        @include('products.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
     @include('layouts.gallery_template', ['product' => $product])
@endsection
@section('scripts')
<script>
  $(function () {
    $('.textarea').wysihtml5();

  });

  $('#fine-uploader-gallery').fineUploader({
      template: 'qq-template-gallery',
      debug: true,
      request: {
          endpoint: '/data/uploads/images',
          params: {
            productid:$('#product_id').val()
          },
      },
      thumbnails: {
          placeholders: {
              waitingPath: "{{asset('images/placeholders/waiting-generic.png')}}",
              notAvailablePath: "{{asset('images/placeholders/not_available-generic.png')}}"
          }
      },
      validation: {
          allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
      }
  });
</script>
@endsection
