@extends('layouts.app')

@section('content')

<div class="modal fade" id="new_hebergement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Ajouter un hébergement</h4>
      </div>
      <div class="modal-body">
        <form  action="{!! route('products.store') !!}" method="post">
          @csrf
        <div class="form-group col-sm-10">
            {!! Form::label('categorie', "Type d'hebergement:") !!}
            <select class="form-control" name="category" required>
              <option value="">Sélectionner le type d'hebergement</option>
              <option value="1">Hôtel  (chambre)</option>
              <option value="2">Résidence (Entier)</option>
            </select>
        </div>

        <!-- Name Field -->
        <div class="form-group col-sm-10">
            {!! Form::label('name', 'Nom de votre Hébergement (Résidence - Hôtel):') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

            <div class="">
              <button type="submit" name="submit" class="btn btn-primary">Save changes</button>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>


    <section class="content-header">
        <h1 class="pull-left">Hébergements</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" data-toggle="modal" data-target="#new_hebergement">Ajouter nouveau</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('products.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection
