<div class="table-responsive">
    <table class="table" id="optionItems-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($optionItems as $optionItem)
            <tr>
                <td>{!! $optionItem->name !!}</td>
            <td>{!! $optionItem->status !!}</td>
                <td>
                    {!! Form::open(['route' => ['optionItems.destroy', $optionItem->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('optionItems.show', [$optionItem->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('optionItems.edit', [$optionItem->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
