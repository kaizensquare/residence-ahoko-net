@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Option Item
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($optionItem, ['route' => ['optionItems.update', $optionItem->id], 'method' => 'patch']) !!}

                        @include('option_items.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection