@extends('layouts.master')

@push('css')
<link rel="stylesheet" href="{{asset('css/dashboard.css')}}">
@endpush

@section('content')


<div class="container">
	<div class="row" style="margin-top: 35px;">

    <div class="col-md-12">
      <h3>Bonjour {{Auth::user()->name}}, heureux de vous rencontrer</h3>

    </div>
		<!-- Widget -->
		<div class="col-md-4" style="margin-top: 35px;">
			<div class="sidebar left">

				<div class="my-account-nav-container">

					<ul class="my-account-nav">
						<li class="sub-nav-title">Manage Account</li>
						<li><a href="/dashboard" class="current"><i class="sl sl-icon-home"></i> Accueil</a></li>
						<li><a href="/commandes"><i class="sl sl-icon-docs"></i> Mes commandes</a></li>
						<!--li><a href="#"><i class="sl sl-icon-star"></i> Mes Favoris</a></li-->
					</ul>

					<!--ul class="my-account-nav">
						<li class="sub-nav-title">Manage Listings</li>
						<li><a href="#"><i class="sl sl-icon-docs"></i> Mes Hébergements</a></li>
						<li><a href="#"><i class="sl sl-icon-action-redo"></i> Publier un Hébergement</a></li>
					</ul-->

					<ul class="my-account-nav">
            <!--li><a href="/settings"><i class="sl sl-icon-user"></i> Paramètres</a></li>
						<!--li><a href="#"><i class="sl sl-icon-lock"></i>Modifier Mot de passe</a></l-->
						<li>
							<a href="{!! url('/logout') !!}"
									onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									<i class="sl sl-icon-power"></i> Déconnexion
							</a>
							<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
							</form>

							</li>
					</ul>

				</div>

			</div>
		</div>

		<div class="col-md-8" style="margin-top: 35px;">

      <div class="row">
        <div class="action_panel clearfix">
          <div class="action_panel-main no_sidepanel">
          <div class="action_panel--mainoptions clearfix">
          <a class="action_panel--mainoptions_links ap_newcomers fix_for_lang" href="/">
          <span class="action_panel--mainoptions_icon ic_search"></span>
          <span class="action_panel--mainoptions_title">Trouvez les meilleures offres</span>
          <!--span class="action_panel--mainoptions_text">Accédez aux Offres Privilège en vous connectant avant de commencer votre recherche.</span-->
          <span class="fadeout"></span>
          </a>
          </div>
          </div>
          </div>
      </div>

		</div>

	</div>
</div>
@endsection
